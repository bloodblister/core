<!-- Content Header (Page header) -->
<section class="content-header">
<div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
         <h1>Hospital Profile</h1>
        
        </div>
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/core/admin/">Home</a></li>
            <li class="breadcrumb-item active"><a href="/core/admin/index.php?page=hospital">hospital</a></li>
            <li class="breadcrumb-item active">Edit hospital</li>
        </ol>
        </div>
    </div>
</div>
<!-- /.container-fluid -->
</section>



<?php
     $sql="SELECT * FROM `hospital` WHERE `h_id` = ".$hospital['h_id'];
     $query=mysqli_query($con,$sql);
     $hospital_details= mysqli_fetch_array($query);
?>

<form  action="php/edit-hospital.php" method="post" data-parsley-validate enctype="multipart/form-data">
    
    <section class="content">
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">hospital Details</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <div class="row">
                <div class="col-5">
                   <label class="form-control-label">Name</label>
                    <input type="text" class="form-control" name='name' value="<?php echo $hospital_details['h_name']; ?>">
                    <input type="hidden" name="h_id" value="<?php echo $hospital['h_id'];?>">                    
                </div>
                <div class="col-4">
                       <label class="form-control-label">Image</label>
                        <input type="file" name="image" class="form-control">
                </div>
                <div class="col-3">
                    <strong>Profile</strong>
                    <p class="text-muted mt-1 ">
                        <?php
                            if($hospital_details['h_profile'])
                            {
                                ?>
                                  <img src="../image/profile/<?php echo $hospital_details['h_profile'];?>" width="100px" alt="<?php echo $hospital_details['h_name']; ?>">
                                <?php
                            }
                            else
                            {
                                ?>
                                    <img src="../image/defualt.jpg" width="100px" alt="">
                                <?php
                            }
                        ?>
                    </p>
                </div>
            
            
            <hr>
            <div class="col-6">
           <label class="form-control-label">Email</label>
            <input type="email" class="form-control" name='email' value="<?php echo $hospital_details['email'];?>" readonly>
            </div>
            <hr>
            <div class="col-6">
           <label class="form-control-label">Contact</label>
            <input type="text" class="form-control" name="contact" value="<?php echo $hospital_details['h_contect'];?>">
            </div>
                
            <hr>
            <?php 
                 if($hospital_details['city_id'])
                 {
                     $city_sql="SELECT * FROM `city` WHERE `id`=".$hospital_details['city_id'];
                     $city_run=$con->query($city_sql);
                     $city_details=$city_run->fetch_assoc();
                     $city=$city_details['name'];
                   
                    $dist_sql="SELECT `district_title` FROM `district` WHERE `districtid`=".$city_details['districtid'];
                    //  $dist_sql="SELECT `district_title` FROM `district` WHERE `districtid`=508";
                     $ads=$con->query($dist_sql);
                     $ads=$ads->fetch_assoc();

                     $dist=$ads['district_title'];
                     $sql="SELECT `state_title` FROM `state` WHERE `state_id`=".$city_details['state_id'];
                     $ads=$con->query($sql);
                     $ads=$ads->fetch_assoc();
                     $state=$ads['state_title'];

                 }
                 else{
                     $city="";
                     $dist='';
                     $state='';
                 }
            ?>
            <div class="col-6">
           <label class="form-control-label">Address 1</label>
            <input type="text" class="form-control" name="ads1" value="<?php echo $hospital_details['h_ads1'];?>">
            </div>
            <div class="col-6">
           <label class="form-control-label">Address 2</label>
            <input type="text" class="form-control" name="ads2" value="<?php echo $hospital_details['h_ads2'];?>">
            </div>
             <div class="col-md-4 form-group">
            <label class="form-control-label">Pincode</label>
            <input type="number" name="pincode" name="pincode" class="input-group form-control "  value="<?php echo $hospital_details['h_pincode']; ?>" required>
        </div>
        <?php 

        $state_sql="SELECT * FROM `state` ORDER BY `state_title`";
        $state_run=$con->query($state_sql);
    
        ?>
        <div class="col-md-4 form-group " id="state_div">
            <label class="form-control-label">State</label>
            <select class="form-control" name="state" readonly >
                    <option value="S" selected>--- Select State ---</option>
                    <?php
                        while($state=$state_run->fetch_assoc())
                        {
                            echo "<option value='{$state['state_id']}' required>{$state['state_title']} </option>";
                        }
                    ?>
            </select>
        </div>
        <div class="col-md-4 form-group" id="dist_div">
            <label class="form-control-label">District</label>
            <select class="form-control" name="dist"  disabled>
                    <option value="" selected >--- Select District ---</option>
            </select>
        </div>
        <div class="col-md-4 form-group" id="city_div">
            <label class="form-control-label">City</label>
            <select class="form-control " value="<?php echo $hospital_details['city_id']; ?>" name="city"  disabled>
                    <option>--- Select city ---</option>
            </select>
        </div>
        <div class="col-md-4 form-group">
            <label class="form-control-label">Password</label>
            <input class="form-control " name="password"  type="text" id="password">    
        </div>
        <div class="col-md-4 form-group">
            <label class="form-control-label">Confirm Password</label>
            <input class="form-control " name="cpassword"  type="text" data-parsley-equalto="#password">    
        </div>
    </div>
            
                  
            <button type="submit" class="btn btn-primary">Save </button>
            <a href="/core/hospital/" class="btn btn-danger">Cancel</a>
        </div>

        <!-- /.card-body -->
    </div>
                  
</section>


<script type="text/javascript">
    
    $("select[name='state']").change(function (e) { 
    e.preventDefault();
    var state=$(this).val();
    $.ajax({

        url: "php/dist.php",
        data: {
            state_id:state,
        },
        success: function (response) {
            $('#dist_div').empty();
            $('#dist_div').append(response);
        }
    });
});

$( "#dist_div" ).on( "change", "select[name='dist']", function(e){

    e.preventDefault();
    var dist=$(this).val();
    // console.log(dist);
    $.ajax({

        url: "php/city.php",
        data: {
            districtid:dist,
        },
        success: function (response) {
            $('#city_div').empty();
            $('#city_div').append(response);
                    
        }
    });
});




$(document).ready(function(){
    var pin = $("input[name='pincode'").val();
    // console.log(pin.length);
    
    if (pin.length == 6) {
        get_pincode(pin);
    }
});
 $("input[name='pincode'").keyup(function(e) {
    var pin = $(this).val();
    // console.log(pin.length);

    if (pin.length == 6) {
        get_pincode(pin);
    }
});

    
function get_pincode(pin) {
    try {
        var url = "https://api.postalpincode.in/pincode/" + pin;
        var state, dist, city;
        $.ajax({
            type: "GET",
            url: url,
            success: function(response) {
                try {
                    $.each(response, function(key, value) {
                        $.each(value.PostOffice, function(p_key, p_value) {
                            state = p_value.State;
                            dist = p_value.District;
                            city = p_value.Division;
                        });
                    });
                } catch (err) {
                    console.log('Ferror');
                }

                // $("input[name='state']").val(state);
                $.ajax({
                    type: "POST",
                    url: window.location.origin + '/core/php/city&dist.php',
                    data: {
                        state: state,
                        dist,
                        dist
                    },
                    success: function(data) {
                        try {
                            $('.pin-error').empty();
                            var details = $.parseJSON(data);
                            $("#state_div").empty().append(details.state);
                            $("#dist_div").empty().append(details.dist);
                            $("#city_div").empty().append(details.city);
                            $("#state_div select").addClass('form-control');
                            $("#dist_div select").addClass('form-control');
                            $("#city_div select").addClass('form-control');
                             $("select[name='city'").val(<?php echo $hospital_details['city_id']; ?>).change(); 
                        } catch (err) {
                            $('.pin-error').empty().append('Pincode not found').css('color', 'red');
                        }
                    }
                })
            }

        });
    } catch (err) {
        console.log(err);
        alert(err);
    }

}



</script>

</script>
