<script>
    
    $( "tbody" ).on( "click", "#delete-btn", function(e){
        e.preventDefault();
        var id=$(this).attr('data-id');
        var type=$(this).attr('type');
        if(confirm('Are you sure..!  you want to delete..?'))
        {

            $.ajax({
                url: "php/delete.php",
                data:{
                    id:id,
                    type:type
                },
                success: function (response) {
                    console.log(response);
                    if(response == 1)
                    {
                        // alert('Record Deleted Successful');
                        window.location="";
                    }
                    else
                    {
                        alert('Fail to Delete Record...');
                    }
                }
            });
        }
    });



    
$("select[name='state']").change(function (e) { 
    e.preventDefault();
    var state=$(this).val();
    $.ajax({

        url: "../php/dist.php",
        data: {
            state_id:state,
        },
        success: function (response) {
            $('#dist_div').empty();
            $('#dist_div').append(response);
            $('#city_div select option').slice(1).remove();
        }
    });
});

$( "#dist_div" ).on( "change", "select[name='dist']", function(e){

    e.preventDefault();
    var dist=$(this).val();
    console.log(dist);
    $.ajax({

        url: "../php/city.php",
        data: {
            districtid:dist,
        },
        success: function (response) {
            $('#city_div').empty();
            $('#city_div').append(response);
              
        }
    });
});

</script>