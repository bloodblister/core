<?php

if (!isset ($_GET['page_no']) ) {  
    $page_no = 1;  
} else {  
    $page_no = $_GET['page_no'];  
}  
$results_per_page = 10;  


$row_sql="SELECT * FROM `appointment`";
$row_run=$con->query($row_sql);

$number_of_result = mysqli_num_rows($row_run);  
  
//determine the total number of pages available  
$number_of_page = ceil ($number_of_result / $results_per_page);  
if($page_no > $number_of_page)
{
    $page_no=1;
}

$page_first_result = ($page_no-1) * $results_per_page;  

$sql="SELECT appointment.*,registration.r_firstname,registration.r_lastname,registration.r_contect,registration.email,city.name,centers.h_id,hospital.h_id,blood_component.component_name,hospital.h_name,blood.* FROM `appointment` INNER JOIN registration ON appointment.r_id = registration.r_id INNER JOIN centers on centers.cen_id = appointment.center_id INNER JOIN city ON appointment.city_id = city.id INNER JOIN blood_component ON blood_component.component_id = appointment.component_id INNER JOIN blood on blood.blood_id = blood_component.blood_id INNER JOIN hospital ON hospital.h_id = centers.h_id WHERE hospital.h_id =".$hospital['h_id']." AND NOT appointment.a_status = 0 ORDER BY a_id DESC  LIMIT " . $page_first_result . "," . $results_per_page;
$run=$con->query($sql);


?>

<!-- Content Header (Page header) -->
<section class="content-header">
<div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
         <h1>Appointment</h1>
        </div>
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/core/admin/">Home</a></li>
            <li class="breadcrumb-item active">Appointment</li>
        </ol>
        </div>
    </div>
</div><!-- /.container-fluid -->
</section>

<section class="content">
    <form action="#" class="form-appoitment">
        <input type="hidden" name="center" value="<?php echo $hospital['cen_id'];?>">
        <div class="row">
            <div class="col-12 mb-3">
                <a href="index.php?page=appointment/create-appointment" class="btn btn-primary">Create Appointment</a>
            </div>
            <div class="col-3"> 
                <div class="input-group ">
                    <input type="search" id="search" name="search" placeholder="Search Username , center name" class="form-control" />
                </div>
            </div> 
                <div class="col-2"> 
                    <div class="input-group ">
                        <select name="type" id="app_type" class="form-control" >
                            <option value="" selected>-- Appointment Type ---</option>
                            <option value="0">Request</option>
                            <option value="1">Donation</option>
                        </select>
                    </div>
                </div> 
                
                <?php 
                    $blood_sql="SELECT * FROM `blood`";
                    $blood_run=$con->query($blood_sql);
                ?>
                <div class="col-2">
                    <div class="input-group">
                        <select name="blood" class="form-control" id="blood_type" >
                            <option value="" selected > --- select Blood Type ---</option>
                            <?php 
                                while($blood=$blood_run->fetch_assoc())
                                {
                                    $type=$blood['blood_type'] == 0 ? '-':'+';
                                    echo "<option value='".$blood['blood_id']."' >".$blood['blood_name']." ".$type."</option>";
                                }
                            ?>
                        </select>
                    </div>    
                </div>
                <div class="col-2">
                    <div class="input-group">
                        <input type="submit" class="btn btn-primary" value="Search">
                    </div>    
                </div>
            </form>
                <div class="col-3" >
                    <form action="pages/appointment/appointment-report.php" target="_new" method="POST">
                        <input type="hidden" name="sql" id="sql" value="<?php echo $sql;?>" >
                        <input type="hidden" name="type" id="sql_typ" value="1">
                        <input type="hidden" name="center" id="center" value="<?php echo $hospital['cen_id'];?>">
                        <button type="submit" class="btn btn-info float-right">Report</button>
                    </form>
                </div> 
            </div>
<table class="table mt-3 table-light" id="pspdfkit">
  <thead class="thead-dark">
        <tr>
            <th scope="col">No.</th>
            <th scope="col">User Name</th>
            <th scope="col">Contact</th>
            <th scope="col">Emali</th>
            <th scope="col">Type</th>
            <th scope="col">Vetification Proof</th>
            <th scope="col">Blood Type</th>
            <th scope="col">Status</th>
        </tr>
    </thead>    
    <tbody class="table-body">
    <?php 
    

    if($run)
    {   $i=$page_first_result+1;
        while($row=$run->fetch_assoc())
        {
            $status="Complete";
            if($row['a_status'] == 1)
            {
                $status = "Cancel";
            }
        ?>
            <tr>
                <td scope="row"><?php echo $i;?></td>
                <td><?php echo $row['r_firstname']." ".$row['r_lastname'];?></td>
                <td><?php echo $row['r_contect'];?></td>
                <td><?php echo $row['email'];?></td>
                <td><?php echo $row['a_type'] == 0 ?'Request':'Donation';?></td>
                <td><?php echo $row['gov_id_name'];?></td>
                <td><?php echo $row['blood_name']; echo $row['blood_type'] == 0 ? '-':'+';?></td>
                <td><?php echo $status;?></td>
            </tr>
        <?php
        $i++;
        }
    }

    ?>
    </tbody>
</table>
<nav aria-label="Page navigation example" class="float-right">
  <ul class="pagination">
    <?php 
    if($page_no-1 == 0 ){
    echo '<li class="page-item"><a class="page-link" href = "#">Prev</a></li>';
    }else{
        $prev=$page_no-1;
        echo '<li class="page-item"><a class="page-link" href = "index.php?page=branch&&page_no=' . $prev . '">Prev</a></li>';
    }
    ?>
  
        <?php 
            for($pg = 1; $pg<= $number_of_page; $pg++) {  
                echo '<li class="page-item"><a class="page-link" href = "index.php?page=appointment&&page_no=' . $pg . '">' . $pg . ' </a></li>';  
            } 
        ?>
     <?php 
        $next=$page_no+1;    
        if($page_no == $number_of_page ){
        echo '<li class="page-item"><a class="page-link" href = "#">Next</a></li>';
        }else{
            echo '<li class="page-item"><a class="page-link" href = "index.php?page=branch&&page_no=' . $next . '">Next</a></li>';
        }
    ?>
   </ul>
 </nav>

</section>


<script>
    
    $('.form-appoitment').submit(function (e) { 
        e.preventDefault();
        console.log($(this).serialize());

        $.ajax({
            url: "php/sort-appointment.php",
            data: $(this).serialize(),
            success: function (response) {
                var data=JSON.parse(response);
                
                $('.table-body').empty();
                $('.table-body').append(data.html);
                $("#sql").attr('value',data.sql);
                $("#sql_typ").attr('value',data.type);
            }
        });
    });


    // $('#search').keyup(function (e) { 
    //     var text=$(this).val();
    //     $.ajax({
    //         type: "get",
    //         url: "php/appointment-search.php",
    //         data: {
    //             txt:text,
    //         },
    //         success: function (response) {
    //             $('.table-body').empty();
    //             $('.table-body').append(response);
    //         }
    //     });
    // });
    


    // $('#app_type').change(function (e) { 
    //     e.preventDefault();
    //     var app_type=$(this).val();
        
    //     console.log(app_type);
    //     $.ajax({
    //         url: "php/sort-appointment.php",
    //         data: {
    //             type:app_type,

    //         },
    //         success: function (response) {
    //             console.log(response);
               
    //         }
    //     });

    // });
</script>
<?php include 'pages/common-js.php';?>
