<?php
$con=mysqli_connect('localhost','root', '','bloodblister'); 
$a_id=$_REQUEST['a_id'];
$sql="SELECT * FROM `appointment` INNER JOIN `registration` ON registration.r_id=appointment.r_id INNER JOIN `blood_component` on appointment.component_id = blood_component.component_id INNER JOIN `centers` ON appointment.center_id=centers.cen_id INNER JOIN blood on blood_component.blood_id = blood.blood_id WHERE appointment.a_id=".$a_id;
$appointment_run=mysqli_query($con,$sql);

$appointment=$appointment_run->fetch_assoc();

if($appointment['blood_type'] == 1)
{
    $blood_name=$appointment['blood_name'].' +';
}
if($appointment['blood_type'] == 0)
{
    $blood_name=$appointment['blood_name'].' -';
}

if($appointment['a_type'] == 0)
{
    $appointment_name='Request';
    $patient_name=$appointment['patient_name'];
}

if($appointment['a_type'] == 1)
{
    $appointment_name='Donation';
}

if(isset($appointment['h_id']))
{

    $hospital_sql="SELECT `hospital`.*,`city`.*,district.district_title,state.state_title FROM hospital INNER JOIN city on city.id=hospital.city_id INNER JOIN district on district.districtid = city.districtid INNER JOIN state on state.state_id=city.state_id where h_id=".$appointment['h_id'];
    $hospital_run=$con->query($hospital_sql);
    $hospital=$hospital_run->fetch_assoc();

    $center_name=$hospital['h_name'];
    $center_ads1=$hospital['h_ads1'];
    $center_ads2=$hospital['h_ads2'];
    $center_city=$hospital['name'];
    $center_dist=$hospital['district_title'];
    $center_state=$hospital['state_title'];
    $center_pin=$hospital['h_pincode'];

}
if(isset($appointment['b_id']))
{
    $branch_sql="SELECT `branch`.*,`city`.*,district.district_title,state.state_title FROM branch INNER JOIN city on city.id=branch.city_id INNER JOIN district on district.districtid = city.districtid INNER JOIN state on state.state_id=city.state_id where h_id=".$appointment['h_id'];
    $branch_run=$con->query($branch_sql);
    $branch=$branch_run->fetch_assoc();
    $center_name=$hospital['b_name'];
    $center_ads1=$hospital['b_ads1'];
    $center_ads2=$hospital['b_ads2'];
    $center_city=$hospital['name'];
    $center_dist=$hospital['district_title'];
    $center_state=$hospital['state_title'];
    $center_pin=$hospital['b_pincode'];

    
}


require_once('PDF/tcpdf.php');

class PDF extends TCPDF 
{
    
    public function setData(){
        $this->a_id=$_REQUEST['a_id'];
    }

    public function Header(){
        $imageFile=K_PATH_IMAGES.'bb_logo.png';
        $this->Image($imageFile,20,10,18,'','PNG','','T',false,300,'',false,false,0,false,false,false);
        $this->Ln(3);
        //header title
        $this->setFont('helvetica','B',22);   
        $this->Cell(189,5,'Blood Blister',0,1,'C');
    
        $this->Ln(3);
        $this->setFont('helvetica','B',12);
        $this->Cell(189,3,'Appointment Letter',0,1,'C');
        
        //date
        $this->Ln(3);
        $this->setFont('times','B',10);
        $this->Cell(40,1,"Appointmenr Id : BB000".$this->a_id,0,0);
        $this->Cell(110,1,'',0,0);
        $this->Cell(39,1,'Date: '.date("d M, Y") ,0,1);
        $this->Cell(189,1,'________________________________________________________________________________________________________',0,1);

    }

    
    public function Footer()
    {
        $this->SetY(-18);
        
        $this->setFont('times','B',10);
        $this->Cell(189,3,'________________________________________________________________________________________________________',0,1);
        
        $this->Cell(79,10,'Give blood, it is your right and responsibility',0,0);
        // Page number
        $this->Cell(100,10,'',0,0);
        $this->SetFont('helvetica', 'I', 8);
        $this->Cell(10, 10, $this->getAliasNumPage().' / '.$this->getAliasNbPages(), 0, false, 'C', 0, '', 1, false, 'T', 'M');
    }

}



// create new PDF document
$pdf = new PDF('p', 'mm', 'A4', true, 'UTF-8', false);
$pdf->setData($a_id,$con);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Blood Blister');
$pdf->SetTitle($appointment_name.' Appointment');
$pdf->SetSubject('be happy ');
$pdf->SetKeywords('Blood,Blood blister,donation,blood donation');



// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
$pdf->setFooterData(array(0,64,0), array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}


// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('dejavusans', '', 14, '', true);


// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();





$pdf->ln(18);


//center Deatils
$pdf->SetFont('helvetica','B', 10);
$pdf->Cell(189,10,'Center Details',0,1);

$pdf->SetFont('helvetica','', 10);
// $pdf->Cell(189,3,,0,1);
$pdf->writeHTML('<b> '.$center_name.'</b>', true, false, true, false,'');

$pdf->Cell(189,3,$center_ads1.', '.$center_ads2.',',0,1);
$pdf->Cell(189,3,$center_city .', '.$center_dist.',' ,0,1);
$pdf->Cell(189,3,$center_state.' - '.$center_pin,0,1);

$pdf->ln(13);

$pdf->SetFont('kozminproregular','B',12);
$pdf->Cell(189,3,$appointment_name.' Appointment',0,1,'C');


$pdf->ln(5);
$pdf->SetFont('kozminproregular','',11);
$pdf->Cell(189,10,'An appointment letter must be mandatory for blood donation or request',0,1,'C');



$pdf->ln(10);


// output some RTL HTML content
$html = '
    <table style="width:100%;display: flex;
    justify-content: end;">
        <tbody>
            <tr style="border: 1px solid #000;">
                <td rowspan="10" width="18%"></td>
                <td width="30%" style="line-height:31px">Name </td>
                <td width="40%" style="border-bottom:1px dashed #000;line-height:31px;"><b>'.$appointment["r_firstname"].' '.$appointment["r_lastname"].' </b></td>
                <td rowspan="5" width="10%"></td>

            </tr>
            <tr style="border: 1px solid #000;">
                <td style="line-height:31px">Email </td>
                <td style="border-bottom:1px dashed #000;line-height:31px;"><b>'.$appointment["email"].'</b></td>
            </tr>
            <tr style="border: 1px solid #000;">
                <td style="line-height:31px">Mobile No.</td>
                <td style="border-bottom:1px dashed #000;line-height:31px;"><b>'.$appointment["r_contect"].'</b></td>
            </tr>';

            if($appointment['a_type'] == 0)
            {
                $html.='<tr style="border: 1px solid #000;">
                    <td style="line-height:31px">Patient Name </td>
                    <td style="border-bottom:1px dashed #000;line-height:31px;"><b>'.$appointment['patient_name'].'</b></td>
                </tr>';
            }

            $html.='<tr style="border: 1px solid #000;">
                <td style="line-height:31px">Blood Group </td>
                <td style="border-bottom:1px dashed #000;line-height:31px;"><b>'.$blood_name.'</b></td>
            </tr>
            <tr style="border: 1px solid #000;">
                <td style="line-height:31px">Blood Compponent </td>
                <td style="border-bottom:1px dashed #000;line-height:31px;"><b>'.$appointment['component_name'].'</b></td>
            </tr>
            <tr style="border: 1px solid #000;">
                <td style="line-height:31px">Identity Proof Name </td>
                <td style="border-bottom:1px dashed #000;line-height:31px;"><b>'.$appointment['gov_id_name'].'</b></td>
            </tr>
            <tr style="border: 1px solid #000;">
                <td style="line-height:31px">Identity Proof Number </td>
                <td style="border-bottom:1px dashed #000;line-height:31px;"><b>'.$appointment['gov_id_nume'].'</b></td>
                </tr>
            <tr style="border: 1px solid #000;">
                <td style="line-height:31px">Center Visit Date </td>
                <td style="border-bottom:1px dashed #000;line-height:31px;"><b>'.date_format(date_create($appointment['visiting_date']),'d M, Y').'</b></td>
            </tr>
        </tbody>
    </table>';

$pdf->writeHTML($html, true, false, true, false,'');

$pdf->Ln(37);

$pdf->SetFont('times','B',10);
$pdf->Cell(20,0 ,'',0,0);
$pdf->Cell(50,0 ,'------------------------',0,0);
$pdf->Cell(69,0 ,'',0,0);
$pdf->Cell(50,0 ,'------------------------',0,1);


$pdf->SetFont('kozminproregular','',10);
$pdf->Cell(23,0 ,'',0,0);
$pdf->Cell(40,0 ,'Doner Signter',0,0);
$pdf->Cell(86,0 ,'',0,0);
$pdf->Cell(40,0 ,'Signter',0,1);


// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('example_001.pdf', 'I');
?>
