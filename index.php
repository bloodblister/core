<?php
	include "php/connection.php";
    session_start();
    if(!empty($_SESSION['email']))
    {
        $sql="SELECT * FROM `registration` WHERE `email` = '".$_SESSION['email']."'";
        $query=mysqli_query($con,$sql);
        $row = mysqli_fetch_array($query);
    }
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Home</title>

    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/index.css">
    <link rel="stylesheet" type="text/css" href="css/thalassemia.css">
    <link rel="stylesheet" type="text/css" href="css/donate-blood.css">
    <link rel="stylesheet" type="text/css" href="css/appointment.css">
    <link rel="stylesheet" type="text/css" href="css/center-loction.css">
    <!-- profile css -->
    <link rel="stylesheet" href="css/profile.css">


    <?php require 'attachment/link.php'?>

    <!-- Font -->
    <link href="https://fonts.googleapis.com/css2?family=Righteous&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Cantarell:wght@700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@1,400&display=swap" rel="stylesheet">

            
        <style>
        ul.parsley-errors-list li{
            list-style: none;
            color: red;
            text-transform: capitalize;
        }
        ul.parsley-errors-list{
            padding:5px 0 0 10px;
            margin-bottom: 10px;
        }
        </style>


    <script type="text/javascript">
    $(document).ready(function() {
        var day = 0;
        $("#day-prev").click(function() {
            if (day == 0) {
                day = 4;
                $("#day ul li").css("display", "none");
                $("#day ul li:nth-child(4)").css("display", "block");
            }
            if (day == 1) {
                day = 0;
                $("#day ul li").css("display", "none");
                $("#day ul li:nth-child(0)").css("display", "block");
            }
            if (day == 2) {
                day = 1;
                $("#day ul li").css("display", "none");
                $("#day ul li:nth-child(1)").css("display", "block");
            }
            if (day == 4) {
                day = 2;
                $("#day ul li").css("display", "none");
                $("#day ul li:nth-child(2)").css("display", "block");
            }
            if (day == 4) {
                day = 3;
                $("#day ul li").css("display", "none");
                $("#day ul li:nth-child(3)").css("display", "block");
            }
        })
        $("#day-next").click(function() {
            if (day == 4) {
                day = 0;
                $("#day ul li").css("display", "none");
                $("#day ul li:nth-child(0)").css("display", "block");
            }
            if (day == 3) {
                day = 4;
                $("#day ul li").css("display", "none");
                $("#day ul li:nth-child(4)").css("display", "block");
            }
            if (day == 2) {
                day = 3;
                $("#day ul li").css("display", "none");
                $("#day ul li:nth-child(3)").css("display", "block");
            }
            if (day == 1) {
                day = 2;
                $("#day ul li").css("display", "none");
                $("#day ul li:nth-child(2)").css("display", "block");
            } else {
                day = 1;
                $("#day ul li").css("display", "none");
                $("#day ul li:nth-child(1)").css("display", "block");
            }
        })


    })
    </script>
    <script type="text/javascript">
    $(document).ready(function() {
        $(".registraion").click(function() {
            var h1 = $("#registraion").height();
            if (h1 <= 0) {
                $(".registraion p .fa-minus").show();
                $(".registraion p .fa-plus").hide();
                $("#registraion").css('height', 'auto');
                $("#health-history").height('0px');
                $("#blood-donation").height('0px');
                $("#recovery").height('0px');
            } else {
                $("#registraion").css('height', '0px');
                $("#health-history").height('0px');
                $("#blood-donation").height('0px');
                $(".registraion .fa-minus").hide();
                $(".registraion .fa-plus").show();

            }
        });

        $(".health-history").click(function() {
            var h2 = $("#health-history").height();
            if (h2 <= 0) {
                $(".health-history p .fa-minus").show();
                $(".health-history p .fa-plus").hide();
                $("#health-history").css('height', 'auto');
                $("#registraion").height('0px');
                $("#blood-donation").height('0px');
                $("#recovery").height('0px');
            } else {
                $("#health-history").css('height', '0px');
                $("#registraion").height('0px');
                $("#blood-donation").height('0px');
                $(".health-history .fa-minus").hide();
                $(".health-history .fa-plus").show();
            }
        });

        $(".blood-donation").click(function() {
            var h3 = $("#blood-donation").height();
            if (h3 <= 0) {
                $(".blood-donation p .fa-minus").show();
                $(".blood-donation p .fa-plus").hide();
                $("#blood-donation").css('height', 'auto');
                $("#registraion").height('0px');
                $("#health-history").height('0px');
                $("#recovery").height('0px');
            } else {
                $("#blood-donation").css('height', '0px');
                $("#registraion").height('0px');
                $("#health-history").height('0px');
                $(".blood-donation .fa-minus").hide();
                $(".blood-donation .fa-plus").show();
            }
        });

        $(".recovery").click(function() {
            var h4 = $("#recovery").height();
            if (h4 <= 0) {
                $(".recovery p .fa-minus").show();
                $(".recovery p .fa-plus").hide();
                $("#recovery").css('height', 'auto');
                $("#registraion").height('0px');
                $("#health-history").height('0px');
                $("#blood-donation").height('0px');
            } else {
                $("#recovery").css('height', '0px');
                $("#registraion").height('0px');
                $("#health-history").height('0px');
                $(".recovery .fa-minus").hide();
                $(".recovery .fa-plus").show();
            }
        });

    });
    </script>


    <style type="text/css">
    .container-fluid,
    .col-12,
    .col-xl-12 {
        margin: 0;
        padding: 0;
    }
    </style>

</head>

<body>
   
    <!-- top part -->
    <!-- ===============       navbar ==================== -->
    <?php require 'attachment/navbar.php';?>


    <div id="main-div" >

        <?php
            @$page = $_REQUEST['page'];
            if (@$_REQUEST['page'] == "" || basename($_SERVER['PHP_SELF'] == 'index.php')) {
                $page = "home";
            }

            if (isset($page) && file_exists("page/" . $page . ".php")) {
                include "page/" . $page . ".php";
            } else {
                include 'page/404.php';
            }

        ?>
    </div>

  


    <!-- =================================== -->
    <!-- footer -->
    <?php require "attachment/footer.php"?>
    <!-- =================================== -->


    <script src="javascript/javascript.js"></script>
     <!-- <script src="https://unpkg.com/aos@next/dist/aos.js"></script> -->
<script>
  AOS.init({
  	offset: 200,
	  duration: 800
  	});
</script>

</body>

</html>