<?php 

require "php/connection.php";
session_start();

$error=array();
unset($error['email']);  
unset($error['otp']);
unset($error['password']);
$otp=false;
$password=false;

if(isset($_REQUEST['email_submit']))
{
	$email = $_REQUEST['email'];


    $sql="SELECT `password`,`user_type` FROM `login` WHERE `email`='". $_REQUEST['email']."'";
	$result=mysqli_query($con,$sql);
	$row_num =mysqli_num_rows($result);

	if($row_num > 0)
    {
       $otp=true;
       $otp_num=123456;
        //insert input login table
        $sql="UPDATE `login` SET `otp`='{$otp_num}' WHERE `email`='". $_REQUEST['email']."'";
        $run=$con->query($sql);
    }
    else{
      
        $error['email']="Email does not exist";
    }

}

if(isset($_REQUEST['otp_submit']))
{
    $otp=true;
    $sql="SELECT `otp` FROM `login` WHERE `email`='". $_REQUEST['email']."'";
	$result=mysqli_query($con,$sql);
    $row=mysqli_fetch_assoc($result);
    if($_REQUEST['otp'] == $row['otp'])
    {
       $password=true;
        
    }
    else{
        $error['otp']="Invalid OTP";
    }
}

if(isset($_REQUEST['password_submit']))
{ $password=true;
    if($_REQUEST['password'] == $_REQUEST['confirm_password'])
    {
        $sql="UPDATE `login` SET `password`='".md5($_REQUEST['password'])."' WHERE `email`='". $_REQUEST['email']."'";
        $run=$con->query($sql);
        if($run){
            ?>
            <script>
                alert("Password Changed Successfully"); 
                window.location.href="index.php";
            </script>
            <?php
        }
    }
    else{
        $error['password']="Password does not match";
    }
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Login</title>

    <?php require 'attachment/link.php'?>

    <link rel="stylesheet" type="text/css" href="css/login.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">


    <style>
    ul.parsley-errors-list li{
        list-style: none;
        color: red;
        text-transform: capitalize;
    }
    ul.parsley-errors-list{
        padding:5px 0 0 10px;
        margin-bottom: 10px;
    }
    </style>
    <script>
    $(document).ready(function() {
        $(".form").submit(function(e) {
            var value = $(".password").val();
            if (value.length >= 8) {
                $(".password-erroe").hide();
                return true;
            } else {
                $(".password-erroe").show();
                return false;
            }
        });

        $('.fa-eye').click(function(e) {
            $(".cpassword").attr('type', 'text');
            $(this).hide();
            $('.fa-eye-slash').show();
        });
        $('.fa-eye-slash').click(function(e) {
            $(".cpassword").attr('type', 'password');
            $(this).hide();
            $('.fa-eye').show();
        });

    });
    </script>

    <style>

    </style>
</head>

<body>
    <div class="container" id="skip">
        <h5 class="text-right pr-5  mr-3 mt-5"><span onclick="window.location='index.php'"><b>Skip <i
                        class="fa fa-arrow-right"></i></b><span></h5>
    </div>

    <div class="container-fluid row m-auto">
        <div class="col-md-6 col-xl-4 m-auto " id="form">
            <div id="border-bottom">
                <h2>Forget Password</h2>
                <p>Enter your Email to change password</p>
            </div>
            <form class="form row mt-3" action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post">

            <?php
                if(!$password)
                {
                    ?>

                <div class="form-group col-xl-12">
                    <label>Email </label>
                    <div class="input-div">
                        <i class="fa fa-envelope"></i>
                        <input type="email" name="email" <?php if(isset($_REQUEST['email'])) echo "value='{$_REQUEST['email']}'" ?> class="form-control" placeholder="Enter Email"  <?php if(!isset($_REQUEST['email'])) echo "autofocus='on'"  ?>
                            required>
                    </div>

                    <?php
                        if(isset($error['email']))
                        {
                    ?>
                    <p class="text-danger mt-1"><?php echo $error['email'] ?></p>
                    <?php 
                    }
                    ?>
                </div>
                
                <?php if($otp){
                    ?>
                <div class="form-group col-xl-12">
                    
                    <label>OTP </label>
                    <div class="input-div">
                        <i class="fa fa-lock"></i>
                            <input type="number" name="otp" n class="form-control" placeholder="Enter OTP" autofocus="on"
                                required>
                            </div>

                            <?php
                            if(isset($error['otp']))
                            {
                        ?>
                        <p class="text-danger mt-1"><?php echo $error['otp'] ?></p>
                        <?php 
                        }
                        ?>
                    </div>
                <?php
                }   
                
                ?>

                <?php
                if($otp)
                {
                    ?>

                <div class="form-group col-xl-12 m-0">
                    <input type="submit" name="otp_submit" class="btn float-right">
                </div>
                    <?php
                }else{
                    ?>
                <div class="form-group col-xl-12 m-0">
                    <input type="submit" name="email_submit" class="btn float-right">
                </div>


                    <?php
                }
                ?>
                  
                  <?php
                }else{
?>


            <input type="hidden" name="email" value="<?php echo $_REQUEST['email']?>">
            <div class="form-group col-xl-12 m-0">
                    <label>Password</label>
                    <div class="passwor-div">
                        <i class="fa fa-lock"></i>
                        <input type="password" name="password" class="form-control password"
                            placeholder="Enter Password" required>
                        
                    </div>
                    <div class="invalid-feedback password-erroe">Password Must be 8 digit</div>

                </div>

                <div class="form-group col-xl-12 m-0">
                    <label>Confirm Password</label>
                    <div class="passwor-div">
                        <i class="fa fa-lock"></i>
                        <input type="password" name="confirm_password" class="form-control cpassword"
                            placeholder="Enter Confirm Password" required>
                        <span><i class="fas fa-eye"></i></span>
                        <span><i class="fas fa-eye-slash"></i></span>
                    </div>
                   
                    <?php
                        if(isset($error['password']))
                        {
                    ?>
                    <p class="text-danger mt-1"><?php echo $error['password'] ?></p>
                    <?php 
                    }
                    ?>
                </div>

                <div class="form-group col-xl-12 m-0">
                    <input type="submit" name="password_submit" class="btn float-right">
                </div>
                <?php
                }
            ?>

                <div class="col-xl-12" id="link">
                    <p><a href="registration.php">Registre</a> <a href="#" class="float-right">Forget Password</a></p>
                </div>

            </form>

        </div>
    </div>


</body>

</html>