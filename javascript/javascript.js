window.addEventListener('scroll', function () {
    let header = document.querySelector('header');
    let windowPosition = window.scrollY >90;
    header.classList.toggle('fixed-top', windowPosition);
})