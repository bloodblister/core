<?php 

include 'connection.php';

// var_dump($_REQUEST['city']== '');


if($_REQUEST['Center_type'] == 1)
{
    if(isset($_REQUEST['center_name']))
    {
        $name=$_REQUEST['center_name'];
        $sql="SELECT * FROM `hospital` WHERE `h_name` LIKE '%{$name}%';";
    }
    else{
        $name=NULL;
    }


    if($_REQUEST['city'] != '')
    {
        $city=$_REQUEST['city'];
        $sql="SELECT * FROM `hospital` WHERE city_id IN (SELECT id FROM `city` WHERE `name` LIKE '%{$city}%') OR `h_name` LIKE '%{$name}%';";
    }
    else{
        $city=NULL;
    }

    if($_REQUEST['city'] != '' && $_REQUEST['center_name'] == '')
    {
        $city=$_REQUEST['city'];
        $sql="SELECT * FROM `hospital` WHERE city_id IN (SELECT id FROM `city` WHERE `name` LIKE '%{$city}%');";
    }

    $run=$con->query($sql);
    $html=''; 
    if($run)
    {   $i=1;
        while($hospital=$run->fetch_assoc())
        {
            if($hospital['city_id'])
            {
                $city_sql="SELECT `name` FROM `city` WHERE `id`=".$hospital['city_id'];
                $city_run=$con->query($city_sql);
                $city_name=$city_run->fetch_assoc();
                $city=$city_name['name'];
            }
            else{
                $city="";
            }
            $html.="<tr>
                <td scope='row'> {$i}</td>
                <td>{$hospital['h_name']}</td>
                <td>{$hospital['email']}</td>
                <td>{$hospital['h_ads1']}, {$hospital['h_ads2']}</td>
                <td>{$city}</td>
                <td>{$hospital['h_contect']}</td>
                <td>
                    <a href='index.php?page=Donation-Request&&appointment_type=1&&hospital_id={$hospital['h_id']}' class='btn btn-danger'>Donation</a>
                    <a href='index.php?page=Blood-bottle-Request&&appointment_type=0&&hospital_id={$hospital['h_id']}' class='btn btn-request'>Request</a>
                </td>
            </tr>"
            ;
            $i++;
        }
    }
}
else if($_REQUEST['Center_type'] == 2)
{
    
    if(isset($_REQUEST['center_name']))
    {
        $name=$_REQUEST['center_name'];
        $sql="SELECT * FROM `branch` WHERE `b_name` LIKE '%{$name}%'";
        
    }
    else{
        $name=NULL;
    }


    if($_REQUEST['city'] != '')
    {
        $city=$_REQUEST['city'];
        $sql="SELECT * FROM `branch` WHERE city_id IN (SELECT id FROM `city` WHERE `name` LIKE '%{$city}%') OR `b_name` LIKE '%{$name}%';";

    }
    else{
        $city=NULL;
    }

    if($_REQUEST['city'] != '' && $_REQUEST['center_name'] == '')
    {
        $city=$_REQUEST['city'];
        $sql="SELECT * FROM `branch` WHERE city_id IN (SELECT id FROM `city` WHERE `name` LIKE '%{$city}%');";
    }


    $run=$con->query($sql);
    $html=''; 
    if($run)
    {   $i=1;
        while($branch=$run->fetch_assoc())
        {
            if($branch['city_id'])
            {
                $city_sql="SELECT `name` FROM `city` WHERE `id`=".$branch['city_id'];
                $city_run=$con->query($city_sql);
                $city_name=$city_run->fetch_assoc();
                $city=$city_name['name'];
            }
            else{
                $city="";
            }
            $html.="<tr>
                <td scope='row'> {$i}</td>
                <td>{$branch['b_name']}</td>
                <td>{$branch['email']}</td>
                <td>{$branch['b_ads1']}, {$branch['b_ads2']}</td>
                <td>{$city}</td>
                <td>{$branch['b_contect']}</td>
                <td>
                    <a href='index.php?page=Donation-Request&&appointment_type=1&&branch_id={$branch['b_id']}' class='btn btn-danger'>Donation</a>
                    <a href='index.php?page=Blood-bottle-Request&&appointment_type=0&&branch_id={$branch['b_id']}' class='btn btn-request'>Request</a>
                </td>
            </tr>"
            ;
            $i++;
        }
    }
}

else{
    $html=0;
}

echo $html;


?>