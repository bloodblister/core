<footer class="footer">
	<div class="container row">
		<div class="col-12 col-xl-4">
			<span class="footer-logo">
				<img src="image/logo.png" > Blood Blister
			</span>
			<span class="footer-about">
				<p>
					It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using
				</p>
			</span>
		</div>
		<div class="col-12 col-xl-4">
			<div class="footer-page">
				<h5>Information</h5>
				<ul>
						<li><a href="index.php"> Home </a></li>
						<li><a href="index.php?page=Donation-Request&&appointment_type=1"> Donation Request</a></li>
						<li><a href="index.php?page=Blood-bottle-Request&&appointment_type=0"> Blood Request </a></li>
						<li><a href="index.php?page=Search-Blood-Bank"> Search Center </a></li>
						<li><a href="index.php?page=Blood-Donattion-About"> About us </a></li>
						<li><a href="#"> Services </a></li>
						<li><a href="#"> Contacts </a></li>
				</ul>
			</div>
		</div>
		<div class="col-12 col-xl-4">
			<span class="footer-contact">
				<h5>Contact us</h5>
				<p>
					Washington, USA 6036 Richmond,<br>
					hwy., Alexandria, VA, 2233
				</p>
				<p>Phone <a href="tel:+919099801380" class="phoneno mb-0">+919099801380</a></p>
				<a href="email:ravinlakhani200@gmail.com" class="mt-0 text-primary" style="text-decoration: none;">ravinlakhani200@gmail.com</a>
			</span>
			<span class="footer-social">
				<h5>Social Media</h5>
				<p>View our social media profiles</p>
				<div class="icon">
					<a href="#"><i class="fab fa-facebook-f"></i></a>
					<a href="#"><i class="fab fa-twitter"></i></a>
					<a href="#"><i class="fab fa-linkedin-in"></i></a>
					<a href="#"><i class="fab fa-google-plus-g"></i></a>
					<a href="#"><i class="fab fa-instagram"></i></a>
				</div>
			</span>
		</div>
	</div>
	<p class="m-0 pb-2 container m-auto">
		© 2021 Transport. All rights reserved
	</p>

</footer>