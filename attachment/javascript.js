window.addEventListener('scroll', function () {
    let header = document.querySelector('header');
    let windowPosition = window.scrollY >60;
    header.classList.toggle('fixed-top', windowPosition);
})