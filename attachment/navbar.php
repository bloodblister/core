<header>
    <nav class="navbar navbar-expand-lg navbar-light bg-light" id="navbar">
        <a class="navbar-brand" href="index.php"><img src="image/logo.png" width="50" height="40"
                class="d-inline-block mr-2"><b>Blood Blister</b></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
            aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav ml-auto">
                
                <li class="nav-item active">
                    <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
                </li>

                <?php 
       
          if(isset( $_SESSION['email']))
          {
        ?>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        Services
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="index.php?page=Donation-Request&&appointment_type=1">Donation Request</a>
                        <a class="dropdown-item" href="index.php?page=Blood-bottle-Request&&appointment_type=0">Blood Request</a>
                        <a class="dropdown-item" href="index.php?page=Search-Blood-Bank">Search Center</a>
                        <!-- <a class="dropdown-item" href="#">My Donation</a>
                        <a class="dropdown-item" href="#">Camp Schedule</a> -->
                    </div>
                </li>
                <?php } ?>
                <li class="nav-item">
                    <a class="nav-link" href="index.php?page=Blood-Donattion-About">
                        About us
                    </a>
                </li>
            </ul>
            <?php 
          if(isset( $_SESSION['email']))
          {
            if($row['r_profile'])
            {?>
                <a class="nav-link" href="index.php?page=profile" style="color: rgba(0,0,0,.5);font-weight:800;"> <img src="image/profile/<?php echo $row['r_profile'];?>" width="31px" height="31px"   class="round profile-image" style="margin-right:10px;border-radius: 50%;"><?php echo $row['r_firstname'];?></a>
                <?php
            }else{
            ?>
            <a class="nav-link" href="index.php?page=profile" style="color: rgba(0,0,0,.5);font-weight:800;"> <img src="image/default-user.png" width="31px" class="round profile-image" style="margin-right:10px"><?php echo $row['r_firstname'];?></a>

            <?php
            } 
      }else{
        ?>

            <a href="login.php" class="btn btn-danger"> Login </a>
            <?php
      }
      
      ?>
        </div>
    </nav>
</header>