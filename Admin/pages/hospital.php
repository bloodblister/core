<?php

if (!isset ($_GET['page_no']) ) {  
    $page_no = 1;  
} else {  
    $page_no = $_GET['page_no'];  
}  
$results_per_page =10;


$hospital_sql="SELECT * FROM `hospital`";
$hospital_run=$con->query($hospital_sql);

$number_of_result = mysqli_num_rows($hospital_run);  
  
//determine the total number of pages available  
$number_of_page = ceil ($number_of_result / $results_per_page);  
if($page_no > $number_of_page)
{
    $page_no=1;
}

$page_first_result = ($page_no-1) * $results_per_page;  

?>

<!-- Content Header (Page header) -->
<section class="content-header">
<div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
         <h1>Hospital</h1>
        </div>
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/core/admin/">Home</a></li>
            <li class="breadcrumb-item active">Hospital</li>
        </ol>
        </div>
    </div>
</div><!-- /.container-fluid -->
</section>

<section class="content">

<div class="row">
    <div class="col-8"> 
        <div class="input-group w-50">
            <input type="search" id="search" placeholder="Search hospital" class="form-control" />   
        </div>
    </div>
    <div class="col-3">
        <a href="/core/admin/index.php?page=hospital/hospital-add" class="btn btn-primary float-right">
            Add Hospital    
        </a>
    </div>    
    <div class="col-1">
        <div class="row ml-auto">
            <a href="index.php?page=hospital/hospital-report" class="btn btn-info col-md-9 float-right">
                Report
            </a>
        </div>
    </div>  
</div>

<table class="table mt-3 table-light">
  <thead class="thead-dark">
        <tr>
            <th scope="col">No.</th>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Contact</th>
            <th scope="col">Address</th>
            <th scope="col">Action</th>
        </tr>
    </thead>    
    <tbody class="table-body">
    <?php 
    
    $hospital_sql="SELECT * FROM `hospital` LIMIT " . $page_first_result . "," . $results_per_page;
    $hospital_run=$con->query($hospital_sql);
    
    if($hospital_run)
    {   $i=$page_first_result+1;
        while($hospital=$hospital_run->fetch_assoc())
        {
            if($hospital['city_id'])
            {
                $city_sql="SELECT `name` FROM `city` WHERE `id`=".$hospital['city_id'];
                $city_run=$con->query($city_sql);
                $city_name=$city_run->fetch_assoc();
                $city=$city_name['name'];
            }
            else{
                $city="";
            }
        ?>
            <tr>
                <td scope="row"><?php echo $i;?></td>
                <td><?php echo $hospital['h_name'];?></td>
                <td><?php echo $hospital['email'];?></td>
                <td><?php echo $hospital['h_contect'];?></td>
                <td><?php echo $city;?></td>
                <td>
                    <a href="index.php?page=hospital/hospital-view&&hospitalid=<?php echo $hospital['h_id'];?>" class="btn btn-info" alt="view"> <i class="fa fa-eye"></i> </a>
                    <!-- <a href="#" class="btn btn-warning" alt="Edit"><i class="fas fa-edit"></i></a>  -->
                    <a href="#" id="delete-btn" data-id="<?php echo $hospital['h_id'];?>" type="H" class="btn btn-danger" alt="Delete"><i class="fa fa-trash" ></i> </a>
                </td>
            </tr>
        <?php
        $i++;
        }
    }

    ?>
    </tbody>
</table>
<nav aria-label="Page navigation example" class="float-right">
  <ul class="pagination">
    <?php 
    if($page_no-1 == 0 ){
    echo '<li class="page-item"><a class="page-link" href = "#">Prev</a></li>';
    }else{
        $prev=$page_no-1;
        echo '<li class="page-item"><a class="page-link" href = "index.php?page=hospital&&page_no=' . $prev . '">Prev</a></li>';
    }
    ?>
  
        <?php 
            for($pg = 1; $pg<= $number_of_page; $pg++) {  
                echo '<li class="page-item"><a class="page-link" href = "index.php?page=hospital&&page_no=' . $pg . '">' . $pg . ' </a></li>';  
            } 
        ?>
     <?php 
        $next=$page_no+1;    
        if($page_no == $number_of_page ){
        echo '<li class="page-item"><a class="page-link" href = "#">Next</a></li>';
        }else{
            echo '<li class="page-item"><a class="page-link" href = "index.php?page=hospital&&page_no=' . $next . '">Next</a></li>';
        }
    ?>
   </ul>
 </nav>

</section>

<script>

    $('#search').keyup(function (e) { 
        var text=$(this).val();
        $.ajax({
            type: "get",
            url: "php/hospital-search.php",
            data: {
                txt:text,
            },
            success: function (response) {
                $('.table-body').empty();
                $('.table-body').append(response);
            }
        });
    });
</script>

<?php include 'pages/common-js.php';?>