<!-- Content Header (Page header) -->
<section class="content-header">
<div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
         <h1><b>Add Branch</b></h1>
        </div>
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/core/admin/">Home</a></li>
            <li class="breadcrumb-item active"><a href="/core/admin/index.php?page=branch">Branch</a></li>
            <li class="breadcrumb-item active">Add Branch</li>
        </ol>
        </div>
    </div>
</div><!-- /.container-fluid -->
</section>

<section class="content">
    <div class="row">
    <div class="col-12 pl-3 pr-5 pb-5">
        <form action="php/add-hospital-branch.php" method="post" enctype="multipart/form-data" data-parsley-validate>
            <input type="hidden" name="db" value="B">

            <div class="form-group">
                <label for="exampleInputEmail1">Name</label>
                <input type="text" class="form-control" name="name" data-parsley-required-message="please enter branch name" placeholder="Enter Your Name" <?php if(isset($_SESSION['user'])){ ?> value="<?php echo $_SESSION['user']['name'];?>"<?php }?>  required>
                <small id="emailHelp" class="form-text text-muted"></small>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Contact</label>
                <input type="tel" class="form-control" name="contact"class="form-control" data-parsley-length="[10,10]" data-parsley-length-message="Contact must be 10 digit"
                data-parsley-required-message="please enter contact number" placeholder="Enter Your Contact" <?php if(isset($_SESSION['user'])){ ?> value="<?php echo $_SESSION['user']['contact'];?>"<?php }?> required>
                <?php
                if(isset($_SESSION['numberError']))
                {
                    ?>
                    <p class='text-danger'><?php echo $_SESSION['numberError'] ;?></p>
                    
                    <?php
                    unset($_SESSION["numberError"]);
                    
                }
                ?>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Email</label>
                <input type="email" class="form-control" name="email" data-parsley-required-message="please enter branch email" <?php if(isset($_SESSION['user'])){ ?> value="<?php echo $_SESSION['user']['email'];?>"<?php }?> placeholder="Enter Your Email" required>
                <?php
                if(isset($_SESSION['emailError']))
                {
                    ?>
                    <p class='text-danger'><?php echo $_SESSION['emailError'] ;?></p>
                    
                    <?php
                    unset($_SESSION["emailError"]);
            
                }
                ?>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Passowrd</label>
                <input type="password" data-parsley-minlength="8" data-parsley-required-message="please enter valid Passowrd " data-parsley-trigger="keypress" class="form-control" data-parsley-minlength-message="password should be 8 character long" class="form-control"  name="password" placeholder="Enter Password" required>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">profile</label>
                <input type="file" class="form-control"  name="profile" data-parsley-required-message="please select media photo" placeholder="Select File" required>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Address1</label>
                <input type="text" class="form-control" name="ads1" data-parsley-required-message="please enter hospita address" placeholder="Enter your first address" <?php if(isset($_SESSION['user'])){ ?> value="<?php echo $_SESSION['user']['ads1'];?>"<?php }?> required>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Address2</label>
                <input type="text" class="form-control"  name="ads2" placeholder="Enter your second address" <?php if(isset($_SESSION['user'])){ ?> value="<?php echo $_SESSION['user']['ads2'];?>"<?php }?> required>
            </div>
           
            <div class="form-group"> 
                <label for="exampleInputPassword1">Pincode</label>
                <input type="text" class="form-control" name='pincode' data-parsley-required-message="please enter pincode" name='pincode' placeholder="Enter Pincode" data-parsley-trigger="change" data-parsley-length="[6,6]" data-parsley-length-message="pincode must be 6 digit" placeholder="Enter Pincode" <?php if(isset($_SESSION['user'])){ ?> value="<?php echo $_SESSION['user']['pincode'];?>"<?php }?> required>
                <p class="pin-error"></p>
            </div>
           
            <div class="form-group" id="city_div">
                <label for="exampleInputPassword1">City</label>
                <select name="city" class="form-control" data-parsley-required-message="please enter city" required>
                    <option value="" required>--- Select City ---</option>
                </select>
            </div>
            <div class="form-group" id="dist_div">
                <label for="exampleInputPassword1">district</label>
                <select name="dist" class="form-control" required>
                    <option>--- Select District ---</option>
                </select>
            </div>
            <?php 
                $s_sql="SELECT * FROM `state`";
                $s_query=mysqli_query($con,$s_sql);	
            ?>
                <div class="form-group" id="state_div">
                <label for="exampleInputPassword1">state</label>
                <select name="state" class="form-control" required>
                    <option>--- Select State ---</option>
                    <?php 
                        while ($s_row = mysqli_fetch_array($s_query))
                         {   
                            echo "<option value='".$s_row['state_id']."'>".$s_row['state_title']."</option>";     
                         }
                    ?>
                </select>
            </div>

            <!-- <div class="form-group form-check">
                <input type="checkbox" class="form-check-input" id="exampleCheck1">
                <label class="form-check-label" for="exampleCheck1">Check me out</label>
            </div> -->
            <button type="submit" class="btn btn-primary">Submit</button>
            <?php 
                unset($_SESSION['user']);
            ?>
        </form>
        </div>
    </div>
</section>
</html>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    $("input[name='pincode'").keyup(function(e) {
    var pin = $(this).val();
    // console.log(pin.length);

    if (pin.length == 6) {
        get_pincode(pin);
    }
});

    
function get_pincode(pin) {
    try {
        var url = "https://api.postalpincode.in/pincode/" + pin;
        var state, dist, city;
        $.ajax({
            type: "GET",
            url: url,
            success: function(response) {
                try {
                    $.each(response, function(key, value) {
                        $.each(value.PostOffice, function(p_key, p_value) {
                            state = p_value.State;
                            dist = p_value.District;
                            city = p_value.Division;
                        });
                    });
                } catch (err) {
                    console.log('Ferror');
                }

                // $("input[name='state']").val(state);
                $.ajax({
                    type: "POST",
                    url: window.location.origin + '/core/php/city&dist.php',
                    data: {
                        state: state,
                        dist,
                        dist
                    },
                    success: function(data) {
                        try {
                            $('.pin-error').empty();
                            var details = $.parseJSON(data);
                            $("#state_div").empty().append(details.state);
                            $("#dist_div").empty().append(details.dist);
                            $("#city_div").empty().append(details.city);
                            $("#state_div select").addClass('form-control');
                            $("#dist_div select").addClass('form-control');
                            $("#city_div select").addClass('form-control');

                        } catch (err) {
                            $('.pin-error').empty().append('Pinc    ode not found').css('color', 'red');
                        }
                    }
                })
            }

        });
    } catch (err) {
        console.log(err);
        alert(err);
    }

}
</script>
<?php  include 'pages/common-js.php';?>
