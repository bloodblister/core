<!-- Content Header (Page header) -->
<section class="content-header">
<div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
         <h1>View Branch</h1>
        
        </div>
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/core/admin/">Home</a></li>
            <li class="breadcrumb-item active"><a href="/core/admin/index.php?page=branch">Branch</a></li>
            <li class="breadcrumb-item active">View Branch</li>
        </ol>
        </div>
    </div>
</div>
<!-- /.container-fluid -->
</section>
<?php
     $sql="SELECT * FROM `branch` WHERE `b_id` = ".$_REQUEST['branchid'];
     $query=mysqli_query($con,$sql);
     $branch_details= mysqli_fetch_array($query);
?>


<section class="content">
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Branch Details</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <div class="row">
                <div class="col-9">
                    <strong>Name</strong>
                    <p class="text-muted">
                        <?php echo $branch_details['b_name']; ?>
                    </p>
                </div>
                <div class="col-3">
                    <strong >Profile</strong>
                    <p class="text-muted mt-1 ">
                        <?php
                            if($branch_details['b_profile'])
                            {
                                ?>
                                  <img src="../image/profile/<?php echo $branch_details['b_profile'];?>" width="100px" alt="<?php echo $branch_details['b_name']; ?>">
                                <?php
                            }
                            else
                            {
                                ?>
                                    <img src="../image/defualt.jpg" width="100px" alt="">
                                <?php
                            }
                        ?>
                    </p>
                </div>
            </div>
            
            <hr>
            <strong>Email</strong>
            <p class="text-muted">
                <?php echo $branch_details['email'];?>
            </p>
            
            <hr>
            <strong>Contact</strong>
            <p class="text-muted">
                <?php echo $branch_details['b_contect'];?>
            </p>
            <hr>
            <?php 
                 if($branch_details['city_id'])
                 {
                     $city_sql="SELECT * FROM `city` WHERE `id`=".$branch_details['city_id'];
                     $city_run=$con->query($city_sql);
                     $city_details=$city_run->fetch_assoc();
                     $city=$city_details['name'];
                   
                    $dist_sql="SELECT `district_title` FROM `district` WHERE `districtid`=".$city_details['districtid'];
                    //  $dist_sql="SELECT `district_title` FROM `district` WHERE `districtid`=508";
                     $ads=$con->query($dist_sql);
                     $ads=$ads->fetch_assoc();

                     $dist=$ads['district_title'];
                     $sql="SELECT `state_title` FROM `state` WHERE `state_id`=".$city_details['state_id'];
                     $ads=$con->query($sql);
                     $ads=$ads->fetch_assoc();
                     $state=$ads['state_title'];

                 }
                 else{
                     $city="";
                     $dist='';
                     $state='';
                 }
            ?>
            <strong>Address</strong>
            <p class="text-muted">

                <?php
                  if($branch_details['city_id'])
                  {
                    echo $branch_details['b_ads1'].', '.$branch_details['b_ads2'].', '.$city .', '.$dist.', '.$state .' -'.$branch_details['b_pincode'];
                  }
                  else{
                      echo '';
                  }
                
                ?>
                  
            </p>
        </div>
        <!-- /.card-body -->
    </div>
                  
</section>
