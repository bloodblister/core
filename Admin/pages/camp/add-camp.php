<!-- Content Header (Page header) -->
<section class="content-header">
<div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
         <h1><b>Add Camp</b></h1>
        </div>
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/core/admin/">Home</a></li>
            <li class="breadcrumb-item active"><a href="/core/admin/index.php?page=branch">Camp</a></li>
            <li class="breadcrumb-item active">Add Camp</li>
        </ol>
        </div>
    </div>
</div><!-- /.container-fluid -->
</section>

<section class="content">
    <div class="row">
    <div class="col-12 pl-3 pr-5 pb-5">
        <form action="php/add-camp.php" method="post" enctype="multipart/form-data" data-parsley-validate>
           
            <div class="form-group">
                <label for="exampleInputEmail1">Camp Tile</label>
                <input type="text" class="form-control" name="name" data-parsley-required-message="please enter camp title"placeholder="Enter Camp Name"   required>
                <small id="emailHelp" class="form-text text-muted"></small>
            </div>

            <div class="form-group">
                <label for="exampleInputPassword1">Camp Details :</label>
                <textarea name="details" class="form-control" rows="3" placeholder="Enter Camp details" data-parsley-required-message="please enter camp details"required></textarea>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <label for="">Camp start Date:</label>
                        <input type="date" name="c_start_date" class="form-control" data-parsley-required-message="please enter camp start date" required>
                    </div>
                    <div class="col-md-6">
                        <label for="">Camp End Date:</label>
                        <input type="date" name="c_end_date" data-parsley-required-message="please enter camp end date" class="form-control" required>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="exampleInputPassword1">Camp Thumbnail :</label>
                <input type="file" name="thumbnail" class="form-control" data-parsley-required-message="please enter camp Thumbnail" required>
            </div>
            
            <div class="form-group">
                <label for="exampleInputPassword1">Camp Location(address) :</label>
                <textarea name="ads" class="form-control" rows="3" placeholder="Enter Camp Address" data-parsley-required-message="please enter camp location address" required></textarea>
            </div>
           
            <div class="form-group"> 
                <label for="exampleInputPassword1">Pincode</label>
                <input type="text" class="form-control" name='pincode' data-parsley-required-message="please enter pincode" placeholder="Enter Pincode" data-parsley-trigger="change" data-parsley-length="[6,6]" data-parsley-length-message="pincode must be 6 digit." placeholder="Enter Pincode"  required>
                <p class="pin-error"></p>
            </div>
           
            <div class="form-group" id="city_div">
                <label for="exampleInputPassword1">City</label>
                <select name="city" class="form-control" required>
                    <option>--- Select City ---</option>
                </select>
            </div>
            <div class="form-group" id="dist_div">
                <label for="exampleInputPassword1">district</label>
                <select name="dist" class="form-control" data-parsley-required-message="please enter city" required>
                    <option>--- Select District ---</option>
                </select>
            </div>
            <?php 
                $s_sql="SELECT * FROM `state`";
                $s_query=mysqli_query($con,$s_sql);	
            ?>
                <div class="form-group" id="state_div">
                <label for="exampleInputPassword1">state</label>
                <select name="state" class="form-control" required>
                    <option>--- Select State ---</option>
                    <?php 
                        while ($s_row = mysqli_fetch_array($s_query))
                         {   
                            echo "<option value='".$s_row['state_id']."'>".$s_row['state_title']."</option>";     
                         }
                    ?>
                </select>
            </div>

            <!-- <div class="form-group form-check">
                <input type="checkbox" class="form-check-input" id="exampleCheck1">
                <label class="form-check-label" for="exampleCheck1">Check me out</label>
            </div> -->
            <button type="submit" class="btn btn-primary">Submit</button>
            
        </form>
        </div>
    </div>
</section>
</html>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    $("input[name='pincode'").keyup(function(e) {
    var pin = $(this).val();
    // console.log(pin.length);

    if (pin.length == 6) {
        get_pincode(pin);
    }
});

    
function get_pincode(pin) {
    try {
        var url = "https://api.postalpincode.in/pincode/" + pin;
        var state, dist, city;
        $.ajax({
            type: "GET",
            url: url,
            success: function(response) {
                try {
                    $.each(response, function(key, value) {
                        $.each(value.PostOffice, function(p_key, p_value) {
                            state = p_value.State;
                            dist = p_value.District;
                            city = p_value.Division;
                        });
                    });
                } catch (err) {
                    console.log('Ferror');
                }

                // $("input[name='state']").val(state);
                $.ajax({
                    type: "POST",
                    url: window.location.origin + '/core/php/city&dist.php',
                    data: {
                        state: state,
                        dist,
                        dist
                    },
                    success: function(data) {
                        try {
                            $('.pin-error').empty();
                            var details = $.parseJSON(data);
                            $("#state_div").empty().append(details.state);
                            $("#dist_div").empty().append(details.dist);
                            $("#city_div").empty().append(details.city);
                            $("#state_div select").addClass('form-control');
                            $("#dist_div select").addClass('form-control');
                            $("#city_div select").addClass('form-control');

                        } catch (err) {
                            $('.pin-error').empty().append('Pinc    ode not found').css('color', 'red');
                        }
                    }
                })
            }

        });
    } catch (err) {
        console.log(err);
        alert(err);
    }

}
</script>
<?php  include 'pages/common-js.php';?>
