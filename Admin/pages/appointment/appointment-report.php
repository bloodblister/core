<?php 
$con=mysqli_connect('localhost','root', '','bloodblister'); 


require_once('../../../PDF/tcpdf.php');

$appointment=array();
$html='';

$app_data=array();
if($_REQUEST['type'] == 1)
{

     $sql=explode('LIMIT',$_REQUEST['sql']);
     
     $a_sql=$sql[0];
    $a_run=$con->query($a_sql);
    while($app=$a_run->fetch_array())
    {
        array_push($appointment,$app['a_id']);
    }

    $lenght=count($appointment);
    for($i=0;$i<=$lenght-1;$i++)
    {
        $a_sql="SELECT appointment.*,registration.r_firstname,registration.r_lastname,registration.r_contect,registration.email,city.name,centers.*,blood_component.component_name,blood.* FROM `appointment` INNER JOIN registration ON appointment.r_id = registration.r_id INNER JOIN centers on centers.cen_id = appointment.center_id INNER JOIN city ON appointment.city_id = city.id INNER JOIN blood_component ON blood_component.component_id = appointment.component_id INNER JOIN blood on blood.blood_id = blood_component.blood_id WHERE appointment.a_id=".$appointment[$i];
        $run=$con->query($a_sql);
        $row=$run->fetch_array();

        if($row['h_id'] != '')
        {
            $center_sql="SELECT hospital.h_name FROM `centers` INNER JOIN hospital on centers.h_id = hospital.h_id WHERE cen_id = {$row['cen_id']}";
            $center_run=$con->query($center_sql) or die('Fail to get center');
            $center_row=$center_run->fetch_assoc();
            $row['26']=$center_row['h_name'];
            $row['center']=$center_row['h_name'];
        }
        if ($row['b_id'])
        {
            $center_sql="SELECT branch.h_name FROM `centers` INNER JOIN branch on centers.b_id = branch.b_id WHERE cen_id = {$row['cen_id']}";
            $center_run=$con->query($center_sql) or die('Fail to get center');
            $center_row=$center_run->fetch_assoc();
            $row['26']=$center_row['b_name'];
            $row['center']=$center_row['b_name'];
        }
        if($row['c_id'])
        {
            $center_sql="SELECT camp.c_title FROM `centers` INNER JOIN camp on centers.c_id = camp.c_id WHERE cen_id = {$row['cen_id']}";
            $center_run=$con->query($center_sql) or die('Fail to get center');
            $center_row=$center_run->fetch_assoc();
            $row['26']=$center_row['c_title'];
            $row['center']=$center_row['c_title'];
        }

        array_push($app_data,$row);
    }

}

if($_REQUEST['type'] == 0)
{
    $a_sql=$_REQUEST['sql'];
    $run=$con->query($a_sql);
    
    while($row=$run->fetch_array())
    {  
        if($row['h_id'] != '')
        {
            $center_sql="SELECT hospital.h_name FROM `centers` INNER JOIN hospital on centers.h_id = hospital.h_id WHERE cen_id = {$row['cen_id']}";
            $center_run=$con->query($center_sql) or die('Fail to get center');
            $center_row=$center_run->fetch_assoc();
            $row['26']=$center_row['h_name'];
            $row['center']=$center_row['h_name'];
        }
        if ($row['b_id'])
        {
            $center_sql="SELECT branch.h_name FROM `centers` INNER JOIN branch on centers.b_id = branch.b_id WHERE cen_id = {$row['cen_id']}";
            $center_run=$con->query($center_sql) or die('Fail to get center');
            $center_row=$center_run->fetch_assoc();
            $row['26']=$center_row['b_name'];
            $row['center']=$center_row['b_name'];
        }
        if($row['c_id'])
        {
            $center_sql="SELECT camp.c_title FROM `centers` INNER JOIN camp on centers.c_id = camp.c_id WHERE cen_id = {$row['cen_id']}";
            $center_run=$con->query($center_sql) or die('Fail to get center');
            $center_row=$center_run->fetch_assoc();
            $row['26']=$center_row['c_title'];
            $row['center']=$center_row['c_title'];
        }

        array_push($app_data,$row);
        
    }
}

// echo $_REQUEST['sql'];
// echo '<pre>';
// print_r($app_data);
// echo '</pre>';



class PDF extends TCPDF 
{
    

    public function Header(){
        $imageFile=K_PATH_IMAGES.'bb_logo.png';
        $this->Image($imageFile,20,10,18,'','PNG','','T',false,300,'',false,false,0,false,false,false);
        $this->Ln(3);
        //header title
        $this->setFont('helvetica','B',22);   
        $this->Cell(189,5,'Blood Blister',0,1,'C');
        $this->Ln(1);
        $this->setFont('helvetica','B',12);
        //date
        $this->Ln(3);
        $this->setFont('times','B',10);
        $this->Cell(150,1,'',0,0);
        $this->Cell(39,1,'Date: '.date("d M, Y") ,0,1);
        $this->Cell(189,1,'________________________________________________________________________________________________________',0,1);

    }

    public function ColoredTable($header,$data) {
        // print_r();
        // Colors, line width and bold font
        $this->SetFillColor(87, 87, 87);
        $this->SetTextColor(255);
        $this->SetDrawColor(0, 0, 0);
        $this->SetLineWidth(0.3);
        $this->SetFont('', '');
        // Header
        $w = array(25,25,27,56,20,30);
        $num_headers = count($header);
        for($i = 0; $i < $num_headers;$i++) {
            // echo $i;
            $this->Cell($w[$i], 10, $header[$i], 1, 0, 'C', 1);
        }
        $this->Ln();
        // Color and font restoration
        $this->SetFillColor(224, 235, 255);
        $this->SetTextColor(0);
        $this->SetFont('');
        // Data
        $fill = 0;
        foreach($data as $row) {
            

            $this->Cell($w[0], 9, $row[13], 'LR', 0, 'L', $fill);
            $this->Cell($w[1], 9, $row[26], 'LR', 0, 'L', $fill);
            $this->Cell($w[2], 9, $row[15], 'LR', 0, 'C', $fill);
            $this->Cell($w[3], 9, $row[16], 'LR', 0, 'C', $fill);
            if($row[8] == 0)
            {
                $app_type='Request';
            }
            else{
                $app_type='Donatin';
            }
            $this->Cell($w[4], 9, $app_type, 'LR', 0, 'C', $fill);
            if($row[25] == 1)
            {
                $b_type='+';
            }
            else{
                $b_type='-';
            }
            $this->Cell($w[5], 9, $row[24] . ' '.$b_type, 'LR', 0, 'C', $fill);
            $this->Ln();
            $fill=!$fill;
        }
        $this->Cell(array_sum($w), 0, '', 'T');
    }

    
    public function Footer()
    {
        $this->SetY(-18);
        
        $this->setFont('times','B',10);
        $this->Cell(189,3,'________________________________________________________________________________________________________',0,1);
        
        $this->Cell(79,10,'Give blood, it is your right and responsibility',0,0);
        // Page number
        $this->Cell(100,10,'',0,0);
        $this->SetFont('helvetica', 'I', 8);
        $this->Cell(10, 10, $this->getAliasNumPage().' / '.$this->getAliasNbPages(), 0, false, 'C', 0, '', 1, false, 'T', 'M');
    }

}



// create new PDF document
$pdf = new PDF('p', 'mm', 'A4', true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Blood Blister');
$pdf->SetTitle('Report');
$pdf->SetSubject('be happy ');
$pdf->SetKeywords('Blood,Blood blister,donation,blood donation');



// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
$pdf->setFooterData(array(0,64,0), array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}


// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('dejavusans', '', 14, '', true);


// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();


$pdf->ln(14);
$pdf->SetFont('kozminproregular','',15);
$pdf->Cell(0, 0, 'Appointment Report', 0, 1, 'C', 0, '', 0);


$pdf->ln(6);
$pdf->SetFont('kozminproregular','',11);
$header=array('Username','Center Name','Mobile', 'Email','Type' ,'Blood Group');

$pdf->ColoredTable($header,$app_data);




// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('example_001.pdf', 'I');


?>