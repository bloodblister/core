<?php

if (!isset ($_GET['page_no']) ) {  
    $page_no = 1;  
} else {  
    $page_no = $_GET['page_no'];  
}  
$results_per_page =10;  


$branch_sql="SELECT * FROM `camp`";
$branch_run=$con->query($branch_sql);

$number_of_result = mysqli_num_rows($branch_run);  
  
//determine the total number of pages available  
$number_of_page = ceil ($number_of_result / $results_per_page);  
if($page_no > $number_of_page)
{
    $page_no=1;
}

$page_first_result = ($page_no-1) * $results_per_page;  

?>

<!-- Content Header (Page header) -->
<section class="content-header">
<div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
         <h1>Camp</h1>
        </div>
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/core/admin/">Home</a></li>
            <li class="breadcrumb-item active">Camp</li>
        </ol>
        </div>
    </div>
</div><!-- /.container-fluid -->
</section>

<section class="content">


    <div class="row">
        <div class="col-8"> 
            <div class="input-group w-50">
                <input type="search" id="search" placeholder="Search camp" class="form-control" />
            </div>
        </div>
        <div class="col-3">
            <a href="/core/admin/index.php?page=camp/add-camp" class="btn btn-primary float-right">
                Add Camp    
            </a>
        </div>    
        <div class="col-1">
            <div class="row ml-auto">
                <a href="index.php?pagecamp/add-camp=" class="btn btn-info col-md-9 float-right">
                    Report
                </a>
            </div>
        </div> 
    </div>
<table class="table mt-3 table-light">
  <thead class="thead-dark">
        <tr>
            <th scope="col">No.</th>
            <th scope="col">Tital</th>
            <th scope="col">Address</th>
            <th scope="col">City</th>
            <th scope="col">Status</th>
            <th scope="col">Action</th>
        </tr>
    </thead>    
    <tbody class="table-body">
    <?php 
    
    $camp_sql="SELECT camp.*,city.name FROM `camp` INNER JOIN city on camp.c_city = city.id LIMIT " . $page_first_result . "," . $results_per_page;
    $camp_run=$con->query($camp_sql);
    
    if($camp_run)
    {   $i=$page_first_result+1;
        while($camp=$camp_run->fetch_assoc())
        {
        ?>
            <tr>
                <td scope="row"><?php echo $i;?></td>
                <td><?php echo $camp['c_title'];?></td>
                <td><?php echo $camp['c_addressc'];?></td>
                <td><?php echo $camp['name'];?></td>
                <?php 
                    if($camp['c_status'] == 0)
                    {
                        $status="Cancel";
                    }
                    if($camp['c_status'] == 1)
                    {
                        $status="Compelet";
                    }
                    if($camp['c_status'] == 2)
                    {
                        $status="Pending";
                    }
                ?>
                <td><?php echo $status;?></td>
                <td>
                    <a href="#" id="delete-btn" data-id="<?php echo $camp['c_id'];?>" type="C" class="btn btn-danger" alt="Delete"><i class="fa fa-trash" ></i> </a>

                </td>
            </tr>
        <?php
        $i++;
        }
    }

    ?>
    </tbody>
</table>
<nav aria-label="Page navigation example" class="float-right">
  <ul class="pagination">
    <?php 
    if($page_no-1 == 0 ){
    echo '<li class="page-item"><a class="page-link" href = "#">Prev</a></li>';
    }else{
        $prev=$page_no-1;
        echo '<li class="page-item"><a class="page-link" href = "index.php?page=camp&&page_no=' . $prev . '">Prev</a></li>';
    }
    ?>
  
        <?php 
            for($pg = 1; $pg<= $number_of_page; $pg++) {  
                echo '<li class="page-item"><a class="page-link" href = "index.php?page=camp&&page_no=' . $pg . '">' . $pg . ' </a></li>';  
            } 
        ?>
     <?php 
        $next=$page_no+1;    
        if($page_no == $number_of_page ){
        echo '<li class="page-item"><a class="page-link" href = "#">Next</a></li>';
        }else{
            echo '<li class="page-item"><a class="page-link" href = "index.php?page=camp&&page_no=' . $next . '">Next</a></li>';
        }
    ?>
   </ul>
 </nav>

</section>


<script>
     $('#search').keyup(function (e) { 
        var text=$(this).val();
        $.ajax({
            type: "get",
            url: "php/camp-search.php",
            data: {
                txt:text,
            },
            success: function (response) {
                console.log(response);
                $('.table-body').empty();
                $('.table-body').append(response);
            }
        });
    });
</script>
<?php include 'pages/common-js.php';?>
