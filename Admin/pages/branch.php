<?php

if (!isset ($_GET['page_no']) ) {  
    $page_no = 1;  
} else {  
    $page_no = $_GET['page_no'];  
}  
$results_per_page =10;  


$branch_sql="SELECT * FROM `branch`";
$branch_run=$con->query($branch_sql);

$number_of_result = mysqli_num_rows($branch_run);  
  
//determine the total number of pages available  
$number_of_page = ceil ($number_of_result / $results_per_page);  
if($page_no > $number_of_page)
{
    $page_no=1;
}

$page_first_result = ($page_no-1) * $results_per_page;  

?>

<!-- Content Header (Page header) -->
<section class="content-header">
<div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
         <h1>Branch</h1>
        </div>
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/core/admin/">Home</a></li>
            <li class="breadcrumb-item active">Branch</li>
        </ol>
        </div>
    </div>
</div><!-- /.container-fluid -->
</section>

<section class="content">


    <div class="row">
        <div class="col-8"> 
            <div class="input-group w-50">
                <input type="search" id="search" placeholder="Search branch" class="form-control" />
            </div>
        </div>
        <div class="col-3">
            <a href="/core/admin/index.php?page=branch/branch-add" class="btn btn-primary float-right">
                Add Branch    
            </a>
        </div>    
        <div class="col-1">
            <div class="row ml-auto">
                <a href="index.php?page=branch/branch-report" class="btn btn-info col-md-9 float-right">
                    Report
                </a>
            </div>
        </div> 
    </div>
<table class="table mt-3 table-light">
  <thead class="thead-dark">
        <tr>
            <th scope="col">No.</th>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Contact</th>
            <th scope="col">Address</th>
            <th scope="col">Action</th>
        </tr>
    </thead>    
    <tbody class="table-body">
    <?php 
    
    $branch_sql="SELECT * FROM `branch` LIMIT " . $page_first_result . "," . $results_per_page;
    $branch_run=$con->query($branch_sql);
    
    if($branch_run)
    {   $i=$page_first_result+1;
        while($branch=$branch_run->fetch_assoc())
        {
            if($branch['city_id'])
            {
                $city_sql="SELECT `name` FROM `city` WHERE `id`=".$branch['city_id'];
                $city_run=$con->query($city_sql);
                $city_name=$city_run->fetch_assoc();
                $city=$city_name['name'];
            }
            else{
                $city="";
            }
        ?>
            <tr>
                <td scope="row"><?php echo $i;?></td>
                <td><?php echo $branch['b_name'];?></td>
                <td><?php echo $branch['email'];?></td>
                <td><?php echo $branch['b_contect'];?></td>
                <td><?php echo $city;?></td>
                <td>
                    <a href="/core/admin/index.php?page=branch/branch-view&&branchid=<?php echo $branch['b_id'];?>" class="btn btn-info" alt="view"> <i class="fa fa-eye"></i> </a>
                    <!-- <a href="/core/admin/index.php?page=branch/branch-view&&branchid=<?php echo $branch['b_id'];?>"  class="btn btn-warning" data-id="<?php echo $branch['b_id'];?>" type="B" alt="Edit"><i class="fas fa-edit"></i></a> -->
                    <a href="#" id="delete-btn" data-id="<?php echo $branch['b_id'];?>" type="B" class="btn btn-danger" alt="Delete"><i class="fa fa-trash" ></i> </a>

                </td>
            </tr>
        <?php
        $i++;
        }
    }

    ?>
    </tbody>
</table>
<nav aria-label="Page navigation example" class="float-right">
  <ul class="pagination">
    <?php 
    if($page_no-1 == 0 ){
    echo '<li class="page-item"><a class="page-link" href = "#">Prev</a></li>';
    }else{
        $prev=$page_no-1;
        echo '<li class="page-item"><a class="page-link" href = "index.php?page=branch&&page_no=' . $prev . '">Prev</a></li>';
    }
    ?>
  
        <?php 
            for($pg = 1; $pg<= $number_of_page; $pg++) {  
                echo '<li class="page-item"><a class="page-link" href = "index.php?page=branch&&page_no=' . $pg . '">' . $pg . ' </a></li>';  
            } 
        ?>
     <?php 
        $next=$page_no+1;    
        if($page_no == $number_of_page ){
        echo '<li class="page-item"><a class="page-link" href = "#">Next</a></li>';
        }else{
            echo '<li class="page-item"><a class="page-link" href = "index.php?page=branch&&page_no=' . $next . '">Next</a></li>';
        }
    ?>
   </ul>
 </nav>

</section>


<script>
     $('#search').keyup(function (e) { 
        var text=$(this).val();
        $.ajax({
            type: "get",
            url: "php/branch-search.php",
            data: {
                txt:text,
            },
            success: function (response) {
                $('.table-body').empty();
                $('.table-body').append(response);
            }
        });
    });
</script>
<?php include 'pages/common-js.php';?>
