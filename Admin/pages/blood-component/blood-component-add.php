<!-- Content Header (Page header) -->
<section class="content-header">
<div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
         <h1>Add Component</h1>
        </div>
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/core/admin/">Home</a></li>
            <li class="breadcrumb-item active"><a href="/core/admin/index.php?page=blood-component">Blood & Components</a></li>
            <li class="breadcrumb-item active">Add Component </li>
        </ol>
        </div>
    </div>
</div><!-- /.container-fluid -->
</section>
<?php 

$blood_sql="SELECT * FROM `blood`";
$blood_run=$con->query($blood_sql);

?>

<section class="content">
    <div class="row">
        <div class="col-12 pl-3 pr-5 pb-5">
            <form action="php/component-save.php" method="post" data-parsley-validate>
                <div class="form-group">
                    <label>Blood group</label>
                    <select name="blood_id" class="form-control" data-parsley-required-message="please enter blood group" required>
                        <option value="" required>--- Select Blood group ---</option>
                        <?php
                            while($blood=$blood_run->fetch_assoc())
                            {
                                $type='';
                                if($blood['blood_type'] == 0)
                                {
                                    $type='-';
                                }
                                if($blood['blood_type'] == 1)
                                {
                                    $type='+';
                                }
                                echo " <option value=".$blood['blood_id']." required>{$blood['blood_name']}{$type}</option>";
                            }
                        ?>
                    </select>
                </div>

                <div class="form-group">
                    <label for="my-input">Component Name</label>
                    <input class="form-control" type="text" name="name" placeholder="Enter Component Name" data-parsley-required-message="please enter Component name" required>
                </div>
                <div class="form-group">
                    <label for="my-input">Expiry Day</label>
                    <input class="form-control" type="number" name="expiry_day" data-parsley-required-message="please enter Expiry day" placeholder="Enter Component Day" required>
                </div>
                <div class="form-group">
                    <label for="my-input">Component Price</label>
                    <input class="form-control" type="number" name="price" data-parsley-required-message="please enter Component price" placeholder="Enter Component Price" required>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">
                        Submit
                    </button>
                </div>
            </form>
        </div>
    </div>
</section>