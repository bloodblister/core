<!-- Content Header (Page header) -->
<section class="content-header">
<div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
         <h1>View User</h1>
        
        </div>
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/core/admin/">Home</a></li>
            <li class="breadcrumb-item active"><a href="/core/admin/index.php?page=user">User</a></li>
            <li class="breadcrumb-item active">View User</li>
        </ol>
        </div>
    </div>
</div>
<!-- /.container-fluid -->
</section>
<?php
     $sql="SELECT * FROM `registration` WHERE `r_id` = ".$_REQUEST['userid'];
     $query=mysqli_query($con,$sql);
     $user_details= mysqli_fetch_array($query);
?>


<section class="content">
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">User Details</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <div class="row">
                <div class="col-9">
                    <strong>Name</strong>
                    <p class="text-muted">
                        <?php echo $user_details['r_firstname'].$user_details['r_lastname']; ?>
                    </p>
                </div>
                <div class="col-3">
                    <strong >Profile</strong>
                    <p class="text-muted mt-1 ">
                        <?php
                            if($user_details['r_profile'])
                            {
                                ?>
                                  <img src="../image/profile/<?php echo $user_details['r_profile'];?>" width="100px" alt="<?php echo $user_details['r_firstname'].$user_details['r_lastname']; ?>">
                                <?php
                            }
                            else
                            {
                                ?>
                                    <img src="../image/defualt.jpg" width="100px" alt="">
                                <?php
                            }
                        ?>
                    </p>
                </div>
            </div>
            
            <hr>
            <strong>Email</strong>
            <p class="text-muted">
                <?php echo $user_details['email'];?>
            </p>
            <hr>
            <strong>Gander</strong>
            <p class="text-muted text-capitalize">
                <?php echo $user_details['r_gender'];?>
            </p>
            <hr>
            <strong>Contact</strong>
            <p class="text-muted">
                <?php echo $user_details['r_contect'];?>
            </p>
            <hr>
            <?php 
                 if($user_details['city_id'])
                 {
                     $city_sql="SELECT * FROM `city` WHERE `id`=".$user_details['city_id'];
                     $city_run=$con->query($city_sql);
                     $city_details=$city_run->fetch_assoc();
                     $city=$city_details['name'];
                   
                    $dist_sql="SELECT `district_title` FROM `district` WHERE `districtid`=".$city_details['districtid'];
                    //  $dist_sql="SELECT `district_title` FROM `district` WHERE `districtid`=508";
                     $ads=$con->query($dist_sql);
                     $ads=$ads->fetch_assoc();

                     $dist=$ads['district_title'];
                     $sql="SELECT `state_title` FROM `state` WHERE `state_id`=".$city_details['state_id'];
                     $ads=$con->query($sql);
                     $ads=$ads->fetch_assoc();
                     $state=$ads['state_title'];

                 }
                 else{
                     $city="";
                     $dist='';
                     $state='';
                 }
            ?>
            <strong>Address</strong>
            <p class="text-muted">

                <?php
                  if($user_details['city_id'])
                  {
                    echo $user_details['r_ads1'].', '.$user_details['r_ads2'].', '.$city .', '.$dist.', '.$state .' -'.$user_details['r_pincode'];
                  }
                  else{
                      echo '';
                  }
                
                ?>
                  
            </p>
        </div>
        <!-- /.card-body -->
    </div>

    <?php   
        $appoint_sql="SELECT appointment.*,city.name,centers.*,blood.* FROM appointment INNER JOIN city ON appointment.city_id = city.id INNER JOIN centers ON appointment.center_id = centers.cen_id INNER JOIN blood_component ON appointment.component_id = blood_component.component_id INNER JOIN blood ON blood_component.blood_id = blood.blood_id WHERE appointment.r_id = {$user_details['r_id']};";
        $appoint_run=$con->query($appoint_sql);    
        
    ?>


    <div class="card card-danger">
        <div class="card-header">
            <h3 class="card-title">User Appointment</h3>
        </div>
        <table class="table table-light">
            <thead class="thead-light">
                <tr>
                    <th>No.</th>
                    <th>Center Name</th>
                    <th>City</th>
                    <th>Type</th>
                    <th>Appointment Date</th>
                    <th>visiting Date</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
               <?php
               $i = 1;
                while($result=$appoint_run->fetch_assoc())
                {
                    ?>
                        <tr>
                            <td><?php echo $i++;?></td>  

                            <?php 
                            if(isset($result['h_id']))
                            {
                                $hospital_sql="SELECT * FROM `hospital` WHERE `h_id`=".$result['h_id'];
                                $hospital_run=$con->query($hospital_sql);
                                $hospital=$hospital_run->fetch_assoc();
                                $center=$hospital['h_name'];
                                
                            }
                            if(isset($result['b_id']))
                            {
                                $branch_sql="SELECT * FROM `branch` WHERE `b_id` = ".$result['b_id'];
                                $branch_run=$con->query($branch_sql);
                                $branch=$branch_run->fetch_assoc();
                                $center=$branch['b_name'];
                                
                            }
                            
                            
                            ?>

                            <td><?php echo $result['name']; ?></td>
                            <td><?php echo $center; ?></td>
                            <?php 
                                if($result['a_type'] == 0)
                                {
                                    echo "<td>Request</td>";
                                }
                                else{
                                    echo "<td>Donation</td>";
                                }
                            ?>
                            
                            <td><?php echo date_format(date_create($result['created_at']),'d M, Y'); ?></td>
                            <td><?php echo date_format(date_create($result['visiting_date']),'d M, Y'); ?></td>
                            <?php 
                                if($result['a_status'] == 1)
                                {
                                    echo "<td>Compleat</td>";
                                }
                                else if($result['a_status'] == 0){
                                    echo "<td>Booked</td>";
                                }
                                else{
                                    echo "<td>Canceled</td>";
                                }
                            ?>

                        </tr>
                    <?php

                }
                ?>
            </tbody>
        </table>
    </div>

</section>
