<!-- Content Header (Page header) -->
<section class="content-header">
<div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
         <h1>User</h1>
        </div>
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/core/admin/">Home</a></li>
            <li class="breadcrumb-item active">User</li>
            <li class="breadcrumb-item active">Report</li>
        </ol>
        </div>
    </div>
</div><!-- /.container-fluid -->
</section>

<?php

if(isset($_REQUEST['submit']))
{
    if($_REQUEST['s_date'] != '' and $_REQUEST['e_date'] != '' and $_REQUEST['city'] != '')
    {
        $sql="SELECT * FROM `registration` WHERE city_id IN (SELECT id FROM `city` WHERE `name` LIKE '%{$_REQUEST['city']}%') And `created_at` BETWEEN '{$_REQUEST['s_date']}' AND '{$_REQUEST['e_date']}'";   
    }
    if($_REQUEST['s_date'] != '' and $_REQUEST['e_date'] != '' and $_REQUEST['city'] == '')
    {
        $sql="SELECT * FROM `registration` WHERE `created_at` BETWEEN '{$_REQUEST['s_date']}' AND '{$_REQUEST['e_date']}'";   
    }
    if($_REQUEST['city'] != '' and  $_REQUEST['s_date'] =='' and $_REQUEST['e_date'] == '' )
    {
        $sql="SELECT * FROM `registration` WHERE city_id IN (SELECT id FROM `city` WHERE `name` LIKE '%{$_REQUEST['city']}%')";
    }
    // echo $sql;
    $run=$con->query($sql);
}
    ?>

<section class="content ">
    <div class="row">
        <div class="col-9 pt-4 pb-4">

            <form action="index.php?page=user/user-report" method="post">
                <div class="row">
                    <div class="col-md-3">
                        <label for="">City</label>
                        <input type="text" name="city" class="form-control" placeholder="Enter City Name"
                        <?php
                            if(isset($_REQUEST['city']))
                            {
                                echo "value={$_REQUEST['city']}";
                            }
                            ?>
                        >
                    </div>
                    <div class="col-md-3">
                        <label for="">Registration Form :</label>
                        <input type="date" name="s_date" class="form-control" placeholder="Select date" <?php
                            if(isset($_REQUEST['s_date']))
                            {
                                echo "value={$_REQUEST['s_date']}";
                            }
                        ?>
                        >
                    </div>
                    <div class="col-md-3">
                        <label for="">Registration To</label>
                        <input type="date" name="e_date" class="form-control" placeholder="Select date"  <?php
                            if(isset($_REQUEST['e_date']))
                            {
                                echo "value={$_REQUEST['e_date']}";
                            }
                            ?>
                        >

                    </div>
                    <div class="col-md-3">
                        <label for="" class="pb-3"></label><br>
                        <input type="submit" name="submit" class="btn btn-primary" value="Submit"  disabled>
                        
                        
                    </div>
                </div>
            </form>
        </div>
        
            <?php
            if(isset($_REQUEST['city']) or isset($_REQUEST['s_date']) or isset($_REQUEST['e_date']) )
            {
                ?>
                <div class="col-md-3 pt-4 pb-4 pr-3">
    
    <form action="pages/user/report-pdf.php" target="_new" method="post">
        <input type="hidden" name="sql" value="<?php echo $sql;?>">
        <?php 
        if($_REQUEST['city'] != '')
        {
            echo "<input type='hidden' name='sql' value='{$_REQUEST['city']}'>";
        }
        ?>
        <input type="hidden" name="sql" value="<?php echo $sql;?>">
        <label for="" class="pb-3"></label><br>
        <?php
            if(isset($_REQUEST['submit']))
            {
                ?>
                    <input type="hidden" name="city" value="<?php echo $_REQUEST['city'];?>">
                <?php
            }
            ?>
        <input type="submit" value="Download" class="btn btn-danger float-right " >
    </form>
</div>
                
                <?php
            }
        ?>


        <table class="table">
            <thead class="table-dark">
                    <tr>
                        <th>No.</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Mobile</th>

                        <?php
                                      if(isset($_REQUEST['city']))
                                      {
                                          if($_REQUEST['city'] != '')
                                          {
                                            ?>
                                        <th>City</th>
                                            <?php
                                          }
                                      }
                                    ?>
                        <th>Registration date</th>
                    </tr>
            </thead>
            <?php

                if(isset($_REQUEST['submit']))
                {
                
                    
                    ?>

                <tbody>
        
                    <?php
                        $i=1;
                        while($user=$run->fetch_assoc())
                        {

                            ?>
                                <tr>
                                    <td><?php echo $i++;?></td>
                                    <td><?php echo $user['r_firstname'].' ' .$user['r_lastname'];?></td>
                                    <td><?php echo $user['email'];?></td>
                                    <td><?php echo $user['r_contect'];?></td>
                                    <?php
                                      if($_REQUEST['city'] != '')
                                      {
                                          ?>
                                            <td><?php echo $_REQUEST['city'];?></td>
                                        <?php
                                      }
                                    ?>
                                    <td><?php echo $user['created_at'];?></td>
                                </tr>

                            <?php
                            }

                        }
                        
                    ?>
                </tbody>
        </table>
    </div>
</section>
<script>
    $("input[name='city']").keyup(function () { 
        var len=$(this).val();
        console.log(len);
        if(len.length > 1)
        {
            $("input[name='submit']").removeAttr('disabled');
        }
    });
    $("input[type='date']").change(function () { 
        var len=$(this).val();
        console.log(len);
        if(len.length > 1)
        {
            $("input[name='submit']").removeAttr('disabled');
            $("input[type='date']").attr('required',true );
        }
    });
    
</script>

