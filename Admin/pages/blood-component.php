<?php

if (!isset ($_GET['page_no']) ) {  
    $page_no = 1;  
} else {  
    $page_no = $_GET['page_no'];  
}  
$results_per_page =10;  


$blood_component_sql="SELECT * FROM `blood_component`";
$blood_component_run=$con->query($blood_component_sql);

$number_of_result = mysqli_num_rows($blood_component_run);  
  
//determine the total number of pages available  
$number_of_page = ceil ($number_of_result / $results_per_page);  
if($page_no > $number_of_page)
{
    $page_no=1;
}

$page_first_result = ($page_no-1) * $results_per_page;  

?>

<!-- Content Header (Page header) -->
<section class="content-header">
<div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
         <h1>Blood & Components</h1>
        </div>
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/core/admin/">Home</a></li>
            <li class="breadcrumb-item active">Blood & Components</li>
        </ol>
        </div>
    </div>
</div><!-- /.container-fluid -->
</section>

<section class="content">

<div class="row">
    <div class="col-6">
        <a href="/core/admin/index.php?page=blood-component/blood-component-add" class="btn btn-primary">
            Add Components    
        </a>
    </div>    
    <div class="col-6"> 
        <div class="float-right input-group w-50 pr-4">
            <input type="search" id="search" placeholder="Search Blood Component" class="form-control" />   
        </div>
    </div>
</div>

<table class="table mt-3 table-light">
  <thead class="thead-dark">
        <tr>
            <th scope="col">No.</th>
            <th scope="col">Component Name</th>
            <th scope="col">Blood group</th>
            <th scope="col">Expiry Day</th>
            <th scope="col">Price</th>
            <th scope="col">Action</th>
        </tr>
    </thead>    
    <tbody class="table-body">
    <?php 
    
    $blood_component_sql="SELECT * FROM `blood_component` LIMIT " . $page_first_result . "," . $results_per_page;
    $blood_component_run=$con->query($blood_component_sql);
    
    if($blood_component_run)
    {   $i=$page_first_result+1;
        while($blood_component=$blood_component_run->fetch_assoc())
        {
            $type='';
            if($blood_component['blood_id'])
            {
                
                $blood_sql="SELECT * FROM `blood` WHERE `blood_id`=".$blood_component['blood_id'];
                $blood_run=$con->query($blood_sql);
                $blood=$blood_run->fetch_assoc();
               
                if($blood['blood_type'] == 0)
                {
                    $type='-';
                }
                if($blood['blood_type'] == 1)
                {
                    $type='+';
                }
            }
            else{
                $blood="";
            }
        ?>
            <tr>
                <td scope="row"><?php echo $i;?></td>
                <td><?php echo $blood_component['component_name'];?></td>
                <td><?php echo $blood['blood_name'].$type ;?></td>
                <td><?php echo $blood_component['expiry_day'];?>Day</td>
                <td><?php echo $blood_component['component_price'];?></td>
                <td>
                    <!-- <a href="/core/admin/index.php?page=blood_component/blood_component-view&&blood_componentid=<?php echo $blood_component['b_id'];?>" class="btn btn-info" alt="view"> <i class="fa fa-eye"></i> </a> -->
                    <!-- <a href="#" class="btn btn-warning" alt="Edit"><i class="fas fa-edit"></i></a> -->
                    <a href="#" id="delete-btn" data-id="<?php echo $blood_component['component_id'];?>" type="BC" class="btn btn-danger" alt="Delete"><i class="fa fa-trash" ></i> </a>

                </td>
            </tr>
        <?php
        $i++;
        }
    }

    ?>
    </tbody>
</table>
<nav aria-label="Page navigation example" class="float-right">
  <ul class="pagination">
    <?php 
    if($page_no-1 == 0 ){
    echo '<li class="page-item"><a class="page-link" href = "#">Prev</a></li>';
    }else{
        $prev=$page_no-1;
        echo '<li class="page-item"><a class="page-link" href = "index.php?page=blood_component&&page_no=' . $prev . '">Prev</a></li>';
    }
    ?>
  
        <?php 
            for($pg = 1; $pg<= $number_of_page; $pg++) {  
                echo '<li class="page-item"><a class="page-link" href = "index.php?page=blood_component&&page_no=' . $pg . '">' . $pg . ' </a></li>';  
            } 
        ?>
     <?php 
        $next=$page_no+1;    
        if($page_no == $number_of_page ){
        echo '<li class="page-item"><a class="page-link" href = "#">Next</a></li>';
        }else{
            echo '<li class="page-item"><a class="page-link" href = "index.php?page=blood_component&&page_no=' . $next . '">Next</a></li>';
        }
    ?>
   </ul>
 </nav>

</section>


<script>
     $('#search').keyup(function (e) { 
        var text=$(this).val();
        $.ajax({
            type: "get",
            url: "php/blood_component-search.php",
            data: {
                txt:text,
            },
            success: function (response) {
                $('.table-body').empty();
                $('.table-body').append(response);
            }
        });
    });
</script>
<?php include 'pages/common-js.php';?>
