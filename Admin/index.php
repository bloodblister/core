<?php
include "../php/connection.php";
session_start();

if(!isset($_SESSION['admin']))
{
  header('location:/core/login.php');
}

$sql="SELECT * FROM `login` WHERE `email`='". $_SESSION['admin']."'";
$run=mysqli_query($con,$sql);
$admin=mysqli_fetch_assoc($run);

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Admin :: Blood Blister</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.9.2/parsley.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.9.2/parsley.js"></script>

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>

<style>
  ul.parsley-errors-list li{
      list-style: none;
      color: red;
      text-transform: capitalize;
  }
  ul.parsley-errors-list{
    padding:5px 0 0 10px;
    margin-bottom: 10px;
  }
</style>

</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->
<?php include "template/nav.php"?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  
<?php include "template/sidebar.php"?>
  <!-- Content Wrapper. Contains page content -->
    <!-- Content Header (Page header) -->

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
     <?php
        @$page=$_REQUEST['page'];
        if (@$_REQUEST['page']=="" || basename($_SERVER['PHP_SELF']=='index.php')) {
            $page="home";
        }

        if (isset($page) && file_exists("pages/".$page.".php")) {
            include "pages/".$page.".php";
        }
        else {
            include 'pages/404.php';                     
        }
     ?>
</div>
  <!-- /.content-wrapper -->
<!-- footer -->
  <?php include "template/footer.php"?>
<!-- footer end -->  
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
</body>
</html>
