<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/core/admin/" class="brand-link d-flex align-items-center">
      <img src="../image/logo.png" alt="Blood Blister Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <div class="row pl-2">
        <span class="brand-text">Blood Blister</span><br>
        <span class="brand-text font-weight-light w-100" style="font-size: 17px;">Admin </span>
      </div>
    </a>
   
    <!-- Sidebar -->
    <div class="sidebar">
    
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="/core/admin/" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
                <!-- <i class="right fas fa-angle-left"></i> -->
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/core/admin/index.php?page=user" class="nav-link">
              <i class="nav-icon fas fa-user"></i>
              <p>
               Users
              </p>               
            </a>
          </li>
          <li class="nav-item">
            <a href="/core/admin/index.php?page=hospital" class="nav-link">
              <i class="nav-icon fas fa-hospital"></i>
              <p>
               Hospitals
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/core/admin/index.php?page=branch" class="nav-link">
             <i class="nav-icon fas fa-building"></i>
              <p>
               Branchs
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/core/admin/index.php?page=blood-component" class="nav-link">
             <i class="nav-icon fas fa-tint"></i>
              <p>
              Blood & component
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/core/admin/index.php?page=appointment" class="nav-link">
            <i class="nav-icon fas fa-calendar-check"></i>
              <p>
              Appointment
              </p>
            </a>
          </li>
          
          <li class="nav-item">
            <a href="/core/admin/index.php?page=camp" class="nav-link">
            <img class="nav-icon" src="images/camping-tent.png"/>
              <p>
                Camp
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/core/admin/change_password.php" class="nav-link">
              <i class="fa fa-lock nav-icon" aria-hidden="true"></i>
              <p>
                Change Password
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/core/logout.php" class="nav-link">
            <i class="fas fa-sign-out-alt nav-icon" aria-hidden="true"></i>
            <p>
              Logout
              </p>
            </a>
          </li>

      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>