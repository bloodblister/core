<?php
include "../php/connection.php";
session_start();

if(!isset($_SESSION['admin']))
{
  header('location:/core/login.php');
}
// change password
if (isset($_REQUEST['change_password'])) {
  $old_password = $_REQUEST['old_password'];
  $new_password = $_REQUEST['new_password'];
  $confirm_password = $_REQUEST['confirm_password'];


  $old_password = md5($old_password);
  $new_password = md5($new_password);
  $confirm_password = md5($confirm_password);

  $sql="SELECT * FROM `login` WHERE `email`='". $_SESSION['admin']."'";
  $result = mysqli_query($con, $sql);
  $row = mysqli_fetch_assoc($result);

  if ($old_password == $row['password']) {
    if ($new_password == $confirm_password) {
      $sql = "UPDATE users SET password = '$new_password' WHERE id = '$user_id'";
      $result = mysqli_query($con, $sql);
      if ($result) {
        $success = "Password changed successfully";
      } else {
        $error = "Something went wrong";
      }
    } else {
      $error = "New password and confirm password does not match";
    }
  } else {
    $error = "Old password does not match";
  }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Admin :: Blood Blister</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.9.2/parsley.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.9.2/parsley.js"></script>

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>

<style>
  ul.parsley-errors-list li{
      list-style: none;
      color: red;
      text-transform: capitalize;
  }
  ul.parsley-errors-list{
    padding:5px 0 0 10px;
    margin-bottom: 10px;
  }
</style>

</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->
<?php include "template/nav.php"?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  
<?php include "template/sidebar.php"?>
  <!-- Content Wrapper. Contains page content -->
    <!-- Content Header (Page header) -->

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
     
<!-- Content Header (Page header) -->
<section class="content-header">
<div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
         <h1>Change Password</h1>
        </div>
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/core/admin/">Home</a></li>
            <li class="breadcrumb-item active">Change Password</li>
        </ol>
        </div>
    </div>
</div><!-- /.container-fluid -->
</section>

<section class="content">

    
    <!-- change password -->
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Change Password</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
        <!-- succassce alert box -->
        <div class="card-body">
            <?php if (isset($success)) { ?>
                <div class="alert alert-default-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h5><i class="icon fas fa-check"></i> Success!</h5>
                    <?php echo $success; ?>
                </div>
            <?php } ?>

            
            <?php if (isset($error)) { ?>
                <div class="alert alert-default-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <!-- cross -->

                    <h5><i class="icon fas fa-times"></i> Error!</h5>
                    <?php echo $error; ?>
                </div>
            <?php } ?>

            
    
                <div class="form-group">
                    <label for="old_password">Old Password</label>
                    <input type="password" class="form-control" id="old_password" name="old_password" required placeholder="Enter old password">
                    
                </div>
                <div class="form-group">
                    <label for="new_password">New Password</label>
                    <input type="password" class="form-control" id="new_password" name="new_password" required placeholder="Enter new password">
                </div>
                <div class="form-group">
                    <label for="confirm_password">Confirm Password</label>
                    <input type="password" class="form-control" id="confirm_password" name="confirm_password" required placeholder="Confirm password">
                </div>
            </div>
            <!-- save button -->
            <div class="card-footer">
                <input type="submit" class="btn btn-primary" name="change_password" value="Save">
            </div>
        </form>
    </div>
</section>

</div>
  <!-- /.content-wrapper -->
<!-- footer -->
  <?php include "template/footer.php"?>
<!-- footer end -->  
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
</body>
</html>
