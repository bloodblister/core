    <!-- Content Header (Page header) -->
<section class="content-header">
<div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
         <h1>Create Appointment</h1>
        </div>
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/core/admin/">Home</a></li>
            <li class="breadcrumb-item active">Create Appointment</li>
        </ol>
        </div>
    </div>
</div><!-- /.container-fluid -->
</section>
    
<section class="content">
    <div class="row">
        <div class="col-12 pl-3 pr-5 pb-5">
            <form action="php/create-appointment.php" method="post">
               <input type="hidden" name="center" value="<?php echo $hospital['cen_id'];?>">
            <div class="row">    
                <div class="form-group col-6">
                    <label for="exampleInputEmail1">Firstname</label>
                    <input type="text" class="form-control" name="fname" data-parsley-required-message="please enter branch name" placeholder="Enter Your Name" <?php if(isset($_SESSION['user'])){ ?> value="<?php echo $_SESSION['user']['name'];?>"<?php }?>  required>
                    <small id="emailHelp" class="form-text text-muted"></small>
                </div>
                <div class="form-group col-6">
                    <label for="exampleInputEmail1">Lastname</label>
                    <input type="text" class="form-control" name="lname" data-parsley-required-message="please enter branch name" placeholder="Enter Your Name" <?php if(isset($_SESSION['user'])){ ?> value="<?php echo $_SESSION['user']['name'];?>"<?php }?>  required>
                    <small id="emailHelp" class="form-text text-muted"></small>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-4">
                    <label for="exampleInputPassword1">Contact</label>
                    <input type="number" class="form-control" name="contact"class="form-control" data-parsley-length="[10,10]" data-parsley-length-message="Contact must be 10 digit"
                    data-parsley-required-message="please enter contact number" placeholder="Enter Your Contact"  required>
                </div>
                <div class="form-group col-md-4  row">
                        <div class="form-check col-12 pl-2">
                            <label class="form-check-label" for="male">
                                <b>Gender</b>
                            </label>
                        </div>
                       <div class="form-check form-check-inline col-sm-3 pl-2">
                            <input class="form-check-input" type="radio" name="gender" value="male" id="male">
                            <label class="form-check-label" for="male">
                                Male
                            </label>
                        </div>
                        <div class="form-check form-check-inline col-sm-3">
                            <input class="form-check-input" type="radio" name="gender" value="female" id="female">
                            <label class="form-check-label" for="female">
                                Female
                            </label>
                        </div>
                        <div class="form-check form-check-inline col-sm-3">
                            <input class="form-check-input" type="radio" name="gender" value="other" id="other">
                            <label class="form-check-label" for="other">
                                Other
                            </label>
                        </div>
                </div>

                <div class="form-group col-md-4">
                    <label for="exampleInputPassword1">Date of Birth</label>
                    <input type="date" class="form-control" name="dob"class="form-control" data-parsley-required-message="please enter contact number" placeholder="Enter Your Contact"  required>
                </div>
                
            </div>
            <div class="row">
                <div class="form-group col-4">
                    <label>Appointment Type</label>
                    <select name="a_type" class="form-control" required>
                        <option value="" required>--- select Appointment Type ---</option>
                        <option value="0" required>Request</option>
                        <option value="1" required>Donation</option>
                    </select>
                </div>
                <?php 
                        $blood_sql="SELECT * FROM `blood`";
                        $blood_run=$con->query($blood_sql);

                        ?>

                    <div class="form-group col-4">
                        <label class="form-control-label">Blood Group</label>
                        <select class="form-control" name="blood_group" required>
                            <option value="" selected required>Select Blood group</option>
                        <?php

                        while($blood=$blood_run->fetch_assoc())
                        {    

                            if($blood['blood_type'] == 0)
                            {
                                $type='-';
                            }
                            if($blood['blood_type'] == 1)
                            {
                                $type='+';
                            }
                        ?>
                            <option value="<?php echo $blood['blood_id'];?>" required><?php echo $blood['blood_name'].$type;?></option>
                        <?php 
                        }
                        ?>
                        </select>
                    </div>
                    <div class="form-group col-4" id="component">
                        <label class="form-control-label">Component</label>
                        <select class="form-control" name="component" disabled required>
                                <option value="S" selected>Select Component</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="form-control-label">Identity Proof </label>
                    <select name="gov_id_name" class="form-control input-group" id="">
                        <option value="">--- select Indentity Proof ---</option>    
                        <option value="Passport ">Passport </option>    
                        <option value="Driving License ">Driving License </option>    
                        <option value="Aadhaar Card ">Aadhaar Card </option>    
                        <option value="PAN Card ">PAN Card </option>    
                        <option value="Election Commission ID Card ">Election Commission ID Card </option>    
                    </select>
                </div>
                <div class="form-group">
                    <label class="form-control-label">Identity Proof Nubmer </label>
                    <input type="number" name="gov_id_number" class="input-group form-control">
                </div>
                <div class="row">
                      <div class="col-md-4 form-group">
                      <label for="exampleInputPassword1">House No. / Fleat No.</label>
                        <input type="text" class="form-control" name='ads1' data-parsley-required-message="please enter Address"  required>
                      </div>  
                      <div class="col-md-4 form-group">
                      <label for="exampleInputPassword1">Area</label>
                        <input type="text" class="form-control" name='ads2' data-parsley-required-message="please enter Address"  required>
                      </div>  
                    <div class="form-group col-md-4"> 
                        <label for="exampleInputPassword1">Pincode</label>
                        <input type="text" class="form-control" name='pincode' data-parsley-required-message="please enter pincode" placeholder="Enter Pincode" data-parsley-trigger="change" data-parsley-length="[6,6]" data-parsley-length-message="pincode must be 6 digit" required>
                        <p class="pin-error"></p>
                    </div>
                </div>
            <div class="form-group" id="city_div">
                <label for="exampleInputPassword1">City</label>
                <select name="city" class="form-control" data-parsley-required-message="please enter city" required>
                    <option value="" required>--- Select City ---</option>
                </select>
            </div>
            <div class="form-group" id="dist_div">
                <label for="exampleInputPassword1">district</label>
                <select name="dist" class="form-control" required>
                    <option>--- Select District ---</option>
                </select>
            </div>
            <?php 
                $s_sql="SELECT * FROM `state`";
                $s_query=mysqli_query($con,$s_sql);	
            ?>
                <div class="form-group" id="state_div">
                <label for="exampleInputPassword1">state</label>
                <select name="state" class="form-control" required>
                    <option>--- Select State ---</option>
                    <?php 
                        while ($s_row = mysqli_fetch_array($s_query))
                         {   
                            echo "<option value='".$s_row['state_id']."'>".$s_row['state_title']."</option>";     
                         }
                    ?>
                </select>
            </div>
            <div class="row ">
                <h3 class="col-12">Payment Details</h3>
            </div>
            <div class="row">
                <div class="col-12 form-group">
                         <label for="">Amount</label>
                         <input type="number" name="amount" required class="form-control">
                </div>
                <div class="col-12 from-group">
                         <label for="">Transaction Id</label>
                         <input type="text" name="t_id" required class="form-control">
                </div>
            </div>
            <!-- <div class="form-group form-check">
                <input type="checkbox" class="form-check-input" id="exampleCheck1">
                <label class="form-check-label" for="exampleCheck1">Check me out</label>
            </div> -->
            <button type="submit" class="btn btn-primary mt-3">Submit</button>
        </form>
        </div>
    </div>
</section>


<script>
      $("input[name='pincode'").keyup(function(e) {
    var pin = $(this).val();
    // console.log(pin.length);

    if (pin.length == 6) {
        get_pincode(pin);
    }
});

    
function get_pincode(pin) {
    try {
        var url = "https://api.postalpincode.in/pincode/" + pin;
        var state, dist, city;
        $.ajax({
            type: "GET",
            url: url,
            success: function(response) {
                try {
                    $.each(response, function(key, value) {
                        $.each(value.PostOffice, function(p_key, p_value) {
                            state = p_value.State;
                            dist = p_value.District;
                            city = p_value.Division;
                        });
                    });
                } catch (err) {
                    console.log('Ferror');
                }

                // $("input[name='state']").val(state);
                $.ajax({
                    type: "POST",
                    url: window.location.origin + '/core/php/city&dist.php',
                    data: {
                        state: state,
                        dist,
                        dist
                    },
                    success: function(data) {
                        try {
                            $('.pin-error').empty();
                            var details = $.parseJSON(data);
                            $("#state_div").empty().append(details.state);
                            $("#dist_div").empty().append(details.dist);
                            $("#city_div").empty().append(details.city);
                            $("#state_div select").addClass('form-control');
                            $("#dist_div select").addClass('form-control');
                            $("#city_div select").addClass('form-control');

                        } catch (err) {
                            $('.pin-error').empty().append('Pinc    ode not found').css('color', 'red');
                        }
                    }
                })
            }

        });
    } catch (err) {
        console.log(err);
        alert(err);
    }

}



$("select[name='blood_group']").change(function (e) { 
    e.preventDefault();
    var blood_id=$(this).val();
    $.ajax({
        url: "../php/get_component.php",
        data: {
            blood_id:blood_id,
        },
        success: function (response) {
            console.log(response);
            if(response != 0)
            {
                $('#component').empty();
                $('#component').append(response);
            }
        }
    });
});

$("select[name='state']").change(function (e) { 
    e.preventDefault();
    var state=$(this).val();
    $.ajax({

        url: "../php/dist.php",
        data: {
            state_id:state,
        },
        success: function (response) {
            $('#dist_div').empty();
            $('#dist_div').append(response);
              
        }
    });
});

$( "#dist_div" ).on( "change", "select[name='dist']", function(e){

    e.preventDefault();
    var dist=$(this).val();
    // console.log(dist);
    $.ajax({

        url: "../php/city.php",
        data: {
            districtid:dist,
        },
        success: function (response) {
            $('#city_div').empty();
            $('#city_div').append(response);
              
        }
    });
});


$( "#city_div" ).on( "change", "select[name='city']", function(e){
    e.preventDefault();
    var city=$(this).val();
    $.ajax({
        url: "../php/get_center.php",
        data: {
            city_id:city,
        },
        success: function (response) {
            $('#center_div').empty();
            $('#center_div').append(response);
        }
    });
});

</script>
<?php include 'pages/common-js.php';?>
