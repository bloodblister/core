<!-- Content Header (Page header) -->
<section class="content-header">
<div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
         <h1>View Request</h1>
        
        </div>
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/core/admin/">Home</a></li>
            <li class="breadcrumb-item active"><a href="/core/admin/index.php?page=branch">Appointment</a></li>
            <li class="breadcrumb-item active">View Request</li>
        </ol>
        </div>
    </div>
</div>
<!-- /.container-fluid -->
</section>
<?php
    $sql="SELECT appointment.*,registration.*,district.district_title,city.name,state.state_title,branch.b_name,branch.b_contect,branch.b_ads1,branch.b_ads2,branch.b_pincode,branch.b_pincode FROM appointment INNER JOIN registration on registration.r_id = appointment.r_id INNER JOIN city ON city.id = registration.city_id INNER JOIN state on state.state_id = city.state_id INNER JOIN district on district.districtid = city.districtid INNER JOIN centers ON centers.cen_id = appointment.center_id INNER JOIN branch ON branch.b_id = centers.b_id WHERE appointment.a_id = ".$_REQUEST['a_id'];
     $query=mysqli_query($con,$sql);
     $request= $query->fetch_assoc();
    
?>


<section class="content">
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">User Details</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <div class="row">
                <div class="col-6">
                    <strong>First Name</strong>
                    <p class="text-muted">
                        <?php echo $request['r_firstname'];?>
                    </p>
                    <hr>
                </div>
                <div class="col-6">
                    <strong>Last Name</strong>
                    <p class="text-muted">
                        <?php echo $request['r_lastname'];?>
                    </p>
                    <hr>
                </div>
            </div>
            <div class="row mt-3"> 
                <div class="col-4">
                     <strong>Contact</strong>
                     <p class="text-muted">
                        <?php echo $request['r_contect'];?>
                    </p>
                    <hr>
                </div>
                <div class="col-4">
                     <strong>Date of Birth</strong>
                     <p class="text-muted">
                        <?php echo $request['r_dob'];?>
                    </p>
                    <hr>
                </div>
                <div class="col-4">
                     <strong>Gender</strong>
                     <p class="text-muted">
                        <?php echo $request['r_gender'];?>
                    </p>
                    <hr>
                </div>
            </div>  
            <div class="row mt-3">
                <div class="col-12">
                    <strong>Address</strong>
                     <p class="text-muted">
                        <?php echo $request['r_ads1'].", ".$request['r_ads2'].", ".$request['name'].", ".$request['district_title'].", ".$request['state_title']." -".$request['r_pincode'];?>
                    </p>
                    <hr>
                </div>
            </div>     
        </div>
        <!-- /.card-body-->
    </div>
    <?php 
    if($request['a_type'] == 0)
    {
        $sql= "SELECT * from payment where a_id={$_REQUEST['a_id']}";
        $query=mysqli_query($con,$sql);
        $request= $query->fetch_assoc();
    
    ?>
    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title">Payment Details</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <strong>Payment id</strong>
            <p class="text-muted">
                BBPT00<?php echo $request['p_id'];?>
            </p>
            <hr>
            <strong>Amount</strong>
            <p class="text-muted">
                <?php echo $request['p_amount'];?>
            </p>
            <hr>
            <strong>Transaction Id</strong>
                <p class="text-muted">
                <?php echo $request['t_id'];?>
            </p>
            <hr>
        </div>
        <!-- /.card-body -->
    </div>
    <?php } ?>
    <div class="row">
        <div class="col-12 d-flex justify-content-end">
            <a href="php/appointment.php?id=<?php echo $request['a_id'];?>&&type=y" class="btn btn-primary">Confirem</a>
            <a href="php/appointment.php?id=<?php echo $request['a_id'];?>&&type=n" class="btn btn-danger ml-2">Cancel</a>
        </div>
    </div>
                  
</section>
