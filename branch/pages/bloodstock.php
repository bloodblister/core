

<!-- Content Header (Page header) -->
<section class="content-header">
<div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
         <h1>Blood Stock</h1>
        </div>
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/core/admin/">Home</a></li>
            <li class="breadcrumb-item active">Blood Stock</li>
        </ol>
        </div>
    </div>
</div><!-- /.container-fluid -->
</section>

<section class="content">
                
        <table class="table mt-3 table-light" id="pspdfkit">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">No.</th>
                    <th scope="col">Blood Group</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>    
            <tbody class="table-body">
                <?php
                    $bsql="SELECT * FROM `blood`";
                    $brun=$con->query($bsql);
                    $i=1;
                    while($brow=$brun->fetch_assoc())
                    {
                            $sign='+';
                            if($brow['blood_type'] == 0)
                            {
                                $sign='-';
                            }
                            ?>
                                <tr>
                                    <td><?php echo $i++;?></td>
                                    <td><?php echo $brow['blood_name'] ." ".$sign;?></td>
                                    <td>
                                        <a href="index.php?page=bloodstock/compoment-details&&blood_id=<?php echo $brow['blood_id'];?>" class="btn btn-info">
                                            <i class="fa fa-eye" aria-hidden="true"></i>
                                        </a>
                                    </td> 
                                </tr>
                            <?php
                    }
                    

                ?>
                <tr>
                    <td></td>
                </tr>
            </tbody>
        </table>

</section>


<script>
    
    $('.form-appoitment').submit(function (e) { 
        e.preventDefault();
        console.log($(this).serialize());

        $.ajax({
            url: "php/sort-appointment.php",
            data: $(this).serialize(),
            success: function (response) {
                var data=JSON.parse(response);
                
                $('.table-body').empty();
                $('.table-body').append(data.html);
                $("#sql").attr('value',data.sql);
                $("#sql_typ").attr('value',data.type);
            }
        });
    });


    // $('#search').keyup(function (e) { 
    //     var text=$(this).val();
    //     $.ajax({
    //         type: "get",
    //         url: "php/appointment-search.php",
    //         data: {
    //             txt:text,
    //         },
    //         success: function (response) {
    //             $('.table-body').empty();
    //             $('.table-body').append(response);
    //         }
    //     });
    // });
    


    // $('#app_type').change(function (e) { 
    //     e.preventDefault();
    //     var app_type=$(this).val();
        
    //     console.log(app_type);
    //     $.ajax({
    //         url: "php/sort-appointment.php",
    //         data: {
    //             type:app_type,

    //         },
    //         success: function (response) {
    //             console.log(response);
               
    //         }
    //     });

    // });
</script>
<?php include 'pages/common-js.php';?>
