<?php 

//cout total appointment
$a_sql="SELECT appointment.*,centers.b_id,branch.b_name,branch.b_id FROM appointment INNER JOIN centers on centers.cen_id = appointment.center_id INNER JOIN branch on branch.b_id = centers.b_id WHERE branch.b_id= ".$branch['b_id'];
$a_result = mysqli_query($con, $a_sql);
$total_appointment=mysqli_num_rows($a_result);

// count today appointment
$sql="SELECT appointment.*,centers.b_id,branch.b_name,branch.b_id FROM appointment INNER JOIN centers on centers.cen_id = appointment.center_id INNER JOIN branch on branch.b_id = centers.b_id WHERE branch.b_id= ".$branch['b_id']." AND appointment.created_at ='".date('Y-m-d')."'";
$result = mysqli_query($con, $sql);
$today_appointment = mysqli_num_rows($result);


?>


  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Dashboard</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <!-- <li class="breadcrumb-item active">Dashboard </li> -->
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-info">
            <div class="inner">
              <h3><?php echo $total_appointment; ?></h3>

              <p>Total Appointmnet</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-success">
            <div class="inner">
              <h3><?php echo $today_appointment ?></h3>

              <p>Today's Appointmnet</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <!-- <div class="col-lg-3 col-6">
          <div class="small-box bg-danger">
            <div class="inner">
              <h3>44</h3>

              <p>Total Donation</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div> -->
        <!-- ./col -->
        <!-- <div class="col-lg-3 col-6">
          <div class="small-box bg-warning">
            <div class="inner">
              <h3>65</h3>

              <p>Total Visitors</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div> -->
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      </div>
    </section>