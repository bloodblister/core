<!-- Content Header (Page header) -->
<section class="content-header">
<div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
         <h1>Blood Group Availability</h1>
        
        </div>
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/core/admin/">Home</a></li>
                <li class="breadcrumb-item active"><a href="/core/admin/index.php?page=branch">Blood Stock</a></li>
                <li class="breadcrumb-item active">Blood Group Availability</li>
            </ol>
        </div>
    </div>
</div>
<!-- /.container-fluid -->
</section>


<section class="content">
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Compoment Details</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
        <table class="table mt-3 table-light" id="pspdfkit">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">No.</th>
                    <th scope="col">Blood Group</th>
                    <th scope="col">Blood component</th>
                    <th scope="col">Available Stock</th>
                </tr>
            </thead>    
            <tbody class="table-body">
                <?php
                  
                    $i=1;
                   

                    $csql="SELECT blood_component.*,blood.* FROM `blood_component` INNER JOIN blood ON blood.blood_id = blood_component.blood_id WHERE blood_component.blood_id = ".$_REQUEST['blood_id'];
                    $crun=$con->query($csql);
                    while($crow=$crun->fetch_assoc())
                    {
                        $sign='+';
                        if($crow['blood_type'] == 0)
                        {
                            $sign='-';
                        }

                        $donatin_count="SELECT COUNT(a_id) AS count FROM appointment WHERE component_id=".$crow['component_id']." AND a_type=1 AND center_id=".$branch['cen_id'];
                        $d_run=$con->query($donatin_count);
                        $donations=$d_run->fetch_assoc();

                        $requst_count="SELECT COUNT(a_id) AS count FROM appointment WHERE component_id=".$crow['component_id']." AND a_type=0 AND center_id=".$branch['cen_id'];
                        $r_run=$con->query($requst_count);
                        $requests=$r_run->fetch_assoc();
                    
                        
                        $available=$donations['count']-$requests['count'];

                        ?>
                            <tr>
                                <td><?php echo $i++;?></td>
                                <td><?php echo $crow['blood_name'] ." ".$sign;?></td>
                                <td><?php echo $crow['component_name'];?></td>
                                <td><?php echo $available;?></td>
                                
                            </tr>
                        <?php
                    }
                
                ?>
                <tr>
                    <td></td>
                </tr>
            </tbody>
        </table>
        <!-- /.card-body -->
        </div>
    </div>
                  
</section>
