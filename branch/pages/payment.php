<?php

if (!isset ($_GET['page_no']) ) {  
    $page_no = 1;  
} else {  
    $page_no = $_GET['page_no'];  
}  
$results_per_page = 10;  


$row_sql="SELECT * FROM `appointment`";
$row_run=$con->query($row_sql);

$number_of_result = mysqli_num_rows($row_run);  
  
//determine the total number of pages available  
$number_of_page = ceil ($number_of_result / $results_per_page);  
if($page_no > $number_of_page)
{
    $page_no=1;
}

$page_first_result = ($page_no-1) * $results_per_page;  

$sql="SELECT payment.*,registration.r_firstname,registration.r_lastname FROM payment INNER JOIN appointment on appointment.a_id = payment.a_id INNER JOIN registration on registration.r_id =appointment.r_id WHERE appointment.center_id = ".$branch['cen_id']." ORDER BY p_id DESC  LIMIT " . $page_first_result . "," . $results_per_page;
$run=$con->query($sql);


?>

<!-- Content Header (Page header) -->
<section class="content-header">
<div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
         <h1>Payment</h1>
        </div>
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/core/admin/">Home</a></li>
            <li class="breadcrumb-item active">Payment</li>
        </ol>
        </div>
    </div>
</div><!-- /.container-fluid -->
</section>

<section class="content">
        <div class="row">
            <div class="col-3">
                <div class="input-group ">
                    <input type="search" id="search" name="search" placeholder="Search Username , center name" class="form-control" />
                </div>
            </div>
        </div>
<table class="table mt-3 table-light" id="pspdfkit">
  <thead class="thead-dark">
        <tr>
            <th scope="col">No.</th>
            <th scope="col">Username</th>
            <th scope="col">Transaction Id</th>
            <th scope="col">Amount</th>
            <th scope="col">Date</th>
            
        </tr>
    </thead>    
    <tbody class="table-body">
    <?php 
    

    if($run)
    {   $i=$page_first_result+1;
        while($row=$run->fetch_assoc())
        {

        ?>
            <tr>
                <td scope="row"><?php echo $i;?></td>
                <td><?php echo $row['r_firstname']." ".$row['r_lastname'];?></td>
                <td><?php echo $row['t_id'];?></td>
                <td><?php echo $row['p_amount'];?></td>
                <td><?php echo $row['created_at'];?></td>
             
            </tr>
        <?php
        $i++;
        }
    }

    ?>
    </tbody>
</table>
<nav aria-label="Page navigation example" class="float-right">
  <ul class="pagination">
    <?php 
    if($page_no-1 == 0 ){
    echo '<li class="page-item"><a class="page-link" href = "#">Prev</a></li>';
    }else{
        $prev=$page_no-1;
        echo '<li class="page-item"><a class="page-link" href = "index.php?page=branch&&page_no=' . $prev . '">Prev</a></li>';
    }
    ?>
  
        <?php 
            for($pg = 1; $pg<= $number_of_page; $pg++) {  
                echo '<li class="page-item"><a class="page-link" href = "index.php?page=appointment&&page_no=' . $pg . '">' . $pg . ' </a></li>';  
            } 
        ?>
     <?php 
        $next=$page_no+1;    
        if($page_no == $number_of_page ){
        echo '<li class="page-item"><a class="page-link" href = "#">Next</a></li>';
        }else{
            echo '<li class="page-item"><a class="page-link" href = "index.php?page=branch&&page_no=' . $next . '">Next</a></li>';
        }
    ?>
   </ul>
 </nav>

</section>


<script>
    
    $('.form-appoitment').submit(function (e) { 
        e.preventDefault();
        console.log($(this).serialize());

        $.ajax({
            url: "php/sort-appointment.php",
            data: $(this).serialize(),
            success: function (response) {
                var data=JSON.parse(response);
                
                $('.table-body').empty();
                $('.table-body').append(data.html);
                $("#sql").attr('value',data.sql);
                $("#sql_typ").attr('value',data.type);
            }
        });
    });


    // $('#search').keyup(function (e) { 
    //     var text=$(this).val();
    //     $.ajax({
    //         type: "get",
    //         url: "php/appointment-search.php",
    //         data: {
    //             txt:text,
    //         },
    //         success: function (response) {
    //             $('.table-body').empty();
    //             $('.table-body').append(response);
    //         }
    //     });
    // });
    


    // $('#app_type').change(function (e) { 
    //     e.preventDefault();
    //     var app_type=$(this).val();
        
    //     console.log(app_type);
    //     $.ajax({
    //         url: "php/sort-appointment.php",
    //         data: {
    //             type:app_type,

    //         },
    //         success: function (response) {
    //             console.log(response);
               
    //         }
    //     });

    // });
</script>
<?php include 'pages/common-js.php';?>
