<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="/core/branch/" class="brand-link d-flex align-items-center">
      <img src="../image/logo.png" alt="Blood Blister Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <div class="row pl-2">
        <span class="brand-text"><?php echo $branch['b_name'];?></span><br>
        <span class="brand-text font-weight-light w-100" style="font-size: 17px;">Branch</span>
      </div>
    </a>

  <!-- Sidebar -->
  <div class="sidebar">

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
        <li class="nav-item">
          <a href="/core/branch/" class="nav-link">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
              Dashboard
              <!-- <i class="right fas fa-angle-left"></i> -->
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="index.php?page=profile" class="nav-link">
            <i class="nav-icon fas fa-user-alt"></i>
            <p>
             Profile
              <!-- <i class="right fas fa-angle-left"></i> -->
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-calendar-check"></i>
            <p>
            Appointment
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview" style="display: none;">
           <!--  <li class="nav-item">
              <a href="/core/branch/index.php?page=appointment" class="nav-link ">
                <p class="pl-3">Appointment list</p>
              </a>
            </li> -->
            <li class="nav-item">
              <a href="index.php?page=appointment/requested-appointment" class="nav-link">
                <p class="pl-3">Requested Appointment</p>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item">
          <a href="/core/branch/index.php?page=bloodstock" class="nav-link">
            <img src="../image/icons8-blood-donation-64.png" width="25px">
            <p>
              Blood Stock
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="/core/branch/index.php?page=payment" class="nav-link">
           <i class="fas fa-rupee-sign nav-icon"></i>
            <p>
              Payment
            </p>
          </a>
        </li>
        <li class="nav-item">
            <a href="/core/logout.php" class="nav-link">
            <i class="fas fa-sign-out-alt nav-icon" aria-hidden="true"></i>
            <p>
              Logout
              </p>
            </a>
          </li>

        


    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>