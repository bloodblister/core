<?php 
    if(!isset( $_SESSION['email']))
    {
		?>
			<script>
				window.location = "login.php";
			</script>
		<?php
    }
?>

<!-- online request -->
<div class="container-fluid row" >
	<h5 class="text-center w-100 text-light" style="background: #bb0a1e;margin: 0; padding: 5px 0;font-weight:600;letter-spacing:1px; "><i class="fas fa-hand-holding-water"></i>&nbsp; Online Donation Request</h5>
</div>
<!-- end online request -->

<!-- =========== Appointment form ============ -->
<?php 
    if(isset($row['r_ads2']))
    {
?>

<div class="container row" id="thalassemia">
	<div class="col-12  col-xl-11">
		<h5 class="text-left">Donor Details</h5>
		<?php include 'page/appointment-form.php'?>

	</div>
</div>

<?php }
else{
	?>
	<div class="container row" id="thalassemia">
		<div class="col-12  col-xl-11">
			<h5 class="border-none">Please update your profille </h5>
		</div>
	</div>
	<?php 
 } ?>
<!-- ========================================= -->
<!-- =================================== -->
