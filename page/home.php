
	<!-- =================================== -->
	<!-- image slider -->

	<div class="container-fluid row">
		<div class="col-12 col-xl-12">
			<div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
			  <div class="carousel-inner" id="slider">
			   <div class="carousel-item active">
			      <img class="d-block w-100" src="image/slider-1.jpg" alt="First slide">
			        <div class="carousel-caption ">
				    <h5>Save a Live <br>Donate Blood</h5>
				    <p>Ope pint of Blood can save the live</p>
				    <button class="button" onclick="window.location='index.php?page=Donation-Request&&appointment_type=1'">Donate Now</button>
				  </div>
			    </div>
			    <div class="carousel-item">
			      <img class="d-block w-100" src="image/slider-2.jpg" alt="Second slide">
			    	<div class="carousel-caption ">
			    		<h5>Your blood donation mater.<br> Give Today!</h5>
			    		<p>All types of blood are needed to help patients</p>
			    		<button class="button">Make an Appointment</button>
			    	</div>
			    </div>
			  </div>
			</div>
		</div>
	</div>
	<!-- =========== camps ============= -->
	<div class="container row" id="camps" data-aos="fade-up">
		<div class="col-6 col-xl-2" data-aos="flip-left" >
			<a href="#camp-section">
				<div class="camps-box">
					<div class="blood-wave"></div>
					<h3><i class="far fa-clock"></i></h3>
					<p>Active camps</p>	
				</div>
			</a>
		</div>

		<div class="col-6 col-xl-2" data-aos="flip-right" data-aos-delay="300">
			<a href="index.php?page=Search-Blood-Bank">
				<div class="camps-box">
					<div class="blood-wave"></div>
					<h3><img src="image/blood-bank.png" width="40px" height="50px"></h3>
					<p>Search Center</p>	
				</div>
			</a>
		</div>

		<div class="col-6 col-xl-2" data-aos="flip-left" data-aos-delay="600">
			<a href="index.php?page=Donation-Request&&appointment_type=1">
				<div class="camps-box">
					<div class="blood-wave"></div>
					<h3><img src="https://img.icons8.com/ios-filled/46/000000/drop-of-blood.png"/></h3>
					<p>Donate Now</p>	
				</div>
			</a>
		</div>

		<div class="col-6 col-xl-2" data-aos="flip-right" data-aos-delay="900" >
			<a href="index.php?page=Blood-bottle-Request&&appointment_type=0">
				<div class="camps-box">
					<div class="blood-wave"></div>
					<h3><i class="fas fa-hand-holding-water"></i></h3>
					<p>Request Now</p>	
				</div>
			</a>
		</div>
	</div>

<!-- ============end camps==========  -->

<!-- ================ services =============== -->
<!-- 
<div class="container-fluid" id="services-bg">
	<div class="container row" data-aos="fade-up">
		<h3 style=>Services</h3>
		<div class="col-12 col-xl-3" data-aos="fade-right">
			<div id="services-box">
					<h5>BLOOD AVAILABILITY</h5>
					<div id="services-box-icon">
							<i class="fas fa-tint"></i>
					</div>
					<p>Get status of available blood stock in blood banks</p>
						<button class="services-box-button " onclick="window.location.href='#'">Check Blood Availability</button>	
			</div>
		</div>
		<div class="col-12 col-xl-3" data-aos="fade-left" >
			<div id="services-box">
					<h5>CAMP SCHEDULE</h5>
					<div id="services-box-icon">
							<i class="fas fa-clock"></i>
					</div>
					<p>View and register for Blood Donation camps</p>
						<button class="services-box-button " onclick="window.location.href='#'">Register Now</button>	
			</div>
		</div>
		<div class="col-12 col-xl-3" data-aos="fade-right" >
			<div id="services-box">
					<h5>NEARBY BLOOD BANKS</h5>
					<div id="services-box-icon">
							<i class="fas fa-map-marker-alt"></i>
					</div>
					<p>Get contact and navigation details of blood banks	</p>
						<button class="services-box-button " onclick="window.location.href='#'">Donate Now</button>	
			</div>
		</div>
		<div class="col-12 col-xl-3" data-aos="fade-left">
			<div id="services-box">
					<h5>MY DONATIONS</h5>
					<div id="services-box-icon">
							<i class="fas fa-user"></i>
					</div>
					<p>Register, track and maintain your profile</p>
						<button class="services-box-button" onclick="window.location.href='#'">Start Now</button>
			</div>
		</div>
	</div>
</div>
 -->

<!-- ================ end services =============== -->
<!-- ================= important day ============= -->
<div class=" container-fluid row" id="important-day">
	<div class="col-12 col-xl-6" id="notice">
		<div>
				<h3>Donate blood on some important days and Save life </h3>
				<p>view the guidelines & term and condition</p>
		</div>
	</div>
	<div class="col-12 col-xl-6" id="day">
		<div>
			<h3>Important Day</h3>
			<i class="fas fa-angle-up" id="day-prev"></i>
			<ul>
				<li id="day-1">
					<span><img src="image/whealthDay.jpg"></span>
					 World Health Day - 7 April
				</li>
				<li id="day-2">
					<span><img src="image/hemophilia.png"></span>
					 World Hemophilia Day - 17 April
				</li>
				<li id="day-3">
					<span><img src="image/Thalassemia.png"></span>
					 World Thalassemia Day - 8 May
				</li>
				<li id="day-4">
					<span><img src="image/donationDay.png"></span>
					 World Donation Day - 14 June
				</li>
				<li id="day-5">
					<span><img src="image/voluntary.jpg"></span>
					 World Voluntary Donation Day - 1 October
				</li>
			</ul>
			<i class="fas fa-angle-down" id="day-next"></i>
		</div>
	</div>
</div>

<!-- =================== End Important Day ========== -->



<!-- ============= impact =========== -->
<!-- 	<div class="container-fluid row" id="impact">
		<div class="col-12 col-xl-6"> 
			<img src="image/image-1.jpg"	>
		</div>
		<div class="col-12 col-xl-6" id="impact-work">
			<p style="color: #4286f4;letter-spacing: 1px;">Make an impact</p>
			<h4>Who We Are</h4>
			<p id="impact-pergraph">
				Your financial gift can help a thousand children who struggle from illnesses. Your gift can provide the necessary medicine, treatment, and other essentials.
			</p>
			<button class="button" style="margin-top: 30px;">Donate Now</button>
		</div>
	</div>
 -->
<!-- ============== involved =========== -->
	<!-- <div class="container-fluid" id="involved">
			<p class="text-center" style="color: #4286f4;letter-spacing: 1px;">GET INVOLVED</p>
			<h2 class="text-center" >Ways to help</h2>	
		<div class="container-fluid d-flex">
			<div class="col-12 col-xl-12" id="line">
				<div>
				</div>
			</div>
		</div>
		
		<div class="container-fluid row">
				<div class="col-12 col-xl-4">
					<div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
					  <div class="carousel-inner" id="involved-part">
					   <div class="carousel-item active">
					      <img class="d-block w-100" src="image/image-2.jpg" alt="First slide">
					        <div class="carousel-caption ">
						    <h5>Donate Blood</h5>
						    <p>Donate blood today. Help save lives.</p>
						    <button class="button" >Learn more</button>
						  </div>
					    </div>
					  </div>
					</div>
				</div>
				<div class="col-12 col-xl-4">
					<div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
					  <div class="carousel-inner" id="involved-part">
					   <div class="carousel-item active">
					      <img class="d-block w-100" src="image/image-3.jpg" alt="First slide">
					        <div class="carousel-caption ">
						    <h5>Volunteer Team</h5>
						    <p>We are always looking for your help.	</p>
						    <button class="button">Become a Volunteer</button>
						  </div>
					    </div>
					  </div>
					</div>
				</div>
				<div class="col-12 col-xl-4" id="last-image-involved">
					<div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
					  <div class="carousel-inner" id="involved-part">
					   <div class="carousel-item active">
					      <img class="d-block w-100" src="image/image-4.jpg" alt="First slide">
					       <div class="carousel-caption ">
							    <h5>Donate Money</h5>
							    <p>Your financial gift can help people who <br>need it most.</p>
							    <button class="button last-button">Donate Now</button>
						  	</div>
					    </div>
					  </div>
					</div>
				</div>				
	</div>
</div> -->
<!-- ======================== -->
<!-- involved - get -->
	<div class="container-fluid row m-auto mt-5 p-0" id="involved-get">
		<div class="col-12 col-xl-6" data-aos="fade-right">
			<div class="get">
				<p class="p" style="color: #4286f4;letter-spacing: 1px;">GET INVOLVED</p>
				<h4>How can i help you.. ?</h4>
				<p class="mt-4">
					Blood Center is public donation center with blood donation members in the changing health care system. Founded in 1978, Blood Center is one of the nation’s oldest and largest nonprofit transfusion medicine organizations. We provide a blood and volunteer services across the US. With our national footprint, deep community roots and specialized services, we are the thread that connects people and resources together to fuel progress in transfusion medicine.
				</p>
				<div class="container-fluid row m-auto">
					<div class="col-xl-6 m-0">
						<ul>
							<li>
								Inspiring people to give blood
							</li>
							<li>
								Specialist blood donors and clinical supervision.
							</li>
							<li>
								Producing a safe and ready blood supply
							</li>
						</ul>
					</div>
					<div class="col-xl-6 m-0">
						<ul>
							<li>
								Increasing communication with our members.
							</li>
							<li>
								High quality assessment, diagnosis and treatment.
							</li>
							<li>
								Offering specialized patient services and pharmaceuticals
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-12 col-xl-6 w-100" style="overflow:hidden;">
			<div class="image" data-aos="fade-left">
			</div>
		</div>
	</div>

	<!-- ================== -->
<?php

	$camp_sql="SELECT camp.* FROM camp WHERE '".date('Y-m-d')."' BETWEEN camp.c_start_date AND camp.c_end_date" ; 
    $camp_run=$con->query($camp_sql);
	//row count
	$camp_count=$camp_run->num_rows;
?>


	<?php if($camp_count>0){ ?>
	<!-- = camp = -->
	<div class="camp container" id="camp-section">
		<div class="row">
			<div class="col-12">
				<h1 class="camp-heading">Active Camp</h1>
			</div>
			<?php
			while($camp=$camp_run->fetch_assoc())
			{
				// print_r($camp);
			?>
				<div class="col-md-4 row m-auto">
					<div class=" col-11 m-auto camp-box">
						<div class="camp-image">
							<img src="image/camp/<?php echo $camp['c_image'];?>" alt="" class="w-100">
							<div class="camp-overlay">
								<button class="button camp-overlay-visit-btn">Visit</button>
							</div>
						</div>
						<div class="camp-details">
							<h3 class="camp-title"><?php echo $camp['c_title'];?></h3>
							<p><?php echo substr($camp['c_details'],0,50);?>...</p>
							<div class="">
								<button class="button camp-visit-btn">visit now</button>
							</div>
						</div>
					</div>
				</div>
			<?php
			}
			?>
		</div>
	</div>
	<?php } ?>
	<!-- = end camp = -->


	<!-- =========== donation-appointmen =============-->
<div class="container-fluid row" id="donation-appointmen">
	<div class="col-12 col-xl-5" data-aos="slide-right">
		<div id="donation-side-image" style="overflow: hidden;">
			<div data-aos="slide-left" data-aos-delay="1000" data-aos-duration="900">
				<h3>Donation Campaign	</h3>
				<p>Campaigns to encourage new donors to join and existing to continue to give blood.</p>
			</div>
		</div>
	</div>
	<div class="col-12 col-xl-7" >
			<div id="donation-appointmen-form" style="overflow:hidden;">
					<h3>
						World Blood Donate Day
					</h3>
					<p>
						Every year, on 14 June, countries around the world celebrate World Blood Donor Day. The event serves to raise awareness of the need for safe blood and blood products and to thank blood donors for their life-saving gifts of blood.
					</p>
					<!-- <div class="container-fluid row">
						<div class="col-12 col-xl-6">
							<ul>
								<li>
									<span><i class="far fa-map"></i> Location:</span> 
										<p>
											Transport ltd. inc. 300, Pennsylvania Ave NW, Washington
										</p>
								</li>
									<li>
									<span><i class="far fa-calendar-alt"></i> Date:</span> 
										<p>
											14 June 21
										</p>
								</li>
									<li>
									<span><i class="far fa-clock"></i> Time:</span> 
										<p>
											09:00 AM - 08:00 PM
										</p>
								</li>
							</ul>
						</div>
						<div class="col-12 col-xl-6">
							<h5>Register today</h5>
							<form class="form" data-aos="slide-left">
									<input type="text" name="name" class="input-group form-control" placeholder="your name*" required>
									<input type="email" name="email" class="form-control" placeholder="Email*" required>
									<input type="tel" name="mobileno" class="form-control" placeholder="Phone" required>
									<button class="button">Make an Appointmen</button>
							</form>
						</div>
					</div> -->
			</div>
	</div>
</div>

<!-- ========================== -->
<!-- feedback -->
<!-- 
<div class="container-fluid row" id="feedback" style="margin-top:40px">
	<div class="col-12 col-xl-3" data-aos="zoom-in-up">
		<span><i class="fas fa-quote-left"></i></span>
		<p>
			People here are friendly every time! Very thorough. Always checking on you to make sure you are ok.
		</p>
		<h5 class="text-right">
			ANDY SIMPSON
		</h5>
	</div>
	<div class="col-12 col-xl-3" data-aos="zoom-in-up" >
		<span><i class="fas fa-quote-left"></i></span>
		<p>
			People here are friendly every time! Very thorough. Always checking on you to make sure you are ok.
		</p>
		<h5 class="text-right">
			ANDY SIMPSON
		</h5>
	</div>
	<div class="col-12 col-xl-3" data-aos="zoom-in-up" >
		<span><i class="fas fa-quote-left"></i></span>
		<p>
			People here are friendly every time! Very thorough. Always checking on you to make sure you are ok.
		</p>
		<h5 class="text-right">
			ANDY SIMPSON
		</h5>
	</div>
</div> -->
<div class="container-fluid row" id="schedule-appointmen" style="overflow:hidden;">
	<div class="col-12 col-xl-12" data-aos="zoom-out-down" >
		<div>
			<h2>Become a Blood Donor</h2>
			<p>All types of blood are needed to help patients</p>
			<button class="button" onclick="window.location.href='index.php?page=Donation-Request&&appointment_type=1'">Donate Now</button>
		</div>
	</div>
</div>