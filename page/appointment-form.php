
<form class="form" id="thalassemia-form" method="POST" action="php/save-appointment.php" data-parsley-validate>	
    <input type="hidden" name="appointment_type" value="<?php echo $_REQUEST['appointment_type'];?>">
    <input type="hidden" name="user_id" value="<?php echo $row['r_id'];?>">
    <div class="container-fluid row">
        <div class="col-md-4 form-group">
            <label class="form-control-label">First Name</label>
            <input type="text" name="tlsm_fname" class="input-group form-control" value="<?php echo $row['r_firstname'];?>" readonly required>
        </div>
        <div class="col-md-4 form-group">
            <label class="form-control-label">Last Name</label>
            <input type="text" name="tlsm_lname" class="input-group form-control" value="<?php echo $row['r_lastname'];?>" readonly required>
        </div>
        <div class="col-md-4 form-group">
            <label class="form-control-label">Email</label>
            <input type="email" name="tlsm_ads" class="input-group form-control" value="<?php echo $row['email'];?>" readonly required>
        </div>
        <div class="col-md-4 form-group">
            <label class="form-control-label">Mobile No.</label>
            <input type="tel" name="tlsm_mobile" class="input-group form-control" value="<?php echo $row['r_contect'];?>" readonly required>
        </div>
        <?php if($_REQUEST['appointment_type'] == 0)
        {
        ?>
        <div class="col-md-4 form-group">
            <label class="form-control-label">Patient Name</label>
            <input type="text" name="patient_name" class="input-group form-control" required>
        </div>		
        <?php 
        }
        ?>
        <!-- <div class="col-md-4 form-group">
            <label class="form-control-label">Date of Brith</label>
            <input type="Date" name="tlsm_dob" class="input-group form-control" required>
        </div> -->

        <div class="col-md-4 form-group">
            <label class="form-control-label">Pincode</label>
            <input type="number" name="pincode" class="input-group form-control" required>
        </div>
        <?php 

        $state_sql="SELECT * FROM `state` ORDER BY `state_title`";
        $state_run=$con->query($state_sql);
    
        ?>
        <div class="col-md-4 form-group" id="state_div">
            <label class="form-control-label">State</label>
            <select class="form-control" name="state" readonly >
                    <option value="S" selected>--- Select State ---</option>
                    <?php
                        while($state=$state_run->fetch_assoc())
                        {
                            echo "<option value='{$state['state_id']}' required>{$state['state_title']} </option>";
                        }
                    ?>
            </select>
        </div>
        <div class="col-md-4 form-group" id="dist_div">
            <label class="form-control-label">District</label>
            <select class="form-control" name="dist"  disabled>
                    <option value="" selected >--- Select District ---</option>
            </select>
        </div>
        <div class="col-md-4 form-group" id="city_div">
            <label class="form-control-label">City</label>
            <select class="form-control" name="city"  disabled>
                    <option value="" selected>--- Select city ---</option>
            </select>
        </div>

        <?php 

            if(isset($_REQUEST['hospital_id']))
            {
                $h_sql="SELECT `h_name`,'h_id' FROM `hospital` where `h_id`=".$_REQUEST['hospital_id'];
                $h_run=$con->query($h_sql) or die('Fial to fetch hospital name');
                $hospital_name=$h_run->fetch_assoc();

                $cen_sql="SELECT `cen_id` FROM `centers` WHERE `h_id`=".$_REQUEST['hospital_id'];
                $cen_run=$con->query($cen_sql) or die('Fial to fetch hospital name');
                $cenid=$cen_run->fetch_assoc();
                ?>
                     <div class="col-md-4 form-group">
                        <label class="form-control-label">Hospital Name </label>
                        <input type="text" class="form-control" value="<?php echo $hospital_name['h_name'];?>" readonly>
                        <input type="hidden" name="center_id" value="<?php echo $cenid['cen_id'];?>">
                    </div>
                <?php
            }

            if(isset($_REQUEST['branch_id']))
            {
                $b_sql="SELECT `b_name` FROM `branch` where `b_id`=".$_REQUEST['branch_id'];
                $b_run=$con->query($b_sql) or die('Fial to fetch branch name');
                $branch_name=$b_run->fetch_assoc();

                $cen_sql="SELECT `cen_id` FROM `centers` WHERE `b_id`=".$_REQUEST['branch_id'];
                $cen_run=$con->query($cen_sql) or die('Fial to fetch hospital name');
                $cenid=$cen_run->fetch_assoc();
                ?>
                     <div class="col-md-4 form-group">
                        <label class="form-control-label">Branch Name </label>
                        <input type="text" class="form-control" value="<?php echo $branch_name['b_name'];?>" readonly>
                        <input type="hidden" name="center_id" value="<?php echo $cenid['cen_id'];?>">
                    </div>
                <?php
            }
    
        ?>
        <?php if(!isset($_REQUEST['branch_id']) && !isset($_REQUEST['hospital_id']))
            {
            ?>
            <div class="col-md-4 form-group" id="center_div">
                <label class="form-control-label">Select Center </label>
                <select class="form-control" name="center_id" required disabled>
                        <option value="" selected required>Select Center</option>
                </select>
            </div>
            <?php
            }
        ?>
        <?php 

            $blood_sql="SELECT * FROM `blood`";
            $blood_run=$con->query($blood_sql);
            
        ?>

        <div class="col-md-4 form-group">
            <label class="form-control-label">Blood Group</label>
            <select class="form-control" name="blood_group" required>
                <option value="" selected required>Select Blood group</option>
            <?php
        
            while($blood=$blood_run->fetch_assoc())
            {    

                if($blood['blood_type'] == 0)
                {
                    $type='-';
                }
                if($blood['blood_type'] == 1)
                {
                    $type='+';
                }
            ?>
                <option value="<?php echo $blood['blood_id'];?>" required><?php echo $blood['blood_name'].$type;?></option>
            <?php 
            }
            ?>
            </select>
        </div>
        <div class="col-md-4 form-group" id="component">
            <label class="form-control-label">Component</label>
            <select class="form-control" name="component" disabled required>
                    <option value="" selected>Select Component</option>
            </select>
        </div>
        <?php if($_REQUEST['appointment_type'] == 0)
        {
        ?>

        <div class="col-md-4 form-group">
            <label class="form-control-label">Requirement Date</label>
            <input type="date" id="date" name="visiting_date" required class="input-group form-control">
        </div> 
        <div class="col-md-4 form-group">
            <label class="form-control-label">Amount</label>
            <input type="text" name="amount" id="amount" class="input-group form-control" readonly >
        </div>
        <?php
        }
        ?>
         <?php if($_REQUEST['appointment_type'] == 1)
        {
        ?>
        <div class="col-md-4 form-group">
            <label class="form-control-label">Donation Date</label>
            <input type="date" id="date" name="visiting_date" required class="input-group form-control">
        </div>
        <?php
        }
        ?>
         <div class="col-md-4 form-group">
            <label class="form-control-label">Identity Proof </label>
            <select name="gov_id_name" class="form-control input-group" required id="">
                <option value="">--- select Indentity Proof ---</option>    
                <option value="Passport ">Passport </option>    
                <option value="Driving License ">Driving License </option>    
                <option value="Aadhaar Card ">Aadhaar Card </option>    
                <option value="PAN Card ">PAN Card </option>    
                <option value="Election Commission ID Card ">Election Commission ID Card </option>    
            </select>
        </div>
        <div class="col-md-4 form-group">
            <label class="form-control-label">Identity Proof Nubmer </label>
            <input type="number" name="gov_id_number" class="input-group form-control" required> 
        </div>
    </div>
    <?php 
        if($_REQUEST['appointment_type'] == 0)
        {
    ?>
    <div class="container-fluid row m-auto">
        <div class="col-md-2 m-auto " id="save">
            <a id="pay" class="btn text-white ">Pay Now</a>
           
        </div>
    </div>

    <?php } ?>
    <?php 
        if($_REQUEST['appointment_type'] == 1)
        {
    ?>
    <div class="container-fluid row m-auto">
        <div class="col-md-2 m-auto ">
            <button class="btn text-white">Submit</button>
           
        </div>
    </div>

    <?php } ?>
</form>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>

<script>

$('body').on('click', '#pay', function(e){



    if($("#thalassemia-form").valid())
 {

   var totalAmount = $('#amount').val();
   var options = {
   "key": "rzp_test_bbZQs476brAHkl",
   "amount": (totalAmount*100), // 2000 paise = INR 20
   "name": "Blood Bliseter",
   "description": "Make Payment",
   "image": "image/logo.png",
   "handler": function (response){
       $('#save').empty();
       $('#save').append('<input type="hidden" name="tr_id" value="'+response.razorpay_payment_id+'">');
       $('#save').append('<button type="submit" id="save" name="tlsm_submit" class="btn button w-100">Save</button>');
         $("#thalassemia-form").submit();
   },

   "theme": {
       "color": "#f89b64"
    }
   };
   var rzp1 = new Razorpay(options);
   rzp1.open();
   e.preventDefault();
   
 
   }
});

 $('#date').change(function(e){
    var date = $(this).val();
    var today = "<?php echo date('Y-m-d'); ?>";
    if (date<today)
    {
        $(this).parent().children('p').remove();
        $(this).parent().append("<p class='text-danger'>Invalid Date</p>");
        $(this).val('');
        // alert("Please enter valid date");
    }
    else{
        $(this).parent().children('p').remove();
    }
 });
 $("input[name='pincode'").keyup(function(e) {
    var pin = $(this).val();
    // console.log(pin.length);

    if (pin.length == 6) {
        get_pincode(pin);
    }
});

    
function get_pincode(pin) {
    try {
        var url = "https://api.postalpincode.in/pincode/" + pin;
        var state, dist, city;
        $.ajax({
            type: "GET",
            url: url,
            success: function(response) {
                try {
                    $.each(response, function(key, value) {
                        $.each(value.PostOffice, function(p_key, p_value) {
                            state = p_value.State;
                            dist = p_value.District;
                            city = p_value.Division;
                        });
                    });
                } catch (err) {
                    console.log('Ferror');
                }

                // $("input[name='state']").val(state);
                $.ajax({
                    type: "POST",
                    url: window.location.origin + '/core/php/city&dist.php',
                    data: {
                        state: state,
                        dist,
                        dist
                    },
                    success: function(data) {
                        try {
                            $('.pin-error').empty();
                            var details = $.parseJSON(data);
                            $("#state_div").empty().append(details.state);
                            $("#dist_div").empty().append(details.dist);
                            $("#city_div").empty().append(details.city);
                            $("#state_div select").addClass('form-control');
                            $("#dist_div select").addClass('form-control');
                            $("#city_div select").addClass('form-control');

                        } catch (err) {
                            $('.pin-error').empty().append('Pinc    ode not found').css('color', 'red');
                        }
                    }
                })
            }

        });
    } catch (err) {
        console.log(err);
        alert(err);
    }

}

$("select[name='blood_group']").change(function (e) { 
    e.preventDefault();
    var blood_id=$(this).val();
    $.ajax({
        url: "php/get_component.php",
        data: {
            blood_id:blood_id,
        },
        success: function (response) {
            console.log(response);
            if(response != 0)
            {
                $('#component').empty();
                $('#component').append(response);
            }
        }
    });
});

<?php 

if($_REQUEST['appointment_type'] == 0)
{
    ?>

    
$('body').on('change', "select[name='component']", function(e){

    e.preventDefault();
    var comp=$(this).val();
    var center=$("select[name='center_id']").val();

    <?php
    if(isset($_REQUEST['branch_id']) || isset($_REQUEST['hospital_id']))
    {
    ?>
    center=$("input[name='center_id']").val();
    <?php
    }
    ?>
    console.log(center);
    console.log(center === '');
    console.log(center !== '');
    if(center !== '')
    { 
        $.ajax({
            type: "post",
            url: "php/check_availability.php",
            data: {
                center:center,
                comp_id:comp
            },
            success: function (response) {
                
                if(response >= 1)
                {
                    $.ajax({
                        type: "post",
                        url: "php/get_component_details.php",
                        data: {
                            id:comp
                        },
                        success: function (response) {
                            var details = $.parseJSON(response);
                            $('#amount').val(details.component_price)
                            $("#component p").remove();
                    
                        }
                    });
                }
                else{
                    $('#amount').val('');
                    $("#component p").remove();
                    $("#component").append("<p class='text-danger'>This Component is not availabile on this Center</p>");
                }


            }
        });
    }
    else{
        $("#component p").remove();
        $("#component").append("<p class='text-danger'>Please Select Center</p>");
    }

    
});

<?php
}
?>


$("select[name='state']").change(function (e) { 
    e.preventDefault();
    var state=$(this).val();
    $.ajax({

        url: "php/dist.php",
        data: {
            state_id:state,
        },
        success: function (response) {
            $('#dist_div').empty();
            $('#dist_div').append(response);
        }
    });
});

$( "#dist_div" ).on( "change", "select[name='dist']", function(e){

    e.preventDefault();
    var dist=$(this).val();
    // console.log(dist);
    $.ajax({

        url: "php/city.php",
        data: {
            districtid:dist,
        },
        success: function (response) {
            $('#city_div').empty();
            $('#city_div').append(response);
              
        }
    });
});


$( "#city_div" ).on( "change", "select[name='city']", function(e){
    e.preventDefault();
    var city=$(this).val();
    $.ajax({
        url: "php/get_center.php",
        data: {
            city_id:city,
        },
        success: function (response) {
            $('#center_div').empty();
            $('#center_div').append(response);
        }
    });
});


</script>



