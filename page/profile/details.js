
$("#profile-file-input").change(function (e) { 
    e.preventDefault();
    if ($(this).val() != "")
    {
        $(".profile-overlay").css('display','flex');
        $('body').attr('style','margin: 0; height: 100%; overflow: hidden;');
        readURL(this);
    }
});

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('.profile-image-upload').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}


$("#profile-image-form").submit(function(e) {
    e.preventDefault();
    // console.log($("#profile-image-form").serialize());
    $.ajax({
        type: "POST",
        url: "php/profile_image.php",
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        success: function(response) {
            console.log(response);
            if (response != 0) {
                $('.profile-image').attr('src', 'image/profile/' + response);

            } else {
                alert('There is some thing wrong');
            }
            $(".profile-overlay").css('display','none');
            $('body').attr('style','');
            $('.profile-image-upload').attr('src', '');

        }
    });
});


$("#profile-form").submit(function(e) {
    e.preventDefault();
    $.ajax({
        type: "POST",
        url: "php/profile_save.php",
        data: $("#profile-form").serialize(),
        success: function(data) {
            if (data == 1) {
                window.location = "";
            }

        }
    });
});
$(document).click((event) => {
    if (!$(event.target).closest('.profile-image-box').length) {
        $(".profile-overlay").css('display','none');
        $('body').attr('style','');
        $('.profile-image-upload').attr('src', '');

    }
});


$("input[name='pincode'").keyup(function(e) {
    var pin = $(this).val();
    // console.log(pin.length);

    if (pin.length == 6) {
        get_pincode(pin);
    }
});

function get_pincode(pin) {
    try {
        var url = "https://api.postalpincode.in/pincode/" + pin;
        var state, dist, city;
        $.ajax({
            type: "GET",
            url: url,
            success: function(response) {
                try {
                    $.each(response, function(key, value) {
                        $.each(value.PostOffice, function(p_key, p_value) {
                            state = p_value.State;
                            dist = p_value.District;
                            city = p_value.Division;
                        });
                    });
                } catch (err) {
                    console.log('Ferror');
                }

                // $("input[name='state']").val(state);
                $.ajax({
                    type: "POST",
                    url: window.location.origin + '/core/php/city&dist.php',
                    data: {
                        state: state,
                        dist,
                        dist
                    },
                    success: function(data) {
                        try {
                            $('.pin-error').empty();
                            var details = $.parseJSON(data);
                            $("#state_div").empty().append(details.state);
                            $("#dist_div").empty().append(details.dist);
                            $("#city_div").empty().append(details.city);
                        } catch (err) {
                            $('.pin-error').empty().append('Pinode not found').css('color', 'red');
                        }
                    }
                })
            }

        });
    } catch (err) {
        console.log(err);
        alert(err);
    }

}


$("select[name='state']").change(function (e) { 
    e.preventDefault();
    var state=$(this).val();
    $.ajax({

        url: "php/dist.php",
        data: {
            state_id:state,
        },
        success: function (response) {
            $('#dist_div').empty();
            $('#dist_div').append(response);
            $('#dist_div select').removeClass('form-control');
            $('#city_div select option').slice(1).remove();
        }
    });
});

$( "#dist_div" ).on( "change", "select[name='dist']", function(e){

    e.preventDefault();
    var dist=$(this).val();
    console.log(dist);
    $.ajax({

        url: "php/city.php",
        data: {
            districtid:dist,
        },
        success: function (response) {
            $('#city_div').empty();
            $('#city_div').append(response);
            $('#city_div select').removeClass('form-control');
        }
    });
});
