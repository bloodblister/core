<style>
.appointment-details-box {
    height: 0;
    padding: 0px 0px;
    background-color: #ffac9c3b;
    overflow: hidden;
}

.appointment-download {
    position: absolute;
    right: 13px;
    bottom: 15px;
    background-color: #e43b1a;
    padding: 3px 7px;
    outline: none;
    color: white;
    border: none;
}

.appointment-details-box p {
    color: #e43b1a;
    font-weight: 800;
}

.appointment-details-box p span {
    color: #656565;
    font-weight: 400;
}
</style>
<div class="row">
    <div class="col-12">

        <?php


        $appointment_sql="SELECT appointment.*,registration.r_id,blood_component.*,blood.*,centers.*,city.name FROM appointment INNER JOIN registration ON registration.r_id = appointment.r_id INNER JOIN blood_component ON blood_component.component_id =appointment.component_id INNER JOIN blood on blood.blood_id = blood_component.blood_id INNER JOIN centers on appointment.center_id =centers.cen_id INNER JOIN city on appointment.city_id =city.id  where appointment.r_id=".$row['r_id'];
        $appointment_run=$con->query($appointment_sql);
        
        // print_r($appointment_run->fetch_assoc());
        
        while($appointment=$appointment_run->fetch_assoc())
        {
            ?>

        <div class="row  appointment-box">
            <div class="col-12 row appointment-title-div" id="appointmenr-title-<?php echo $appointment['a_id'];?>"
                data-id="<?php echo $appointment['a_id'];?>">
                <div class="col-11">
                    <?php
                        if($appointment['a_type'] == 0)
                        {
                            echo "<h5>Request Appointment</h5>";
                        }
                        else{
                            echo "<h5>Donation Appointment</h5>";
                        }
                        ?>

                    <p>Date :<b>
                            <?php $date=date_create($appointment['created_at']); echo date_format($date,"d M, Y");?></b>
                    </p>
                </div>
                <div class="col-1 appointment-down-arrow p-0">
                    <i class="fas fa-caret-down"></i>
                    <i class="fas fa-sort-up d-none" style="padding-top: 11px;"></i>
                </div>
            </div>
            <?php 
                    if($appointment['h_id'] != '')
                    {
                        $appointment['h_id'];
                        $center_sql="SELECT * FROM `hospital` WHERE `h_id`=".$appointment['h_id'];
                        $center_run=$con->query($center_sql);
                        $center=$center_run->fetch_assoc();
                        $center_name=$center['h_name'];
                    }
                    if($appointment['b_id'] != '')
                    {
                        $center_sql="SELECT  `b_name` FROM `branch` WHERE `b_id`=".$appointment['b_id'];
                        $center_run=$con->query($center_sql);
                        $center=$center_run->fetch_assoc();
                        $center_name=$center['b_name'];
                    }

                  

                        
                ?>
            <div class="col-12 appointment-details-box appointment-details-box-<?php echo $appointment['a_id'];?>">
                <div class="row m-0 pl-3 pt-1">
                    <div class="col-6">
                        <p>Id : <span> BB000<?php echo $appointment['a_id'];?> </span></p>
                        <?php
                        if($appointment['a_type'] == 0)
                        {
                            echo "<p>Patient Name : <span>{$appointment['patient_name']} </span></p>";
                        }
                       
                        ?>
                        <p>Center Name :<span> <?php echo $center_name;?> </span></p>
                        <p>Center City : <span> <?php echo $appointment['name'];?> </span></p>
                        <div class="row">
                            <div class="col-md-12 col-xl-6">
                                <?php 
                                            $type='';
                                                if($appointment['blood_type'] == 0)
                                                {
                                                    $type='-';
                                                }
                                                if($appointment['blood_type'] == 1)
                                                {
                                                    $type='+';
                                                }
                                        ?>
                                <p>Blood Group :<span> <?php echo $appointment['blood_name'].$type ?></span></p>
                            </div>
                            <div class="col-md-12 col-xl-6">
                                <p>Compponent : <span> <?php echo $appointment['component_name'];?> </span></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">

                        <p class="text-right text-light ">
                            <?php 
                                        if($appointment['a_status'] == 0)
                                        {
                                            echo "<span class='pl-1 pr-1 bg-warning' style='background-color: #28a745;color:#fff'>Booked</span>";
                                        }
                                        else if($appointment['a_status'] == 1)
                                        {
                                            echo "<span class='pl-1 pr-1' style='background-color: #28a745;color:#fff'>completed</span>";
                                        }
                                        else{
                                            echo "<span class='pl-1 pr-1' style='background-color: rgb(162 26 0);color:#fff'>Canceled</span>";
                                        }
                                    ?>
                            <!-- <span class="pl-1 pr-1" style="background-color: #28a745;">completed</span> -->

                        </p>
                        <?php 
                            if($appointment['a_status'] == 0 || $appointment['a_status'] == 1 )
                            {?>
                        <a class="float-right appointment-download" href='/core/report.php?a_id=<?php echo $appointment['a_id'];?>' >Download</a>
                        <?php }?>

                    </div>
                </div>
            </div>
        </div>

        <?php
        }
?>




    </div>
</div>

<script>
    
$(".appointment-title-div").click(function() {
    var id = $(this).attr('data-id');
    $(this).css('background-color', '#e43b1a');
    $(this).css('color', 'white');
    if ($('.appointment-details-box-' + id).height() >= 50) {
        $('.appointment-details-box-' + id).css('border', 'none');
        $('.appointment-details-box-' + id).css('display', 'none');
        $(this).css('background-color', '#fcd1c9b3');
        $(this).css('color', '#e43b1a');
        $(this).css('font-weight', '700');
        $('.appointment-details-box-' + id).css({
            height: '0px'
        }, 1000);
        $('#appointmenr-title-' + id + ' .fa-caret-down').removeClass('d-none');
        $('#appointmenr-title-' + id + ' .fa-sort-up').addClass('d-none');

    } else {
        $(this).css('background-color', '#e43b1a');
        $(this).css('color', 'white');
        $(this).css('font-weight', '400');
        $('.appointment-details-box-' + id).css('border', '2px solid rgb(228, 59, 26)');

        $('.appointment-details-box-' + id).css('height', 'auto');
        $('#appointmenr-title-' + id + ' .fa-caret-down').addClass('d-none');
        $('#appointmenr-title-' + id + ' .fa-sort-up').removeClass('d-none');
        $('.appointment-details-box-' + id).css('display', 'block');

    }
});



</script>