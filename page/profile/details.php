<?php 

	// $sql="SELECT * FROM `registration` WHERE `email` = '".$_SESSION['email']."'";
	// $query=mysqli_query($con,$sql);
	// $row = mysqli_fetch_array($query);

    // print_r($row);
?>
 <div class="profile-overlay" >
        <div class="profile-image-box">
            <span onclick="$('#profile-file-btn').trigger('click');"> <i class="fas fa-check"></i> </span>
                <img src="" class="profile-image-upload">
            <form action="php/profile_image.php" id="profile-image-form" enctype="multipart/form-data" >
                <input type="hidden" name="id" id="profile-id"  value="<?php echo $row['r_id'];?>">
                <input type="file" name="image" class="d-none" id="profile-file-input" >
                <input type="submit" id="profile-file-btn" class="d-none">
            </form>     
        </div>
    </div>

<div class="row">
    <div class=" col-4 col-sm-3  col-xl-2 ">
        <div class="profile-box">
            <span class="edit-profile" onclick="$('#profile-file-input').trigger('click');"><i class="fas fa-edit"></i></span>
            <?php 
            if($row['r_profile'])
            {
                ?>
                <img src="image/profile/<?php echo $row['r_profile'];?>"  width="100%" height="80px" class="profile-image">
                <?php
            }else{
            ?>
            <img src="image/defualt.jpg"  width="100%" height="80px" class="profile-image">
                <?php } ?>

           
        </div>
    </div>
    <div class="col-8 col-sm-9 col-xl-10 p-0 ">
        <h3 class="user-name"><?php echo  $row['r_firstname'] . " ". $row['r_lastname'] ?></h3>
        <p class="user-tagline">Manage your personal information, password and more</p>
    </div>
    <div class="col-12 pl-3 mt-4">
        <h4 class="personal-info">Personal info</h4>
    </div>
</div>
<form action="#" id="profile-form" method="post">
<input type="hidden" name="id"
                <?php
                    if(isset($row['r_id']))
                    {
                        ?>
                        value="<?php echo $row['r_id'];?>"
                        <?php
                    }
                ?>>
    <div class="row profile-details">				
        <div class="col-md-6">
            <div class="input-box">
                <label>Firstname</label>
                <input type="text" name="firstname" class="profile-input" 
                <?php
                    if(isset($row['r_firstname']))
                    {
                        ?>
                        value="<?php echo $row['r_firstname'];?>"
                        <?php
                    }
                ?>
                required>
            </div>
        </div>
        <div class="col-md-6">
            <div class="input-box">
                <label>Lastname</label>
                <input type="text" name="lastname" class="profile-input"
                    <?php
                    if(isset($row['r_lastname']))
                    {
                        ?>
                        value="<?php echo $row['r_lastname'];?>"
                        <?php
                    }
                ?> required>
            </div>
        </div>
        <div class="col-md-6">
            <div class="input-box">
                <label>Email</label>
                <input type="email" name="email" class="profile-input"
                <?php
                    if(isset($row['email']))
                    {
                        ?>
                        value="<?php echo $row['email'];?>"
                        <?php
                    }
                ?>readonly required>
            </div>
        </div>
        <div class="col-md-6">
            <div class="input-box">
                <label>Contect</label>
                <input type="number" name="contect" class="profile-input" 
                <?php
                    if(isset($row['r_contect']))
                    {
                        ?>
                        value="<?php echo $row['r_contect'];?>"
                        <?php
                    }
                ?> required>
            </div>
        </div>
        <div class="col-md-6">
            <div class="input-box">
                <label>Gender</label>
                <select name="gender" id="">
                    <option value=""
                    <?php
                        if(!isset($row['r_gender']))
                        {
                            ?>
                            selected
                            <?php
                        }
                    ?>
                    >--- Select Gender ---</option>
                    <option value="male" class="profile-input"
                    <?php
                        if($row['r_gender'] == 'male')
                        {
                            ?>
                            selected
                            <?php
                        }
                    ?>
                    >Male</option>
                    <option value="female" class="profile-input"
                    <?php
                        if($row['r_gender'] == 'female')
                        {
                            ?>
                            selected
                            <?php
                        }
                    ?>
                    >Female</option>
                    <option value="other" class="profile-input" 
                    <?php
                        if($row['r_gender'] == 'other')
                        {
                            ?>
                            selected
                            <?php
                        }
                    ?>
                    >Other</option>
                </select>
                <!-- <input type="email" name="email" class="profile-input" required> -->
            </div>
        </div>
        <div class="col-md-6">
            <div class="input-box">
                <label>Date of Birth</label>
                <input type="date" name="dob" class="profile-input"
                <?php
                    if(isset($row['r_dob']))
                    {
                        ?>
                        value="<?php echo $row['r_dob'];?>"
                        <?php
                    }
                ?>
                required>
            </div>
        </div>
        <div class="col-md-6">
            <div class="input-box">
                <label>House No. <b>OR</b> Flat No. </label>
                <input type="text" name="ads1" class="profile-input"
                <?php
                    if(isset($row['r_ads1']))
                    {
                        ?>
                        value="<?php echo $row['r_ads1'];?>"
                        <?php
                    }
                ?>
                required>
            </div>
        </div>
        <div class="col-md-6">
            <div class="input-box">
                <label>Area</label>
                <input type="text" name="ads2" class="profile-input" 
                <?php
                    if(isset($row['r_ads2']))
                    {
                        ?>
                        value="<?php echo $row['r_ads2'];?>"
                        <?php
                    }
                ?>
                required>
            </div>
        </div>
        <div class="col-md-6">
            <div class="input-box">
                <label>Pincode</label>
                <input type="number" maxlength="6" name="pincode" class="profile-input" <?php
                    if(isset($row['r_pincode']))
                    {
                        ?>
                        value="<?php echo $row['r_pincode'];?>"
                        <?php
                    }
                ?> required>
            </div>
            <p class="pin-error"></p>
        </div>
        
        <!-- <div class="col-md-6">
            <div class="input-box">
                <label>state</label>
                
                <input type="state" name="state" class="profile-input" required>
            </div>
        </div> -->
        <?php 
             if(isset($row['city_id']))
             {  
                
              


                $city_sql="SELECT * FROM `city` WHERE `id`=".$row['city_id'];
                $city_result=$con->query($city_sql) or die('select city is fail');
                $city_data=$city_result->fetch_array();
                       
                $dist_sql="SELECT * FROM `district` WHERE `state_id`=".$city_data['state_id'];
                $dist_result=$con->query($dist_sql) or die('select district is fail');
            

                $city_all_sql="SELECT * FROM `city` WHERE `districtid`=".$city_data['districtid'];
                $city_all_result=$con->query($city_all_sql) or die('select city is fail');
               
            
               

             }
        ?>
        <div class="col-md-6">
            <div class="input-box" id="city_div">
                <label>City</label>
                <select name="city" id="">
                    <option value="">--- Select City ---</option>
                    <?php 
                    if(isset($row['city_id']))
                    {
                         while($city_all_data=mysqli_fetch_array($city_all_result))
                         {   
                             if($row['city_id'] == $city_all_data['id'])
                             {
                                 echo "<option value='".$city_all_data['id']."' selected>".$city_all_data['name']."</option>";    
                             }
                             else
                             {
                                 echo "<option value='".$city_all_data['id']."'>".$city_all_data['name']."</option>";    
                             }
                         }
                    }
                    ?>
                </select>
            
            </div>
        </div> 
        <div class="col-md-6">
            <div class="input-box" id="dist_div">
                <label>District</label>
                <select name="district" >
                    <option value="">--- Select District ---</option>	
                    <?php 
                    if(isset($row['city_id']))
                    {
                     while( $dist_data=$dist_result->fetch_array())
                     {   
                         if($city_data['districtid'] == $dist_data['districtid'])
                         {
                             echo "<option value='".$dist_data['districtid']."' selected>".$dist_data['district_title']."</option>";    
                         }
                         else
                         {
                             echo "<option value='".$dist_data['districtid']."'>".$dist_data['district_title']."</option>";    
                         }
                     }
                    }
                    ?>
                </select>
            </div>
        </div>
        <?php 
            $s_sql="SELECT * FROM `state`";
            $s_query=mysqli_query($con,$s_sql);	
        ?>
        <div class="col-md-6">
            <div class="input-box" id="state_div">
                <label>state</label>
                <select name="state" id="">	
                    <option >--- Select State ---</option>

                    <?php 
                    if(isset($row['city_id']))
                    {
                        while ($s_row = mysqli_fetch_array($s_query))
                         {   
                             if($city_data['state_id'] == $s_row['state_id'])
                             {
                                 echo "<option value='".$s_row['state_id']."' selected>".$s_row['state_title']."</option>";    
                             }
                             else
                             {
                                 echo "<option value='".$s_row['state_id']."'>".$s_row['state_title']."</option>";    
                             }
                         }
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="col-md-11 profile-submit">
            <input type="submit" name="submit" class="btn float-right">
        </div>
    </div>
</form>
