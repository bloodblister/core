<!-- profile start -->


<div class="container " id="profile-main-div">
	<div class="row m-auto">
		<?php
			@$profile_page=$_REQUEST['profile_page'];
			if(!isset($profile_page))
			{
				$profile_page='details';
			}
			include 'profile/sidemanu.php';
		?>
		<div class="col-md-9 pt-1 ">
			<?php
				
				if(isset($profile_page) && file_exists("page/profile/".$profile_page.".php"))
				{
					include "page/profile/".$profile_page.".php";
				}
				else{
					include "page/profile/details.php";
				}
			?>
		</div>
	</div>
</div>

<?php
	if(isset($profile_page) && file_exists("page/profile/".$profile_page.".js"))
	{
	?>
		<script src="page/profile/<?php echo $profile_page;?>.js"></script>
		
	<?php
	}

?>
<!-- profile end -->
