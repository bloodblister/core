    <!-- =============== give-blood image =========== -->
    <div class="container-fluid m-auto row">
        <div class="col-12 col-xl-12" id="blood-image">
            <div>
                <h4>Donate Blood</h4>
                <p>Patients need your help</p>
            </div>
        </div>
    </div>



    <!-- ==============  requirements -->
    <div class="container-fluid row " id="requirements">
        <div class="col-12 col-xl-6" id="requirements-detailes">
            <div>
                <h4 class="display-4">
                    Requirements by blood donation type
                </h4>
                <p>
                    These are some of the requirements donors must meet to be eligible to donate blood based on their donation type.
                </p>
                <p>
                    Blood Donation
                </p>
                <ul class="arrow">
                    <li>
                        Donation frequency: Every 56 days*
                    </li>
                    <li>
                        You must be in good health and feeling well**
                    </li>
                    <li>
                        You must be at least 16 years old in most states
                    </li>
                    <li>
                        You must weigh at least 110 lbs
                    </li>
                </ul>
                <p class="phoneno">
                    If you have any questions regarding your eligibility, please call us at
                    <a href="tel:+919099801380">+919099801380</a>
                </p>
                <div class="container-fluid row">
                    <div class="col-6 col-xl-6">
                        <button class="btn btn-outline-dark ">Learn more</button>
                    </div>
                    <div class="col-6 col-xl-6">
                        <button class="button">Make an Appointment</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-xl-6" id="requirements-image">
            <div>
            </div>
        </div>
    </div>
    <!-- ========= end Requirements -->
    <!-- =========== Donation process ============ -->

  <!--   <div class="container-fluid row" id="Donation-process">
        <div class="col-12 col-xl-5" id="Donation-process-image">
            <div></div>
        </div>
        <div class="col-12 col-xl-6">
            <div id="Donation-process-details">
                <h4>The Blood Donation Process</h4>
                <span class="registraion">
					<p>
						<i class="fas fa-plus"></i><i class="fas fa-minus"></i> Registraion
					</p>
				</span>
                <ul class="arrow" id="registraion">
                    <li>We’ll sign you in and go over basic eligibility.</li>
                    <li>You’ll be asked to show ID, such as your driver’s license.</li>
                    <li>You’ll read some information about donating blood.</li>
                    <li>We’ll ask you for your complete address.</li>
                </ul>

                <span class="health-history">
					<p>
						<i class="fas fa-plus"></i><i class="fas fa-minus"></i> Health History
					</p>
				</span>
                <ul class="arrow" id="health-history">
                    <li>Sed in urna vitae lorem egestas luctus. Donec posuere, ante vitae ullamcorper iaculis, nunc purus ornare erat, ornare ornare orci nunc vel nisi.</li>
                    <li>Vivamus maximus lobortis tellus, nec mollis neque egestas eleifend. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</li>
                    <li>Donec tristique eros viverra ullamcorper tempus. Ut eleifend mi sit amet neque efficitur, ac vestibulum elit convallis.</li>
                </ul>

                <span class="blood-donation">
					<p>
						<i class="fas fa-plus"></i><i class="fas fa-minus"></i> Your Blood Donation
					</p>
				</span>
                <ul class="arrow" id="blood-donation">
                    <li>Sed in urna vitae lorem egestas luctus. Donec posuere, ante vitae ullamcorper iaculis, nunc purus ornare erat, ornare ornare orci nunc vel nisi.</li>
                    <li>Vivamus maximus lobortis tellus, nec mollis neque egestas eleifend. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</li>
                    <li>Donec tristique eros viverra ullamcorper tempus. Ut eleifend mi sit amet neque efficitur, ac vestibulum elit convallis.</li>
                </ul>

                <span class="recovery">
					<p>
						<i class="fas fa-plus"></i><i class="fas fa-minus"></i> Refreshment and Recovery
					</p>
				</span>
                <ul class="arrow" id="recovery">
                    <li>Sed in urna vitae lorem egestas luctus. Donec posuere, ante vitae ullamcorper iaculis, nunc purus ornare erat, ornare ornare orci nunc vel nisi.</li>
                    <li>Vivamus maximus lobortis tellus, nec mollis neque egestas eleifend. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</li>
                    <li>Donec tristique eros viverra ullamcorper tempus. Ut eleifend mi sit amet neque efficitur, ac vestibulum elit convallis.</li>
                </ul>
            </div>
        </div>
    </div> -->
    <!-- ============= end Donation process =========== -->

    <!-- ============================================ -->

    <!-- ================= about-blood=============== -->
    <div class="container-fluid row " style="margin-top:12vh">
        <div class="col-12 col-xl-6" id="about-blood">
            <img src="image/donationFact.webp">
            <p>After donating blood, the body works to replenish the blood loss. This stimulates the production of new blood cells and in turn, helps in maintaining good health.</p>
            <button class="button"> <i class="fas fa-tint"></i> Donate Now</button>
        </div>
        <div class=" col-12 col-xl-6 p-3 about-blood-table">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <h4>Compatible Blood Type Donors</h4>
                    </tr>
                    <tr>
                        <th scope="col">Blood Type</th>
                        <th scope="col">Donete Blood To</th>
                        <th scope="col">Receiv Blood From</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td scope="row">A+</td>
                        <td>A+ AB+</td>
                        <td>A+ A- O+ O-</td>
                    </tr>
                    <tr>
                        <td scope="row">O+</td>
                        <td>O+ A+ B+ AB+</td>
                        <td>O+ O-</td>
                    </tr>
                     <tr>   
                        <td scope="row">B+</td>
                        <td>B+ AB+</td>
                        <td>B+ B- O+ O-</td>
                    </tr>
                    <tr>
                        <td scope="row">AB+</td>
                        <td>AB+</td>
                        <td>Everyone</td>
                    </tr>    
                    <tr>    
                        <td scope="row">A-</td>
                        <td>A+ A- AB+ AB-</td>
                        <td>A- O-</td>
                    </tr>    
                    <tr>    
                        <td scope="row">O-</td>
                        <td>Everyone</td>
                        <td>O-</td>
                    </tr>    
                    <tr>    
                        <td scope="row">B-</td>
                        <td>B+ B- AB+ AB-</td>
                        <td>B- O-</td>
                    </tr>
                    <tr>
                        <td scope="row">AB-</td>
                        <td>AB+ AB-</td>
                        <td>AB- A- B- O-</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <!-- ============================================ -->

    <!-- ========= Donation Guide ================= -->
    <div class=" container-fluid row " id="donation-guide">
        <div class="col-12 col-xl-6 ">
            <div>
                <h2>
                    Blood Donate Guide
                </h2>
                <p>
                    If you are new to blood donation, download our free donor's guide. There are we answer the most frequently asked questions. Learn the truth about the blood donation process and how you can help patients.
                </p>
                <form>
                    <input type="email " name="guide-email " class="input-group form-control " placeholder="Your Email " required>
                    <button class="button ">Download</button>
                </form>
            </div>
        </div>
        <div class="col-12 col-xl-6 " id="donation-guide-image">
            <div></div>
        </div>
    </div>

    <!-- ========= End Guide ================= -->
