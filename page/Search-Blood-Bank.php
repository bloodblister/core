
<?php
include 'php/connection.php';
if (!isset ($_GET['page_no']) ) {  
    $page_no = 1;  
} else {  
    $page_no = $_GET['page_no'];  
}  
$results_per_page =10;  

$center_sql="SELECT * FROM `centers`";
$center_run=$con->query($center_sql);

$number_of_result = mysqli_num_rows($center_run);  
  
//determine the total number of pages available  
$number_of_page = ceil ($number_of_result / $results_per_page);  
if($page_no > $number_of_page)
{
    $page_no=1;
}
$page_first_result = ($page_no-1) * $results_per_page;  


?>
<style>
	.btn-request{
		background-color: #fff;
	    color: #000;
		border: 2px solid #dddddd;
		
	}
	.btn-request:hover{
		background-color: #dddddd;
	    color: #000;
	}
	.btn-danger:hover{
		background-color: #fff;
		border-color: #dc3545;
		color: #dc3545;
	}
</style>
<!-- =============== Nearest Blood bank =========== -->
<div class="container-fluid row">
    <div class="col-12 col-xl-10" id="nearest-blood-bank">
        <h3>Search Centers</h3>
        <form action="#" class="form container-fluid row" id="sreach-form">

            <div class="col-md-4 border-right">
                <input type="text" class="form-control input-group" name="center_name" placeholder="Center name">
            </div>
            <div class="col-md-3">
				<input type="text" class="form-control input-group" name="city" placeholder="City Name">
            </div>
			<div class="col-md-3">
				<select name="Center_type" class="form-control" style="font-size: 15px" required>
					<option value="" required>--- Sellect Center Type ---</option>
					<option value="1">Hospital</option>
					<option value="2">Blood Bank</option>
				</select></select>
            </div>
            <div class="col-md-2">
                <button type="submit" class="button">Search</button>
            </div>
        </form>
    </div>
</div>
<div class="container-fluid row">
    <div class="col-12 col-xl-10" id="location-list">
        <table class="table">
            <thead class="table-primary">
                <th>No.</th>
                <th>Name</th>
                <th>Email</th>
                <th>Address</th>
                <th>City</th>
                <th>Phone</th>
				<th class="text-center">Appointment</th>
            </thead>


            <tbody class="table-light" id="tbody-div">
                <?php 
    
    $center_sql="SELECT * FROM `centers` LIMIT " . $page_first_result . "," . $results_per_page;
    $center_run=$con->query($center_sql);
    if($center_run)
    {   $i=$page_first_result+1;
        while($center=$center_run->fetch_assoc())
        {
			// print_r($center);
			if(isset($center['h_id']))
			{
				$h_sql="SELECT * FROM `hospital` WHERE `h_id`=".$center['h_id'];
				$get_center=$con->query($h_sql);
				$center_details=mysqli_fetch_assoc($get_center);
			
				if($center_details['city_id'])
				{
					$city_sql="SELECT `name` FROM `city` WHERE `id`=".$center_details['city_id'];
					$city_run=$con->query($city_sql);
					$city_name=$city_run->fetch_assoc();
					$city=$city_name['name'];
				}
				else{
					$city="";
				}
		
				
				?>
                <tr>
					
					<td><?php echo $i++;?></td>
					<td><?php echo $center_details['h_name'];?></td>
					<td><?php echo $center_details['email'];?></td>
					<td><?php echo $center_details['h_ads1'].$center_details['h_ads2'];?></td>
					<td><?php echo $city;?></td>
					<td><?php echo $center_details['h_contect'];?></td>
					<td>
						<a href="index.php?page=Donation-Request&&appointment_type=1&&hospital_id=<?php echo $center_details['h_id'];?>" class="btn btn-danger">Donation</a>
						<a href="index.php?page=Blood-bottle-Request&&appointment_type=0&&hospital_id=<?php echo $center_details['h_id'];?>" class="btn btn-request">Request</a>
					</td>
				</tr>
                <?php
			}
			
			if(isset($center['b_id']))
			{
				$b_sql="SELECT * FROM `branch` WHERE `b_id`=".$center['b_id'];
				$get_center=$con->query($b_sql);
				$center_details=$get_center->fetch_assoc();

					if($center_details['city_id'])
					{
						$city_sql="SELECT `name` FROM `city` WHERE `id`=".$center_details['city_id'];
						$city_run=$con->query($city_sql);
						$city_name=$city_run->fetch_assoc();
						$city=$city_name['name'];
					}
					else{
						$city="";
					}
		
				
				?>
				<tr>

					<td><?php echo $i++;?></td>
					<td><?php echo $center_details['b_name'];?></td>
					<td><?php echo $center_details['email'];?></td>
					<td><?php echo $center_details['b_ads1'].$center_details['b_ads2'];?></td>
					<td><?php echo $city;?></td>
					<td><?php echo $center_details['b_contect'];?></td>
					<td>
						<a href="index.php?page=Donation-Request&&appointment_type=1&&branch_id=<?php echo $center_details['b_id'];?>" class="btn btn-danger">Donation</a>
						<a href="index.php?page=Blood-bottle-Request&&appointment_type=0&&branch_id=<?php echo $center_details['b_id'];?>" class="btn btn-request">Request</a>
					</td>
				</tr>

                <?php
			}
		}
    }

    ?>
            </tbody>
        </table>
    </div>
</div>


<div class="container-fluid row">
    <div class="col-xl-10 m-auto p-0">
        <nav aria-label="Page navigation example" class="float-right">
            <ul class="pagination">
                <?php 
					if($page_no-1 == 0 ){
						echo '<li class="page-item"><a class="page-link" href = "#">Prev</a></li>';
					}else{
						$prev=$page_no-1;
						echo '<li class="page-item"><a class="page-link" href = "index.php?page=Search-Blood-Bank&&page_no=' . $prev . '">Prev</a></li>';
					}
				?>

                <?php 
					for($pg = 1; $pg<= $number_of_page; $pg++) {  
						echo '<li class="page-item"><a class="page-link" href = "index.php?page=Search-Blood-Bank&&page_no=' . $pg . '">' . $pg . ' </a></li>';  
					} 
				?>
                <?php 
					$next=$page_no+1;    
					if($page_no == $number_of_page ){
						echo '<li class="page-item"><a class="page-link" href = "#">Next</a></li>';
					}else{
						echo '<li class="page-item"><a class="page-link" href = "index.php?page=center&&page_no=' . $next . '">Next</a></li>';
					}
				?>
            </ul>
        </nav>
    </div>
</div>

<!-- -===================================================== -->

<!-- call us -->
<div class="container-fluid row" id="callus">
    <div class="col-12">
        <div>
            <h1> Call us <a href="tel:+9190998013" class="phoneno">(+91)9099801380</a></h1>
            <p>or click the link below to book your appointment</p>
            <button class="button" onclick="window.location='index.php?page=Donation-Request&&appointment_type=1' " >Donate Now</button>
        </div>
    </div>
</div>
<!-- ============================== -->


<script>
		
	$('#sreach-form').submit(function (e) { 
		e.preventDefault();
		console.log($("#sreach-form").serialize());
		$.ajax({
			url: "php/search-center.php",
			data: $("#sreach-form").serialize(),
			success: function (response) {
				console.log(response);
				$('#tbody-div').empty();
				$('#tbody-div').append(response);
			}
		});
	});

	$("select[name='state']").change(function (e) { 
		e.preventDefault();
		var id=$(this).val();
	
		$.ajax({
			url: "php/city.php",
			data: {
				stateid:id
			},
			success: function (response) {
				$('#city_div').empty();
				$('#city_div').append(response);

			}
		});
	});

</script>
