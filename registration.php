<?php 
require "php/connection.php";
session_start();
$error=[];

unset( $error['email']);
unset( $error['fail']);
unset( $error['not_match']);
unset( $error['password_length']);

if(isset($_REQUEST['submit']))
{
   
   

    $username=$_REQUEST['username'];
    $email=$_REQUEST['email'];
    $password=$_REQUEST['password'];
    $cpassword=$_REQUEST['cpassword'];

    if(strlen($password) >= 7)
    {
        if($password == $cpassword)
        {
            $e_sql="SELECT `email` FROM `login` WHERE email='".$email."'";
            $e_result=mysqli_query($con,$e_sql);
            $e_row =mysqli_num_rows($e_result);
            
            if($e_row <= 0)
            {
                $sql1="INSERT INTO `login`(`email`, `password`) VALUES ('".$email."','".md5($password)."')";
                $login=mysqli_query($con,$sql1);

               $sql2="INSERT INTO `registration`(`r_firstname`, `email`) VALUES ('".$username."','".$email."');";
               $registra=mysqli_query($con,$sql2);

                if($login && $registra)
                {
                    $_SESSION['email']=$email;
                    ?>
                    <script type="text/javascript">
                         window.location="index.php";
                    </script>
                    
                    <?php
                }
                else{
                    // $del="DELETE FROM `login_m` WHERE email='".$email."';";
                    // $del.="DELETE FROM `registration_m` WHERE email='".$email."'";
                    // mysqli_multi_query($con,$del);
                    $error['fail']="Registration failed please try again kindle";
                }
            }
            else{
                $error['email']="This Email is already exit.";
            }
        }
        else{
            $error['not_match']="Confirm Password not match";
        }
    }
    else
    {
        $error['password_length'] = "Password must be 8 digit";
    }
}



?>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Login</title>

    <?php require 'attachment/link.php'?>

    <link rel="stylesheet" type="text/css" href="css/login.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
   
    
    <style>
    ul.parsley-errors-list li{
        list-style: none;
        color: red;
        text-transform: capitalize;
    }
    ul.parsley-errors-list{
        padding:5px 0 0 10px;
        margin-bottom: 10px;
    }
    </style>


</head>

<body>
<div class="container" id="skip">
        <h5 class="text-right pr-5  mr-3 mt-5"><span onclick="window.location='index.php'"><b>Skip <i
                        class="fa fa-arrow-right"></i></b><span></h5>
    </div>

    <div class="container-fluid row m-auto">
        <div class="col-md-6 col-xl-4 m-auto " id="form">
            <div id="border-bottom">
                <h2>Welcome</h2>
                <p>Enter your details to create account</p>
            </div>

            <?php
                if(isset($error['fail']))
                {
            ?>
            <div class="alert alert-danger" role="alert">
                    <?php 
                    echo $error['fail'];
                ?>
            </div>

            <?php
                }
            ?>

            <form class="form row mt-3" action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post" data-parsley-validate>

                <div class="form-group col-xl-12">
                    <label>Username </label>
                    <div class="input-div">
                        <i class="fa fa-user" aria-hidden="true"></i>
                        <input type="text" name="username" n class="form-control" data-parsley-required-message="please enter name" placeholder="Enter Username" autofocus="on" value="<?php if(isset($username)){echo $username;} ?>" required>
                    </div>
                   

                </div>
                
                <div class="form-group col-xl-12">
                    <label>Email </label>
                    <div class="input-div">
                        <i class="fa fa-envelope" ></i>
                        <input type="email" name="email" n class="form-control" placeholder="Enter Email" data-parsley-required-message="please enter email" autofocus="on" value="<?php if(isset($email)){echo $email;} ?>" required>
                    </div>
                    <?php
                      if(isset($error['email']))
                        {
                    ?>
                    <p class="text-danger mb-0">
                            <?php 
                            echo $error['email'];
                        ?>
                    </p>

                    <?php
                        }
                    ?>

                </div>

                
                <div class="form-group col-xl-12">
                    <label>Password</label>
                    <div class="passwor-div">
                        <i class="fa fa-lock" ></i>
                        <input type="password" name="password" data-parsley-minlength="8" data-parsley-required-message="please enter valid Passowrd " data-parsley-trigger="keypress" data-parsley-minlength-message="password should be 8 character long" class="form-control password"
                            placeholder="Enter Password" required>
                    </div>
                    <?php
                      if(isset($error['password_length']))
                        {
                    ?>
                    <p class="text-danger mb-0">
                            <?php 
                            echo $error['password_length'];
                        ?>
                    </p>

                    <?php
                        }
                    ?>
                </div>
                <div class="form-group col-xl-12 m-0">
                    <label>Confirm Password </label>
                    <div class="input-div">
                        <i class="fa fa-lock" ></i>
                        <input type="password" name="cpassword" data-parsley-required-message="please enter confirm password" n class="form-control" placeholder="Confirm Password" autofocus="on" required>
                    </div>
                    <?php
                      if(isset($error['not_match']))
                        {
                    ?>
                    <p class="text-danger mb-0">
                            <?php 
                            echo $error['not_match'];
                        ?>
                    </p>

                    <?php
                        }
                    ?>
                </div>
                <div class="form-group col-xl-12 m-0">
                    <input type="submit" name="submit" class="btn float-right">
                </div>
                <div class="col-xl-12" id="link">
                    <p><a href="login.php">Login</a></p>
                </div>

            </form>

        </div>
    </div>


</body>

</html>