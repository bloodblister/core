<?php 

require "php/connection.php";
session_start();

$error=array();
unset($error['email']);  
unset($error['password']);

if(isset($_REQUEST['submit']))
{
	$email = $_REQUEST['email'];


    $sql="SELECT `password`,`user_type` FROM `login` WHERE `email`='". $_REQUEST['email']."'";
	$result=mysqli_query($con,$sql);
	$row_num =mysqli_num_rows($result);

	if($row_num > 0)
    {
       
        $row=mysqli_fetch_array($result);
        if($row[0] == md5($_REQUEST['password']))
        {
            if($row[1] == 0)
            {
                $_SESSION['email']=$email;
                ?>
                <script type="text/javascript">
                 window.location = "/core";
                </script>
                <?php
            }
            else if($row[1] == 1)
            {
                $_SESSION['admin']=$email;
                ?>
                <script type="text/javascript">
                 window.location = "admin";
                </script>
                <?php
            }
            else if($row[1] == 2)
            {
                $_SESSION['hospital_email']=$email;
                ?>
                <script type="text/javascript">
                 window.location = "hospital";
                </script>
                <?php
            }
            else if($row[1] == 3)
            {
                $_SESSION['branch_email']=$email;
                ?>
                <script type="text/javascript">
                 window.location = "branch";
                </script>
                <?php
            }

        }
        else{
            $error['password']="Incorrect Password";
        }

    }
    else{
      
        $error['email']="Email does not exist";
    }

}



?>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Login</title>

    <?php require 'attachment/link.php'?>

    <link rel="stylesheet" type="text/css" href="css/login.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">


    <style>
    ul.parsley-errors-list li{
        list-style: none;
        color: red;
        text-transform: capitalize;
    }
    ul.parsley-errors-list{
        padding:5px 0 0 10px;
        margin-bottom: 10px;
    }
    </style>
    <script>
    $(document).ready(function() {
        $(".form").submit(function(e) {
            var value = $(".password").val();
            if (value.length >= 8) {
                $(".password-erroe").hide();
                return true;
            } else {
                $(".password-erroe").show();
                return false;
            }
        });

        $('.fa-eye').click(function(e) {
            $(".password").attr('type', 'text');
            $(this).hide();
            $('.fa-eye-slash').show();
        });
        $('.fa-eye-slash').click(function(e) {
            $(".password").attr('type', 'password');
            $(this).hide();
            $('.fa-eye').show();
        });

    });
    </script>

    <style>

    </style>
</head>

<body>
    <div class="container" id="skip">
        <h5 class="text-right pr-5  mr-3 mt-5"><span onclick="window.location='index.php'"><b>Skip <i
                        class="fa fa-arrow-right"></i></b><span></h5>
    </div>

    <div class="container-fluid row m-auto">
        <div class="col-md-6 col-xl-4 m-auto " id="form">
            <div id="border-bottom">
                <h2>Welcome Back</h2>
                <p>Enter your credentials to access your account</p>
            </div>
            <form class="form row mt-3" action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post">

                <div class="form-group col-xl-12">
                    <label>Email </label>
                    <div class="input-div">
                        <i class="fa fa-envelope"></i>
                        <input type="email" name="email" n class="form-control" placeholder="Enter Email" autofocus="on"
                            required>
                    </div>

                    <?php
                        if(isset($error['email']))
                        {
                    ?>
                    <p class="text-danger mt-1"><?php echo $error['email'] ?></p>
                    <?php 
                    }
                    ?>
                </div>
                <div class="form-group col-xl-12 m-0">
                    <label>Password</label>
                    <div class="passwor-div">
                        <i class="fa fa-lock"></i>
                        <input type="password" name="password" class="form-control password"
                            placeholder="Enter Password" required>
                        <span><i class="fas fa-eye"></i></span>
                        <span><i class="fas fa-eye-slash"></i></span>
                    </div>
                    <div class="invalid-feedback password-erroe">Password Must be 8 digit</div>
                    <?php
                        if(isset($error['password']))
                        {
                    ?>
                    <p class="text-danger mt-1"><?php echo $error['password'] ?></p>
                    <?php 
                    }
                    ?>
                </div>
                <div class="form-group col-xl-12 m-0">
                    <input type="submit" name="submit" class="btn float-right">
                </div>
                <div class="col-xl-12" id="link">
                    <p><a href="registration.php">Registre</a> <a href="forget_password.php" class="float-right">Forget Password</a></p>
                </div>

            </form>

        </div>
    </div>


</body>

</html>